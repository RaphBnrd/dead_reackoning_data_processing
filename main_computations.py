import time
import datetime

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from mpl_toolkits import mplot3d
from pylab import show
import csv

from scipy import signal
from pyproj import Transformer

from i_force_function import add_force_developped
from h_export_json import export_df_final
from g_computation_functions import butter_lowpass_filter, find_freq_fondamental
from e_class_graphic_interfaces import InterfaceFiltering, InterfaceFilteringBehaviour, FinalNextPlot, InterfaceTrajectory3D, InterfaceTrajectory2DSimple
from f_function_plot_graphs_psd import plot_graphs_behaviour, plot_graph_with_psd_with_filter, plot_trajectory3D, plot_trajectory2D_simple, plot_components_force, plot_distribution_v_g, plot_activity_over_day, plot_force_velocity, plot_scan_2D_records, plot_scan_3D_records
from a_import_functions import first_import_csv
from b_filter_functions import filter_behaviour
from c_compute_grade import add_columns_topo_animal, get_elevation_map, add_column_grade_simple
from d_scan_records import scan_records_2D, scan_records_3D


"""
Data must be given at a certain multiple of period sampling (not between 2 sampling period)
ex: if the data are collected each 0.2 sec, we should only have data on multiples of 0.2
(not at 3.1 sec for example)
The processing then interpolate the data for each sampling period
"""





def launch_process(with_graphic_interface=False, export_results=True, export_df={'interp':False, 'full':False, 'small':False}, 
                   path_input='data/data_small_1/Ibex1_complete_v2_short3.csv', type_import='csv',
                   csv_type_columns='complete',
                   max_len_plot=int(2*86400), datetime_start_file='2017-06-02 00:00:00',
                   datetime_start_process='2017-06-10 00:00:00', datetime_end_process='2017-06-10 14:00:00', 
                   sampling_period=1, interp_method='cubic', 
                   dict_computations={'Scan record velocity 2D':True, 'Scan record velocity 3D':True,
                                      'Scan record force 3D':True},
                   dict_display_plots={'Behaviour':True, 'Latitude-Longitude':True,
                                       'Trajectory 2D':True, 'Grade':True, 'Trajectory 3D':True,
                                       'Velocity':True, 'Force':True, 'Components of force':True, 
                                       'Distributions (v-g)':True,
                                       'Activity over day':True, 'Force-velocity':True,
                                       'Records 2D':True, 'Records 3D':True, 'Records 3D force':True},
                   show_final_graphs = True,
                   cutoff_mov=5e-4, cutoff_lon=0.05, cutoff_lat=0.05, cutoff_gra=0.2, cutoff_vel=0.2, cutoff_for=0.2,
                   order_mov=1, order_lon=1, order_lat=1, order_gra=1, order_vel=1, order_for=1,
                   threshold_moving=0.05, threshold_std_g_scan_vgt=5,
                   times_scan_v=[5, 15, 30, 60, 90] + [k*60 for k in range(2, 31)], 
                   times_scan_v_g=[5, 15, 30, 60, 90] + [k*60 for k in range(2, 31)],
                   grade_interv_borders_v_g=[i for i in range(-50, 51, 2)], 
                   threshold_std_v_scan_Fvt=5,
                   times_scan_f_v=[5, 15, 30, 60, 90] + [k*60 for k in range(2, 31)],
                   v_interv_borders_f_v=list(np.arange(0, 4, 0.2)) + list(np.arange(4, 16, 1))):
    """
    Function that executes the whole process. 
    It depends on all parameters
    Input:
        . with_graphic_interface is a boolean that inform whether we display interface or not
        . export_results is a boolean that inform whether we export records 2D/3D and parameters or not
        . export_df is the dictionary of boolean that inform whether we export dataframes computed or not
        . path_input is the path of the file we take in input
        . type_import is the type of import we use (csv, json dataframe interpolated, json final dataframe)
        . csv_type_columns is the type of csv we import depending on the columns there are
          ('corrected' if there are Date, Time, Latitude, Longitude, Altitude
          or 'complete' if there are DateTime, DDMT_Latitude, DDMT_Longitude, Altitude, Behaviour)
        . max_len_plot is the maximum number of points displayed in the graphs
        . datetime_start_file is the datetime of the first instant of the file we import
        . datetime_start_process is the datetime of the first instant we want to import
        . datetime_end_process is the datetime of the last instant we want to import
        . sampling_period is the sampling period of the interpolation done during import
        . interp_method is the method of interpolation used in the process
        . dict_computations is the dictionary containing the computations we have to do (scan 2D/3D)
        . dict_display_plots is the dictionary containing the graphs we have to display
        . show_final_graphs is a boolean that inform whether we display final plots or not
        . cutoff_mov, cutoff_lon, cutoff_lat, cutoff_gra, cutoff_vel, cutoff_for
          are the cutoffs used for each data we have to filter
        . order_mov, order_lon, order_lat, order_gra, order_vel, order_for
          are the orders used for each data we have to filter
        . threshold_moving is the threshold used to decide whether the animal is moving or not
        . threshold_std_g_scan_vgt is the maximum std permitted for the slope on a window scanned
        . times_scan_v is the list containing all the time window width used in the 2D scan
        . times_scan_v_g is the list containing all the time window width used in the 3D scan
        . grade_interv_borders_v_g is the list of borders intervalls for slope ranges in the 3D scan
        . threshold_std_v_scan_Fvt is the maximum std permitted for the velocity on a window in the F-v-t 3D scan
        . times_scan_f_v is the list containing all the time window width used in the F-v-t 3D scan
        . v_interv_borders_f_v is the list of borders intervalls for velocity ranges in the F-v-t 3D scan
    Output:
        . 
    """
    fs = 1/sampling_period       # sample rate, Hz
    # String used in export names
    str_datetime_start_process = datetime_start_process.replace('-', '')
    str_datetime_start_process = str_datetime_start_process.replace(':', '')
    str_datetime_start_process = str_datetime_start_process.replace(' ', '-')
    str_datetime_end_process = datetime_end_process.replace('-', '')
    str_datetime_end_process = str_datetime_end_process.replace(':', '')
    str_datetime_end_process = str_datetime_end_process.replace(' ', '-')






    ##### _____ IMPORT _____ ##### __________________________________________________________

    print("\n\n\n#### START IMPORT ####")
    start_import = time.time()

    if type_import == 'csv': 
        # In this case, we import the csv file and export the dataframe interpolated in a json file
        df = first_import_csv(path_input, sampling_period, interp_method, 
                              datetime_start_file, datetime_start_process, datetime_end_process,
                              csv_type_columns=csv_type_columns)

        # Export a json file for this interpolation
        if export_df['interp']:
            if path_input[-4:] == '.csv':
                path_json_export = path_input[:-4]+'_'+str_datetime_start_process+'_TO_'+\
                                     str_datetime_end_process+'_interpolated.json'
            else:
                path_json_export = path_input+'_'+str_datetime_start_process+'_TO_'+\
                                     str_datetime_end_process+'_interpolated.json'
            df.to_json(path_json_export)
            print("\nDataframe exported at:", path_json_export)

    elif type_import == 'json interpolated':
        # In this case, we import the json file containing the dataframe interpolated
        df = pd.read_json(path_input)

    elif type_import == 'json full':
        # In this case, we import the json file containing the full dataframe
        df = pd.read_json(path_input)
        

        # ___ Data used at the end of the script (computed only in first computation) ___
        
        # idxs_start_moving, idxs_end_moving
        is_moving_filtered = butter_lowpass_filter((df['Behaviour']==3), cutoff_mov, fs, order_mov)
        bool_is_moving_filtered = (is_moving_filtered >= threshold_moving) * 1
        delta_is_moving = np.array([None] + list(np.diff(bool_is_moving_filtered)))
        idxs_start_moving = list(np.where(delta_is_moving==1)[0]) # Not Moving at k-1 and Moving at k
        idxs_end_moving = list(np.where(delta_is_moving==-1)[0]) # Moving at k-1 and Not Moving at k
        if bool_is_moving_filtered[0] == True:
            idxs_start_moving = [0] + idxs_start_moving # idxs_start_moving are inside the duration (to call df[idxs_start_moving:])
        if bool_is_moving_filtered[df.shape[0]-1] == True:
            idxs_end_moving = idxs_end_moving + [df.shape[0]] # idxs_end_moving are outside the duration (to call df[:idxs_end_moving])
        if len(idxs_start_moving) != len(idxs_end_moving):
            print("Intervalls of move seems false... (not the same length for the lists of starts and ends)")
        idx_first_grade = np.where(np.isfinite(df['Grade']))[0][0]
        idx_last_grade = np.where(np.isfinite(df['Grade']))[0][-1]
        idx_first_force = np.where(np.isfinite(df['Force']))[0][0]
        idx_last_force = np.where(np.isfinite(df['Force']))[0][-1]

        # PSD
        f_lon, Pxx_den_lon = signal.periodogram(df['Longitude'], fs)
        f_lat, Pxx_den_lat = signal.periodogram(df['Latitude'], fs)
        f_gra, Pxx_den_gra = signal.periodogram(df['Grade'][idx_first_grade:idx_last_grade+1], fs)
        f_vel, Pxx_den_vel = signal.periodogram(df['Velocity'][1:], fs)
        f_for, Pxx_den_for = signal.periodogram(df['Force'][idx_first_force:idx_last_force+1], fs)

        # TOPOGRAPHY INTERPOLATED FOR THE FINAL PLOT
        lon_grid, lat_grid, elev_grid = get_elevation_map(df)
        transformer_from_latlon_to_xy = Transformer.from_crs("epsg:4326", "epsg:3857")
        # Convert lat lon into x y for the grid of topography
        x_grid_flat, y_grid_flat = transformer_from_latlon_to_xy.transform(lat_grid.flatten(), lon_grid.flatten())
        x_grid, y_grid = x_grid_flat.reshape(lon_grid.shape), y_grid_flat.reshape(lat_grid.shape)
        


    print("----  Import Done  ----")
    time_import = time.time()-start_import
    print("Time for the import:     "+str(datetime.timedelta(seconds=(time_import))))






    # We compute columns required to scan
    if type_import in ['csv', 'json interpolated']:


        ##### _____ FILTER BEHAVIOUR _____ ##### ____________________________________________
        if with_graphic_interface and dict_display_plots['Behaviour'] == True:
            print("\n\n\n#### ENTRY PROCESS BEHAVIOUR ####")
            win_filter_behaviour = InterfaceFilteringBehaviour(
                        fs=fs,
                        df=df, 
                        col_behav=['Behaviour'],
                        times='DateTime',
                        idxs_time_limits=(0,min(max_len_plot, df.shape[0]-1)),
                        max_len_plot=max_len_plot,
                        cutoff=cutoff_mov,
                        order=order_mov,
                        threshold_moving=threshold_moving)
            show()

            cutoff_mov = float(win_filter_behaviour.var_cutoff.get())
            order_mov = int(win_filter_behaviour.var_order.get())
            threshold_moving = float(win_filter_behaviour.var_thresh.get())
            win_filter_behaviour.print_param()

        # ___ Filter the behaviour IsMoving ___
        print("\n\n\n#### FILTER BEHAVIOUR ####")
        start_filter_behaviour = time.time()

        idxs_start_moving, idxs_end_moving, is_moving_filtered, bool_is_moving_filtered\
            = filter_behaviour(df, fs, cutoff_mov, order_mov, threshold_moving)

        print("----  Behaviour filtered  ----")
        time_filter_behaviour = time.time()-start_filter_behaviour
        print("Time for filtering behaviour:     "+str(datetime.timedelta(seconds=(time_filter_behaviour))))

        if dict_display_plots['Behaviour'] == True and not(with_graphic_interface):
            # Plot Behaviour
            plot_graphs_behaviour(df, max_len_plot, threshold_moving, 
                                  is_moving_filtered, bool_is_moving_filtered)





        ##### _____ FILTER LONGITUDE LATITUDE _____ ##### ___________________________________

        # Calculate the Power Spectral Density (to find how to filter)
        f_lon, Pxx_den_lon = signal.periodogram(df['Longitude'], fs)
        f_lat, Pxx_den_lat = signal.periodogram(df['Latitude'], fs)

        # cutoff_lon = round(find_freq_fondamental(df['Longitude'], cutoff=0.00005, nyq=0.5*fs, order=order_lon, fs=fs), 6)
        # cutoff_lat = round(find_freq_fondamental(df['Latitude'], cutoff=0.00005, nyq=0.5*fs, order=order_lat, fs=fs), 6)
        # print(cutoff_lon, cutoff_lat)

        # ___ Entry for filtering (graphical interface) ___
        if with_graphic_interface and dict_display_plots['Latitude-Longitude'] == True:
            print("\n\n\n#### ENTRY PROCESS LONGITUDE LATITUDE ####")
            win_filter_lonlat = InterfaceFiltering(
                        vars=['longitude', 'latitude'],
                        window_title='Filter lon-lat',
                        fs=fs,
                        df = df, 
                        columns=['Longitude', 'Latitude'],
                        times='DateTime',
                        idxs_time_limits=(0,min(max_len_plot, df.shape[0]-1)),
                        max_len_plot=max_len_plot,
                        y_labels=["Longitude (°)", "Latitude (°)"], 
                        freqs=[f_lon, f_lat], 
                        P=[Pxx_den_lon, Pxx_den_lat],
                        cutoffs=[cutoff_lon, cutoff_lat],
                        orders=[order_lon, order_lat],
                        fig_title='Longitude-Latitude', 
                        graph_title="Lon-lat over time")
            show()

            cutoff_lon = float(win_filter_lonlat.string_vars_cutoffs['longitude'].get())
            cutoff_lat = float(win_filter_lonlat.string_vars_cutoffs['latitude'].get())
            order_lon = int(win_filter_lonlat.string_vars_orders['longitude'].get())
            order_lat = int(win_filter_lonlat.string_vars_orders['latitude'].get())
            win_filter_lonlat.print_param()

        # ___ Filter longitude-latitude ___
        print("\n\n\n#### FILTER LONGITUDE LATITUDE ####")
        start_filter_lonlat = time.time()

        column_lon_filtered = butter_lowpass_filter(df['Longitude'], cutoff_lon, fs, order_lon)
        column_lat_filtered = butter_lowpass_filter(df['Latitude'], cutoff_lat, fs, order_lat)
        df['Longitude_filtered'] = column_lon_filtered
        df['Latitude_filtered'] = column_lat_filtered

        print("----  Longitude-Latitude filtered  ----")
        time_filter_lonlat = time.time()-start_filter_lonlat
        print("Time for filtering Longitude-Latitude:     "+str(datetime.timedelta(seconds=(time_filter_lonlat))))

        # Calculate the filtered Power Spectral Density (to monitor if the filter is adequate)
        f_lon_filter, Pxx_den_lon_filter = signal.periodogram(df['Longitude_filtered'], fs)
        f_lat_filter, Pxx_den_lat_filter = signal.periodogram(df['Latitude_filtered'], fs)

        # Plot the delta data
        if dict_display_plots['Latitude-Longitude'] == True and not(with_graphic_interface):
            plot_graph_with_psd_with_filter(df.head(min(max_len_plot, df.shape[0])), 
                    columns=['Longitude', 'Latitude'],
                    times='DateTime',
                    y_labels=["Longitude (°)", "Latitude (°)"], 
                    freqs=[f_lon, f_lat], 
                    P=[Pxx_den_lon, Pxx_den_lat],
                    columns_filtered=['Longitude_filtered', 'Latitude_filtered'],
                    freqs_filtered=[f_lon_filter, f_lat_filter], 
                    P_filtered=[Pxx_den_lon_filter, Pxx_den_lat_filter],
                    cutoffs=[cutoff_lon, cutoff_lat],
                    fig_title='Longitude-Latitude', 
                    graph_title="Lon-lat over time")






        ##### _____ COMPUTE THE TOPOGRAPHY _____ ##### ______________________________________

        # Add the columns 'X', 'Y', 'Z'
        print("\n\n\n#### COMPUTE TOPOGRAPHY ####")
        start_comp_topo = time.time()

        df, lon_grid, lat_grid, x_grid, y_grid, elev_grid = add_columns_topo_animal(df, 
                                                            column_lon='Longitude',
                                                            column_lat='Latitude',
                                                            column_lon_filtered='Longitude_filtered',
                                                            column_lat_filtered='Latitude_filtered')
        
        print("----  Topography computed  ----")
        time_comp_topo = time.time()-start_comp_topo
        print("Time for the computation of the topography:     "+str(datetime.timedelta(seconds=(time_comp_topo))))

        # Plot trajectory 2D
        if dict_display_plots['Trajectory 2D'] == True:
            if with_graphic_interface:
                win_plot_traj2D = InterfaceTrajectory2DSimple(df, (0,min(max_len_plot, df.shape[0]-1)), max_len_plot)
                show()
            else:
                plot_trajectory2D_simple(df, 0, min(max_len_plot, df.shape[0]-1))





        ##### _____ COMPUTE THE GRADE _____ ##### ___________________________________________
        print("\n\n\n#### COMPUTE GRADE ####")
        start_compute_grade = time.time()
        df, idx_first_grade, idx_last_grade = add_column_grade_simple(df, columnX='X_from_filter',
                                                                      columnY='Y_from_filter',
                                                                      columnZ='Z_from_filter')
        print("----  Grade computed  ----")
        time_compute_grade = time.time()-start_compute_grade
        print("Time for computing grade:     "+str(datetime.timedelta(seconds=(time_compute_grade))))

        # ___ Filter the grade ___
        f_gra, Pxx_den_gra = signal.periodogram(df['Grade'][idx_first_grade:idx_last_grade+1], fs)

        # cutoff_gra = round(find_freq_fondamental(df['Grade'][idx_first_grade:idx_last_grade+1], cutoff=0.00005, nyq=0.5*fs, order=order_gra, fs=fs), 6)
        # print(cutoff_gra)

        # ___ Entry for filtering (graphical interface) ___
        if with_graphic_interface and dict_display_plots['Grade'] == True:
            print("\n\n\n#### ENTRY PROCESS GRADE ####")
            win_filter_gra = InterfaceFiltering(
                        vars=['grade'],
                        window_title='Filter grade',
                        fs=fs,
                        df = df, 
                        columns=['Grade'],
                        times='DateTime',
                        idxs_time_limits=(max(1, idx_first_grade),min(idx_last_grade, df.shape[0]-1)),
                        max_len_plot=max_len_plot,
                        y_labels=["Grade (%)"], 
                        freqs=[f_gra], 
                        P=[Pxx_den_gra],
                        cutoffs=[cutoff_gra],
                        orders=[order_gra],
                        fig_title='Grade', 
                        graph_title="Grade over time")
            show()

            cutoff_gra = float(win_filter_gra.string_vars_cutoffs['grade'].get())
            order_gra = int(win_filter_gra.string_vars_orders['grade'].get())
            win_filter_gra.print_param()

        print("\n\n\n#### FILTER GRADE ####")
        start_filter_grade = time.time()
        
        column_gratopo_filtered = butter_lowpass_filter(df['Grade'][idx_first_grade:idx_last_grade+1], cutoff_gra, fs, order_gra)
        column_gratopo_filtered = [None]*idx_first_grade + list(column_gratopo_filtered) + \
                                  [None]*(df.shape[0]-idx_last_grade-1)
        df['Grade_filtered'] = column_gratopo_filtered

        print("----  Grade filtered  ----")
        time_filter_grade = time.time()-start_filter_grade
        print("Time for filtering grade:     "+str(datetime.timedelta(seconds=(time_filter_grade))))

        # Calculate the filtered Power Spectral Density (to monitor if the filter is adequate)
        f_gra_filter, Pxx_den_gra_filter = signal.periodogram(df['Grade_filtered'][idx_first_grade:idx_last_grade+1], fs)

        # Plot the grade data
        if dict_display_plots['Grade'] == True and not(with_graphic_interface):
            plot_graph_with_psd_with_filter(df.head(min(max_len_plot, df.shape[0])), 
                    columns=['Grade'],
                    times='DateTime',
                    y_labels=["Grade (%)"], 
                    freqs=[f_gra], 
                    P=[Pxx_den_gra],
                    columns_filtered=['Grade_filtered'],
                    freqs_filtered=[f_gra_filter], 
                    P_filtered=[Pxx_den_gra_filter],
                    cutoffs=[cutoff_gra],
                    fig_title='Grade', 
                    graph_title="Grade over time")


        # Plot 3D trajectories
        if dict_display_plots['Trajectory 3D'] == True:
            if with_graphic_interface:
                win_plot_traj3D = InterfaceTrajectory3D(df, (0,min(max_len_plot, df.shape[0]-1)), max_len_plot, 
                                                        x_grid, y_grid, elev_grid)
                win_plot_traj3D.mainloop()
            else:
                plot_trajectory3D(df, 0, min(max_len_plot, df.shape[0]-1), 
                                x_grid, y_grid, elev_grid)






        ##### _____ VELOCITIES _____ ##### __________________________________________________

        # Add a column velocity (between t-1 and t): Velocity in m/s
        print("\n\n\n#### COMPUTE VELOCITY ####")
        start_compute_velocity = time.time()

        dx, dy, dz = df['X'].diff(), df['Y'].diff(), df['Z'].diff() # between k and k-1
        dist_3D = np.sqrt(dx**2 + dy**2 + dz**2) # between k and k-1
        delta_t = df['DateTime'].diff().dt.seconds # between k and k-1
        df['Velocity'] = dist_3D/delta_t # between k and k-1

        print("----  Velocity computed  ----")
        time_compute_vel = time.time()-start_compute_velocity
        print("Time for computing velocity:     "+str(datetime.timedelta(seconds=(time_compute_vel))))

        # Calculate the Power Spectral Density (to find how to filter)
        f_vel, Pxx_den_vel = signal.periodogram(df['Velocity'][1:], fs)

        # cutoff_vel = round(find_freq_fondamental(df['Velocity'][1:], cutoff=0.00005, nyq=0.5*fs, order=order_vel, fs=fs), 6)
        # print(cutoff_vel)

        # ___ Entry for filtering (graphical interface) ___
        if with_graphic_interface and dict_display_plots['Velocity'] == True:
            print("\n\n\n#### ENTRY PROCESS VELOCITY ####")
            win_filter_vel = InterfaceFiltering(
                        vars=['velocity'],
                        window_title='Filter velocity',
                        fs=fs,
                        df = df,
                        columns=['Velocity'],
                        times='DateTime',
                        idxs_time_limits=(1,min(max_len_plot, df.shape[0]-1)),
                        max_len_plot=max_len_plot,
                        y_labels=["Velocity (m/s)"], 
                        freqs=[f_vel], 
                        P=[Pxx_den_vel],
                        cutoffs=[cutoff_vel],
                        orders=[order_vel],
                        fig_title='Velocity', 
                        graph_title="Velocity over time")
            show()

            cutoff_vel = float(win_filter_vel.string_vars_cutoffs['velocity'].get())
            order_vel = int(win_filter_vel.string_vars_orders['velocity'].get())
            win_filter_vel.print_param()

        # ___ Filter the velocity ___
        print("\n\n\n#### FILTER VELOCITY ####")
        start_filter_vel = time.time()

        column_vel_filtered = butter_lowpass_filter(df['Velocity'][1:], cutoff_vel, fs, order_vel)
        column_vel_filtered = [None] + list(column_vel_filtered)
        df['Velocity_filtered'] = column_vel_filtered

        print("----  Velocity filtered  ----")
        time_filter_vel = time.time()-start_filter_vel
        print("Time for filtering velocity:     "+str(datetime.timedelta(seconds=(time_filter_vel))))

        # Calculate the filtered Power Spectral Density (to monitor if the filter is adequate)
        f_vel_filter, Pxx_den_vel_filter = signal.periodogram(df['Velocity_filtered'][1:], fs)

        # Plot the delta data
        if dict_display_plots['Velocity'] == True and not(with_graphic_interface):
            plot_graph_with_psd_with_filter(df.head(min(max_len_plot, df.shape[0])), 
                    columns=['Velocity'],
                    times='DateTime',
                    y_labels=["Velocity (m/s)"], 
                    freqs=[f_vel], 
                    P=[Pxx_den_vel],
                    columns_filtered=['Velocity_filtered'],
                    freqs_filtered=[f_vel_filter], 
                    P_filtered=[Pxx_den_vel_filter],
                    cutoffs=[cutoff_vel],
                    fig_title='Velocity', 
                    graph_title="Velocity over time")






        ##### _____ FORCE _____ ##### __________________________________________________

        # Add a column force (between t and t-2): Force in N/kg = m/s²
        print("\n\n\n#### COMPUTE FORCE ####")
        start_compute_force = time.time()

        df, force_loc, force_gra, force_acc = add_force_developped(df, column_grade='Grade_filtered', column_v='Velocity_filtered',
                                                                   column_date_time='DateTime', Cr=3.35, metab_efficiency=0.25) # between k+1 and k-1
        idx_first_force = np.where(np.isfinite(df['Force']))[0][0]
        idx_last_force = np.where(np.isfinite(df['Force']))[0][-1]

        print("----  Force computed  ----")
        time_compute_force = time.time()-start_compute_force
        print("Time for computing force:     "+str(datetime.timedelta(seconds=(time_compute_force))))

        if dict_display_plots['Components of force'] == True:
            plot_components_force(df, force_loc, force_gra, force_acc)
            if with_graphic_interface:
                plt.show()

        # Calculate the Power Spectral Density (to find how to filter)
        f_for, Pxx_den_for = signal.periodogram(df['Force'][idx_first_force:idx_last_force+1], fs)

        # ___ Entry for filtering (graphical interface) ___
        if with_graphic_interface and dict_display_plots['Force'] == True:
            print("\n\n\n#### ENTRY PROCESS FORCE ####")
            win_filter_for = InterfaceFiltering(
                        vars=['force'],
                        window_title='Filter force',
                        fs=fs,
                        df = df, 
                        columns=['Force'],
                        times='DateTime',
                        idxs_time_limits=(max(1, idx_first_force),min(idx_last_force, df.shape[0]-1)),
                        max_len_plot=max_len_plot,
                        y_labels=["Force (N/kg)"], 
                        freqs=[f_for], 
                        P=[Pxx_den_for],
                        cutoffs=[cutoff_for],
                        orders=[order_for],
                        fig_title='Force', 
                        graph_title="Force over time")
            show()

            cutoff_for = float(win_filter_for.string_vars_cutoffs['force'].get())
            order_for = int(win_filter_for.string_vars_orders['force'].get())
            win_filter_for.print_param()

        # ___ Filter the force ___
        print("\n\n\n#### FILTER THE FORCE ####")
        start_filter_force = time.time()

        column_force_filtered = butter_lowpass_filter(df['Force'][idx_first_force:idx_last_force+1], cutoff_vel, fs, order_vel)
        column_force_filtered = [None]*idx_first_force + list(column_force_filtered) + \
                                [None]*(df.shape[0]-idx_last_force-1)
        df['Force_filtered'] = column_force_filtered

        print("----  Force filtered  ----")
        time_filter_force = time.time()-start_filter_force
        print("Time for filtering force:     "+str(datetime.timedelta(seconds=(time_filter_force))))

        # Calculate the filtered Power Spectral Density (to monitor if the filter is adequate)
        f_for_filter, Pxx_den_for_filter = signal.periodogram(df['Force_filtered'][idx_first_force:idx_last_force+1], fs)

        # Plot the delta data
        if dict_display_plots['Force'] == True and not(with_graphic_interface):
            plot_graph_with_psd_with_filter(df.head(min(max_len_plot, df.shape[0])), 
                    columns=['Force'],
                    times='DateTime',
                    y_labels=["Force (N/kg)"], 
                    freqs=[f_for], 
                    P=[Pxx_den_for],
                    columns_filtered=['Force_filtered'],
                    freqs_filtered=[f_for_filter], 
                    P_filtered=[Pxx_den_for_filter],
                    cutoffs=[cutoff_for],
                    fig_title='Force', 
                    graph_title="Force over time")






        ##### _____ EXPORT DATAFRAME _____ ##### ____________________________________________

        # Export a json file for these filters/computations
        if export_df['full']:
            if path_input[-4:] in ['.csv', '.json']:
                path_json_export = path_input[:-4]+'_'+str_datetime_start_process+'_TO_'+\
                                     str_datetime_end_process+'_full.json'
            else:
                path_json_export = path_input+'_'+str_datetime_start_process+'_TO_'+\
                                     str_datetime_end_process+'_full.json'
            
            df.to_json(path_json_export)
            print("\nDataframe exported at:", path_json_export)
        if export_df['small']:
            if path_input[-4:] in ['.csv', '.json']:
                path_json_export_v2 = path_input[:-4]+'_'+str_datetime_start_process+'_TO_'+\
                                     str_datetime_end_process+'_small.json'
            else:
                path_json_export_v2 = path_input+'_'+str_datetime_start_process+'_TO_'+\
                                     str_datetime_end_process+'_small.json'
            export_df_final(df, path_json_export_v2)
            print("\nDataframe exported at:", path_json_export_v2)






    ##### _____ RECAP PLOT FEW STATISTICS _____ ##### _______________________________________

    if dict_display_plots['Distributions (v-g)'] == True:
        plot_distribution_v_g(df)

    if dict_display_plots['Activity over day'] == True:
        plot_activity_over_day(df, is_moving_filtered)

    if dict_display_plots['Force-velocity'] == True:
        plot_force_velocity(df)


    # plt.show()



    ##### _____ SCAN VELOCITIES (time_window -> 2D record) _____ ##### ______________________

    if dict_computations['Scan record velocity 2D'] == True:
        print("\n\n\n#### SCAN VELOCITY 2D ####")
        start_scan_2D = time.time()

        t2, v2, t_start2, t_end2 = scan_records_2D(df, sampling_period, 
                                                   times_scan_v, idxs_start_moving, idxs_end_moving,
                                                   column_records='Velocity_filtered', column_time='DateTime')

        print("Time of the window (s):              ", t2)
        print("Velocity record vector (m/s):        ", v2)
        
        print("\n----  Velocity record 2D scanned  ----")
        time_scan_2D = time.time()-start_scan_2D
        print("Time for scanning record 2D velocity:     "+str(datetime.timedelta(seconds=(time_scan_2D))))

        # Plot 2D
        if dict_display_plots['Records 2D'] == True:
            plot_scan_2D_records(t2, v2, rec_label_axis='Velocity (m/s)')






    ##### _____ SCAN VELOCITIES (time_window and grade -> 3D record) _____ ##### ____________

    if dict_computations['Scan record velocity 3D'] == True:
        print("\n\n\n#### SCAN VELOCITY 3D ####")
        start_scan_3D = time.time()

        t3_vgt, g3_vgt, g3_vgt, t_start3, t_end3 = scan_records_3D(df, sampling_period, 
                                                    times_scan_v_g, grade_interv_borders_v_g,
                                                    threshold_std_g_scan_vgt,
                                                    idxs_start_moving, idxs_end_moving,
                                                    column_records='Velocity_filtered',
                                                    column_condition='Grade_filtered',
                                                    column_time='DateTime')

        print("\n----  Velocity record 3D scanned  ----")
        time_scan_3D = time.time()-start_scan_3D
        print("Time for scanning record 3D velocity:     "+str(datetime.timedelta(seconds=(time_scan_3D))))

        # Plot 3D records
        if dict_display_plots['Records 3D'] == True:
            plot_scan_3D_records(t3_vgt, g3_vgt, g3_vgt, cond_label_axis='Grade (%)',
                                                         rec_label_axis='Velocity (m/s)',
                                                         fig_name='Scan records velocity 3D')






    ##### _____ SCAN FORCES (time_window and velocity -> 3D record) _____ ##### ____________

    if dict_computations['Scan record force 3D'] == True:
        print("\n\n\n#### SCAN FORCE 3D ####")
        start_scan_fvt_3D = time.time()

        idxs_start_moving_fvt = np.copy(idxs_start_moving)
        idxs_start_moving_fvt[0] = max(idxs_start_moving[0], idx_first_force)
        idxs_end_moving_fvt = np.copy(idxs_end_moving)
        idxs_end_moving_fvt[-1] = min(idxs_end_moving[-1], idx_last_force)
        
        t3_fvt, v3_fvt, f3_fvt, t_start3_fvt, t_end3_fvt = scan_records_3D(df, sampling_period, 
                                                            times_scan_f_v, v_interv_borders_f_v,
                                                            threshold_std_v_scan_Fvt,
                                                            idxs_start_moving=idxs_start_moving_fvt,
                                                            idxs_end_moving=idxs_end_moving_fvt,
                                                            column_records='Force_filtered',
                                                            column_condition='Velocity_filtered',
                                                            column_time='DateTime')

        print("\n----  Force record 3D scanned  ----")
        time_scan_fvt_3D = time.time()-start_scan_fvt_3D
        print("Time for scanning record 3D force:     "+str(datetime.timedelta(seconds=(time_scan_fvt_3D))))

        # Plot 3D records
        if dict_display_plots['Records 3D force'] == True:
            plot_scan_3D_records(t3_fvt, v3_fvt, f3_fvt, cond_label_axis='Velocity (m/s)',
                                                         rec_label_axis='Force (N/kg)',
                                                         fig_name='Scan records force 3D')






    ##### _____ Export data record 3D for fitting _____ ##### _______________________________

    if export_results == True:
        date_export = datetime.datetime.now().strftime("D%Y%m%d-H%H%M%S")
        if path_input[-4] == '.':
            name_file_data_export = path_input[:-4]
        else:
            name_file_data_export = path_input
        name_file_data_export += '_' + str_datetime_start_process + '_TO_' + str_datetime_end_process
        
        # Export 2D
        if dict_computations['Scan record velocity 2D'] == True:
            name_export_2D = name_file_data_export + '_' + 'Record2D_' + date_export + '.csv'
            with open(name_export_2D, 'w', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(['File: '+path_input])
                writer.writerow(['t', 'v', 't_start', 't_end'])
                for k in range(len(t2)):
                    writer.writerow([t2[k], v2[k], t_start2[k], t_end2[k]])
        # Export 3D
        if dict_computations['Scan record velocity 3D'] == True:
            name_export_3D = name_file_data_export + '_' + 'Record3D_' + date_export + '.csv'
            with open(name_export_3D, 'w', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(['File: '+path_input])
                writer.writerow(['t', 'g', 'v', 't_start', 't_end'])
                for k in range(len(t3_vgt)):
                    writer.writerow([t3_vgt[k], g3_vgt[k], g3_vgt[k], t_start3[k], t_end3[k]])
        # Export 3D fvt
        if dict_computations['Scan record force 3D'] == True:
            name_export_3D = name_file_data_export + '_' + 'RecordFvt_' + date_export + '.csv'
            with open(name_export_3D, 'w', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(['File: '+path_input])
                writer.writerow(['t', 'v', 'F', 't_start', 't_end'])
                for k in range(len(t3_fvt)):
                    writer.writerow([t3_fvt[k], v3_fvt[k], f3_fvt[k], t_start3_fvt[k], t_end3_fvt[k]])
        # Export parameters
        name_export_param = name_file_data_export + '_' +'param_' + date_export + '.csv'
        with open(name_export_param, 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['File: '+path_input])
            writer.writerow(['Sampling Period (s)', sampling_period])
            writer.writerow(['Interpolation method', interp_method])
            writer.writerow(['Threshold for std on grade in the 3D scan', threshold_std_g_scan_vgt])
            writer.writerow(['Threshold Moving', threshold_moving])
            writer.writerow(['Cutoff Moving (Hz)', cutoff_mov])
            writer.writerow(['Order Moving', order_mov])
            writer.writerow(['Cutoff Longitude (Hz)', cutoff_lon])
            writer.writerow(['Order Longitude', order_lon])
            writer.writerow(['Cutoff Latitude (Hz)', cutoff_lat])
            writer.writerow(['Order Latitude', order_lat])
            writer.writerow(['Cutoff Grade (Hz)', cutoff_gra])
            writer.writerow(['Order Grade', order_gra])
            writer.writerow(['Cutoff Velocity (Hz)', cutoff_vel])
            writer.writerow(['Order Velocity', order_vel])
            writer.writerow(['Cutoff Force (Hz)', cutoff_for])
            writer.writerow(['Order Force', order_for])
            







    ##### _____ Print times of execution _____ ##### ________________________________________

    print("\n\n\n\n--- Total time of execution ---\n"+str(datetime.timedelta(seconds=(time.time()-start_import)))+'\n\n')
    print('Time to    import:                    ', datetime.timedelta(seconds=time_import))
    if type_import in ['csv', 'json interpolated']:
        print('Time to    filter behaviour:          ', datetime.timedelta(seconds=time_filter_behaviour))
        print('Time to    filter longitude-latitude: ', datetime.timedelta(seconds=time_filter_lonlat))
        print('Time to    compute topography:        ', datetime.timedelta(seconds=time_comp_topo))
        print('Time to    compute grade:             ', datetime.timedelta(seconds=time_compute_grade))
        print('Time to    filter grade:              ', datetime.timedelta(seconds=time_filter_grade))
        print('Time to    compute velocity:          ', datetime.timedelta(seconds=time_compute_vel))
        print('Time to    filter velocity:           ', datetime.timedelta(seconds=time_filter_vel))
        print('Time to    compute force:             ', datetime.timedelta(seconds=time_compute_force))
        print('Time to    filter force:              ', datetime.timedelta(seconds=time_filter_force))
    if dict_computations['Scan record velocity 2D'] == True:
        print('Time to    scan 2D records:           ', datetime.timedelta(seconds=time_scan_2D))
    if dict_computations['Scan record velocity 3D'] == True:
        print('Time to    scan 3D records:           ', datetime.timedelta(seconds=time_scan_3D))


    plt.show()



    ##### _____ Display final plots _____ ##### _____________________________________________
    poss_next_plots = ['Trajectory 2D', 'Trajectory 3D', 'Longitude-Latitude',
                    'Grade', 'Velocity', 'Force', 
                    'Components of force', 'Behaviour', 'Force-velocity', 
                    'Scan record velocity 2D', 'Scan record velocity 3D',
                    'Scan record force 3D',
                    'Velocity/grade distribution', 'Activity over day']
    if show_final_graphs == True:
        win_final = FinalNextPlot(poss_next_plots)
        win_final.mainloop()
        graph_display = win_final.next_display_graph
        show_final_graphs = win_final.show_next_graphs
    start, end = 0, min(max_len_plot, df.shape[0]-2)
    while show_final_graphs:
        show_final_graphs = False
        if graph_display == 'Trajectory 2D':
            win_plot = InterfaceTrajectory2DSimple(df, (start, end), max_len_plot)
            show()
        elif graph_display == 'Trajectory 3D':
            win_plot = InterfaceTrajectory3D(df, (start, end), max_len_plot, 
                                                    x_grid, y_grid, elev_grid)
            win_plot.mainloop()
        elif graph_display == 'Longitude-Latitude':
            win_plot = InterfaceFiltering(vars=['longitude', 'latitude'], window_title='Filter lon-lat',
                        fs=fs, df = df,  columns=['Longitude', 'Latitude'],
                        times='DateTime', idxs_time_limits=(start, end),
                        max_len_plot=max_len_plot, y_labels=["Longitude (°)", "Latitude (°)"],  
                        freqs=[f_lon, f_lat], P=[Pxx_den_lon, Pxx_den_lat], cutoffs=[cutoff_lon, cutoff_lat],
                        orders=[order_lon, order_lat], fig_title='Longitude-Latitude', graph_title="Lon-lat over time")
            show()
        elif graph_display == 'Grade':
            win_plot = InterfaceFiltering(vars=['grade'], window_title='Filter grade', 
                        fs=fs, df = df,  columns=['Grade'],
                        times='DateTime', idxs_time_limits=(max(start, idx_first_grade), min(end, idx_last_grade)),
                        max_len_plot=max_len_plot, y_labels=["Grade (%)"],  freqs=[f_gra],  P=[Pxx_den_gra],
                        cutoffs=[cutoff_gra], orders=[order_gra], fig_title='Grade',  graph_title="Grade over time")
            show()
        elif graph_display == 'Velocity':
            win_plot = InterfaceFiltering(vars=['velocity'], window_title='Filter velocity', 
                        fs=fs, df = df, columns=['Velocity'], times='DateTime',
                        idxs_time_limits=(max(1, start), end), max_len_plot=max_len_plot,
                        y_labels=["Velocity (m/s)"],  freqs=[f_vel],  P=[Pxx_den_vel], cutoffs=[cutoff_vel], 
                        orders=[order_vel], fig_title='Velocity', graph_title="Velocity over time")
            show()
        elif graph_display == 'Force':
            win_plot = InterfaceFiltering(vars=['force'], window_title='Filter force',
                        fs=fs, df = df, columns=['Force'], times='DateTime',
                        idxs_time_limits=(max(1, idx_first_force),min(idx_last_force, df.shape[0]-1)),
                        max_len_plot=max_len_plot,
                        y_labels=["Force (N/kg)"], freqs=[f_for], P=[Pxx_den_for], cutoffs=[cutoff_for],
                        orders=[order_for], fig_title='Force', graph_title="Force over time")
            show()
        elif graph_display == 'Components of force':
            plot_components_force(df, force_loc, force_gra, force_acc)
            plt.show()
        elif graph_display == 'Behaviour':
            win_plot = InterfaceFilteringBehaviour(fs=fs, df=df, col_behav=['Behaviour'], 
                        times='DateTime', idxs_time_limits=(0,min(max_len_plot, df.shape[0]-1)),
                        max_len_plot=max_len_plot, cutoff=cutoff_mov,
                        order=order_mov, threshold_moving=threshold_moving)
            show()
        elif graph_display == 'Force-velocity':
            plot_force_velocity(df)
            plt.show()
        elif graph_display == 'Scan record velocity 2D':
            plot_scan_2D_records(t2, v2, rec_label_axis='Velocity (m/s)')
            plt.show()
        elif graph_display == 'Scan record velocity 3D':
            plot_scan_3D_records(t3_fvt, v3_fvt, f3_fvt, cond_label_axis='Velocity (m/s)',
                                                         rec_label_axis='Force (N/kg)',
                                                         fig_name='Scan records velocity 3D')
            plt.show()
        elif graph_display == 'Scan record force 3D':
            plot_scan_3D_records(t3_fvt, v3_fvt, f3_fvt, cond_label_axis='Velocity (m/s)',
                                                         rec_label_axis='Force (N/kg)',
                                                         fig_name='Scan records force 3D')
            plt.show()
        elif graph_display == 'Velocity/grade distribution':
            plot_distribution_v_g(df)            
            plt.show()
        elif graph_display == 'Activity over day':
            plot_activity_over_day(df, is_moving_filtered)
            plt.show()
        if graph_display not in ('Components of force', 'Force-velocity',
                                 'Scan record velocity 2D', 'Scan record velocity 3D', 
                                 'Scan record force 3D',
                                 'Velocity/grade distribution', 'Activity over day'):
            start, end = win_plot.idxs_time_limits
        
        win_final = FinalNextPlot(poss_next_plots)
        win_final.mainloop()
        graph_display = win_final.next_display_graph
        show_final_graphs = win_final.show_next_graphs
