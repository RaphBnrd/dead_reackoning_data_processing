import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from lmfit import Model, Parameters

import datetime
import tkinter as tk


"""
Look at this for the method : 
https://lmfit.github.io/lmfit-py/model.html
https://stackoverflow.com/questions/59889441/prediction-intervals-for-hyperbolic-curve-fit-scipy

examples here: https://github.com/lmfit/lmfit-py/tree/master/examples
"""




##### _____ DEFAULT VARIABLES _____ #####
export = False
# export = True
interactive_entry = False

# name_file_export = 'data/20220621_data_1_month/fitting2/Ibex9_modInv'
name_file_export = 'data/20220627_data_1_month/fit_Ibex1_modInv'
g_min_tresh, g_max_tresh = -40, 40  
t_min_tresh, t_max_tresh = 150, np.inf
v_min_tresh, v_max_tresh = 0.3, np.inf
# file_name = 'data/scans_20220613/Ibex1_complete_v2_Record3D_D20220613-H142346.csv' # Ibex 1 (1 mois)

# file_name = 'data/20220621_data_1_month/rec3D/Ibex1_complete_v2_Record3D_D20220621-H164803.csv'
# file_name = 'data/20220621_data_1_month/rec3D/Ibex2_complete_v2_Record3D_D20220621-H165311.csv'
# file_name = 'data/20220621_data_1_month/rec3D/Ibex3_complete_v2_Record3D_D20220621-H165933.csv'
# file_name = 'data/20220621_data_1_month/rec3D/Ibex6_complete_v2_Record3D_D20220621-H170758.csv'
# file_name = 'data/20220621_data_1_month/rec3D/Ibex8_complete_v2_Record3D_D20220621-H171419.csv'
# file_name = 'data/20220621_data_1_month/rec3D/Ibex9_complete_v2_Record3D_D20220621-H172158.csv'

file_name = 'data/20220627_data_1_month/Ibex1_complete_v2_Record3D_D20220627-H135425.csv'
# file_name = 'data/20220704_open_sample_fvt/Ibex1/0-fitting all/Ibex1_complete_v2_20170602-000000_TO_20170701-000000_Record3D_D20220701-H171047.csv'
# file_name = 'data/20220628_data_1_month/rec3D/Ibex1_complete_v2_Record3D_D20220629-H110903.csv'

model_chosen = 'inverse'
# model_chosen = 'with k'




##### _____ INPUTS _____ #####
if interactive_entry:
    def button_select_csv():
        filetypes = (('csv files', '*.csv'), ('All files', '*.*'))
        var_file_name.set(tk.filedialog.askopenfilename(title='Select a file', initialdir='', filetypes=filetypes))
    win_entry = tk.Tk()
    win_entry.title('Import csv')
    # Input file
    var_file_name = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_file_name, width=50).grid(row=1, column=1, columnspan=3, padx=10, pady=10)
    var_file_name.set(file_name)
    tk.Button(win_entry, text='Browse', command=button_select_csv).grid(row=1, column=4, padx=10, pady=10)
    # Entry thresholds grades
    tk.Label(win_entry, text="Threshold grade (min-max): ").grid(row=2, column=1, sticky=tk.W, padx=10, pady=10)
    var_g_min_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_g_min_tresh, width=5).grid(row=2, column=2, padx=10, pady=10)
    var_g_min_tresh.set(g_min_tresh)
    var_g_max_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_g_max_tresh, width=5).grid(row=2, column=3, padx=10, pady=10)
    var_g_max_tresh.set(g_max_tresh)
    # Entry thresholds times
    tk.Label(win_entry, text="Threshold time (min-max): ").grid(row=3, column=1, sticky=tk.W, padx=10, pady=10)
    var_t_min_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_t_min_tresh, width=5).grid(row=3, column=2, padx=10, pady=10)
    var_t_min_tresh.set(t_min_tresh)
    var_t_max_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_t_max_tresh, width=5).grid(row=3, column=3, padx=10, pady=10)
    var_t_max_tresh.set(t_max_tresh)
    # Entry thresholds velocities
    tk.Label(win_entry, text="Threshold velocity (min-max): ").grid(row=4, column=1, sticky=tk.W, padx=10, pady=10)
    var_v_min_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_v_min_tresh, width=5).grid(row=4, column=2, padx=10, pady=10)
    var_v_min_tresh.set(v_min_tresh)
    var_v_max_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_v_max_tresh, width=5).grid(row=4, column=3, padx=10, pady=10)
    var_v_max_tresh.set(v_max_tresh)
    # Model chosen
    var_model_chosen = tk.StringVar()
    var_model_chosen.set(model_chosen)
    tk.Radiobutton(win_entry, variable=var_model_chosen, value='inverse', 
                    text="Inverse model"
                    ).grid(row=5, column=1, columnspan=2, pady=20)
    tk.Radiobutton(win_entry, variable=var_model_chosen, value='with k', 
                    text="Model with k"
                    ).grid(row=5, column=3, columnspan=2, pady=20)
    # Export?
    var_export = tk.BooleanVar()
    var_export.set(export)
    tk.Checkbutton(win_entry, variable = var_export,onvalue = True, 
                   text="Export results and figures", offvalue = False
                   ).grid(row=6, column=1, columnspan=4, pady=10)
    # Validate button
    tk.Button(win_entry, text='Launch fitting', command=win_entry.destroy
                ).grid(row=7, column=1, columnspan=4, pady=30)
    win_entry.mainloop()




    # File with result
    file_name = var_file_name.get()
    print(file_name)

    # Thresholds
    g_min_tresh = float(var_g_min_tresh.get())
    g_max_tresh = float(var_g_max_tresh.get())
    t_min_tresh = float(var_t_min_tresh.get())
    t_max_tresh = float(var_t_max_tresh.get())
    v_min_tresh = float(var_v_min_tresh.get())
    v_max_tresh = float(var_v_max_tresh.get())

    # Model chosen
    model_chosen = var_model_chosen.get()

    # Export
    export = var_export.get()
    





# origin file of data GPS-DR
file_name_origin_data = pd.read_csv(file_name, nrows=1, header=None)[0][0]


print("\n"+file_name_origin_data+"\n")
print('Pente entre', g_min_tresh, 'et', g_max_tresh, '%')
print('Temps entre', t_min_tresh, 'et', t_max_tresh, 's')
print('Vitesse entre', v_min_tresh, 'et', v_max_tresh, 'm/s')
print("Modèle choisi:", model_chosen)
print("Export des résultats ?", export)




##### _____ START EXECUTION _____ #####
df = pd.read_csv(file_name, header=1)
rows_delete = df[(df['t']<t_min_tresh) | (df['t']>t_max_tresh) | \
                 (df['g']<g_min_tresh) | (df['g']>g_max_tresh) | \
                 (df['v']<v_min_tresh) | (df['v']>v_max_tresh)].index
grade_delete = df['g'][rows_delete]
speed_delete = df['v'][rows_delete]
duration_delete = df['t'][rows_delete]

df.drop(rows_delete, inplace=True)
grade = df.g.values
speed = df.v.values
duration = df.t.values
print(df.head())

fig = plt.figure('Scan records 3D', figsize=(9, 6.5))
ax = plt.axes(projection='3d')
ax.scatter3D(duration, grade, speed, label='data kept')
ax.scatter3D(duration_delete, grade_delete, speed_delete, marker='X', color='orange', label='data ignored')
ax.set_xlabel('Time (s)')
ax.set_ylabel('Grade (%)')
ax.set_zlabel('Velocity (m/s)')
ax.set_title("Records' surface")
ax.legend()

ax.view_init(elev=30, azim=-30)
if export:
    date_export = datetime.datetime.now().strftime("D%Y%m%d-H%H%M%S")
    name_export_fig = name_file_export + '_' + date_export + '_fig3D'
    plt.savefig(name_export_fig+'_0')








# Models
def function_model_inv(d, g, D, a, b, c):
    # function use for speed - grade - duration fitting
    return D/d + 1 / (a*g**2 + b*g + c)
    
def function_model_k(d, g, D, a, b, c, k):
    # function use for speed - grade - duration fitting
    return D/(d+k) + 1 / (a*g**2 + b*g + c)





# figure for visualization
fig = plt.figure('fitting', figsize=(9, 6.5))
ax = plt.axes(projection='3d')

# fitting model configuration___________________________________________________________________________________________
if model_chosen == 'inverse':
    model = Model(function_model_inv, independent_vars=['d', 'g'])
elif model_chosen == 'with k':
    model = Model(function_model_k, independent_vars=['d', 'g'])

D0, a0, b0, c0 = 100, 0.05, 0, 5 # set the starting point for the fitting
apex_min, apex_max = -20, 20
delta_max = 0
D_min, a_min = 0, 0
params = Parameters()
params.add('apex', value=-b0 / (2*a0), min=apex_min, max=apex_max)  # apex is located between grade of -20 and +20%
params.add('delta',  value=b0**2 - 4*a0*c0, max=delta_max)  # delta = b²-4ac < 0 => the polynomial function is strictly positive
params.add('D', value=D0,  min=D_min)
params.add('a', value=a0,  min=a_min)
params.add('b', expr='-apex*2*a')  # b = apex*2*a
params.add('c', expr='(b ** 2 - delta) / (4 * a)')  # c = (b²-delta) / (4a)

if model_chosen == 'with k':
    k0 = 30
    k_min, k_max = 0, 800
    params.add('k', value=k0,  min=k_min, max=k_max)


# first step fitting with all record speed data_________________________________________________________________________
result1 = model.fit(speed, params, d=duration, g=grade)

# second step - removing outlier and fit again__________________________________________________________________________
residuals = result1.residual
z_residuals = (residuals - np.mean(residuals)) / np.std(residuals)  # z-score of the residuals
z_threshold = 2  # threshold to classify as outlier (2 = 95%; 3 = 99%)
is_outlier = (abs(z_residuals) > z_threshold)
ax.scatter3D(duration[is_outlier], grade[is_outlier], speed[is_outlier],
             marker='X', color='red', label='outliers')  # plot for visualization
# remove outlier for grade, speed and duration
grade2 = grade[~is_outlier]
speed2 = speed[~is_outlier]
duration2 = duration[~is_outlier]
result2 = model.fit(speed2, params, d=duration2, g=grade2)  # fit again without outliers

# third step only with positive residuals_______________________________________________________________________________
# reset fitting parameters so starting points are the coeficients determined from step 2 and boundaries
# correspond to CI95% of the parameters
params['D'].value = result2.params['D'].value
params['a'].value = result2.params['a'].value
params['apex'].value = result2.params['apex'].value
params['delta'].value = result2.params['delta'].value
params['D'].min = max(D_min, params['D'].value - 2 * result2.params['D'].stderr)
params['D'].max = params['D'].value + 2 * result2.params['D'].stderr
params['a'].min = max(a_min, params['a'].value - 2 * result2.params['a'].stderr)
params['a'].max = params['a'].value + 2 * result2.params['a'].stderr
params['apex'].min = max(apex_min, params['apex'].value - 2 * result2.params['apex'].stderr)
params['apex'].max = min(apex_max, params['apex'].value + 2 * result2.params['apex'].stderr)
params['delta'].min = params['delta'].value - 2 * result2.params['delta'].stderr
params['delta'].max = min(delta_max, params['delta'].value + 2 * result2.params['delta'].stderr)
if model_chosen == 'with k':
    params['k'].value = result2.params['k'].value
    params['k'].min = max(k_min, params['k'].value - 2 * result2.params['k'].stderr)
    params['k'].max = min(k_max, params['k'].value + 2 * result2.params['k'].stderr)

#remove negative residuals
residuals2 = result2.residual
# /!\ positive residuals correspond to residual < 0 because lmfit calculate residuals as predicted - observed
is_positive_res = (residuals2 < 0)
ax.scatter3D(duration2[~is_positive_res], grade2[~is_positive_res], speed2[~is_positive_res],
             marker='X', color='blue', label='negative residual')   # plot for visualization
# conserve only positive residuals for grade, speed and duration
grade3 = grade2[is_positive_res]
speed3 = speed2[is_positive_res]
duration3 = duration2[is_positive_res]
result3 = model.fit(speed3, params, d=duration3, g=grade3)

# print the report of all 3 fittings____________________________________________________________________________________
print('\n\n\nStep 1:')
print(result1.fit_report())
print('\n\n\nStep 2:')
print(result2.fit_report())
print('\n\n\nStep 3:')
print(result3.fit_report())

# plot the results for visualization ___________________________________________________________________________________
d_grid = np.arange(min(duration), max(duration), 10)
g_grid = np.arange(min(grade), max(grade), 1)
d_grid, g_grid = np.meshgrid(d_grid, g_grid)
if model_chosen == 'inverse':
    s_surf = model.eval(d=d_grid, g=g_grid,
                    D=result3.params['D'].value,
                    a=result3.params['a'].value,
                    b=result3.params['b'].value,
                    c=result3.params['c'].value)
elif model_chosen == 'with k':
    s_surf = model.eval(d=d_grid, g=g_grid,
                    D=result3.params['D'].value,
                    a=result3.params['a'].value,
                    b=result3.params['b'].value,
                    c=result3.params['c'].value,
                    k=result3.params['k'].value)
    

ax.scatter3D(duration3, grade3, speed3, color='black', label='remaining data')
ax.legend()
surf1 = ax.plot_surface(d_grid, g_grid, s_surf,
                        cmap=cm.jet,
                        label='fits(d,g)',
                        alpha=0.5,
                        linewidth=0,
                        antialiased=True)
ax.set_xlabel('Duration (s)')
ax.set_ylabel('Grade (%)')
ax.set_zlabel('Speed (m/s)')
ax.set_title("Speed - Grade - Duration relationship\n"+file_name_origin_data)



if export: 
    # Export parameters
    name_export_param = name_file_export + '_' + date_export + '_fitting.txt'
    with open(name_export_param, 'w') as f:
        f.write("Original file (datas GPS-DR)"+file_name_origin_data+"\n")
        f.write('Pente entre ' + str(g_min_tresh) + ' et ' + str(g_max_tresh) + ' %\n')
        f.write('Temps entre ' + str(t_min_tresh) + ' et ' + str(t_max_tresh) + ' s\n')
        f.write('Vitesse entre ' + str(v_min_tresh) + ' et ' + str(v_max_tresh) + ' m/s\n')
        f.write("Modèle choisi: " + model_chosen + '\n')
        f.write('\n\n\n\nStep 1:\n')
        f.write(result1.fit_report())
        f.write('\n\n\n\nStep 2:\n')
        f.write(result2.fit_report())
        f.write('\n\n\n\nStep 3:\n')
        f.write(result3.fit_report())
    # Save fig
    for i in range(5):
        ax.view_init(elev=20, azim=-70+35*i)
        plt.savefig(name_export_fig+'_'+str(i+2))
    ax.view_init(elev=0, azim=-90)
    plt.savefig(name_export_fig+'_1')
    ax.view_init(elev=0, azim=0)
    plt.savefig(name_export_fig+'_7')
    

ax.view_init(elev=20, azim=-40)
plt.show()
