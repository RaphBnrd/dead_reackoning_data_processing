import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from lmfit import Model, Parameters
import datetime
from tqdm import tqdm
import os



##### _____ DEFAULT VARIABLES _____ #####
export = False

g_min_tresh, g_max_tresh = -40, 40  
t_min_tresh, t_max_tresh = 150, np.inf
v_min_tresh, v_max_tresh = 0.3, np.inf

num_ibex = '2'
folder_name = 'data/20220704_open_sample_fvt/Ibex'+num_ibex+'/Rec3D/'
all_files_name = np.char.array(os.listdir(folder_name))

files_name = folder_name + all_files_name[[i%1==0 for i in range(len(all_files_name))]] # even files
name_file_export = 'data/20220704_open_sample_fvt/Ibex'+num_ibex+'/1-fitting_vgt even/Ibex'+num_ibex+'_even'
# files_name = folder_name + all_files_name[[i%2==1 for i in range(len(all_files_name))]] # odd files
# name_file_export = 'data/20220704_open_sample_fvt/Ibex'+num_ibex+'/1-fitting_vgt odd/Ibex'+num_ibex+'_odd'

model_chosen = 'inverse'
# model_chosen = 'with k'


# origin file of data GPS-DR
file_name_origin_data = pd.read_csv(files_name[0], nrows=1, header=None)[0][0]


print("\n"+file_name_origin_data+"\n")
print('Pente entre', g_min_tresh, 'et', g_max_tresh, '%')
print('Temps entre', t_min_tresh, 'et', t_max_tresh, 's')
print('Vitesse entre', v_min_tresh, 'et', v_max_tresh, 'm/s')
print("Modèle choisi:", model_chosen)
print("Export des résultats ?", export)




# init compiled records
duration_ini = np.array([5, 15, 30, 60, 90] + [k*60 for k in range(2, 31)])
grade_interv_borders = np.array([i for i in range(-50, 51, 2)])
grade_ini = (grade_interv_borders[1:] + grade_interv_borders[:-1]) / 2
duration_full, grade_full = np.meshgrid(duration_ini, grade_ini)
duration_full, grade_full = duration_full.flatten(), grade_full.flatten()
# [t1, t2, ... tn, ... t1, t2, ... tn]
# [g1, g1, g1, ..., g1, g2, g2,  ... gn, gn, ... gn, gn]
speed_full = np.ones(duration_full.shape)*np.nan

##### _____ START EXECUTION _____ #####
for file in tqdm(files_name):
    df = pd.read_csv(file, header=1)
    rows_delete = df[(df['t']<t_min_tresh) | (df['t']>t_max_tresh) | \
                     (df['g']<g_min_tresh) | (df['g']>g_max_tresh) | \
                     (df['v']<v_min_tresh) | (df['v']>v_max_tresh)].index
    duration_delete = df['t'][rows_delete]
    grade_delete = df['g'][rows_delete]
    speed_delete = df['v'][rows_delete]

    df.drop(rows_delete, inplace=True)
    this_duration = df.t.values
    this_grade = df.g.values
    this_speed = df.v.values
    
    for k in range(len(this_speed)):
        idx_duration = np.where(duration_ini == this_duration[k])[0][0]
        idx_grade = np.where((grade_interv_borders[:-1] <= this_grade[k]) & \
                             (grade_interv_borders[1:] >= this_grade[k]))[0][0]
        idx_full = idx_duration + duration_ini.size * idx_grade
        if not(np.isfinite(speed_full[idx_full])):
            speed_full[idx_full] = this_speed[k]
            grade_full[idx_full] = this_grade[k]
        elif this_speed[k] >= speed_full[idx_full]:
            speed_full[idx_full] = this_speed[k]
            grade_full[idx_full] = this_grade[k]

idx_not_na = np.where(np.isfinite(speed_full))[0]
speed = speed_full[idx_not_na]
grade = grade_full[idx_not_na]
duration = duration_full[idx_not_na]

print(list(duration[:10]))
print(list(grade[:10]))
print(list(speed[:10]))

fig = plt.figure('Scan records 3D', figsize=(9, 6.5))
ax = plt.axes(projection='3d')
ax.scatter3D(duration, grade, speed, label='data kept')
ax.scatter3D(duration_delete, grade_delete, speed_delete, marker='X', color='orange', label='data ignored')
ax.set_xlabel('Time (s)')
ax.set_ylabel('Grade (%)')
ax.set_zlabel('Velocity (m/s)')
ax.set_title("Records' surface")
ax.legend()

ax.view_init(elev=30, azim=-30)
if export:
    date_export = datetime.datetime.now().strftime("D%Y%m%d-H%H%M%S")
    name_export_fig = name_file_export + '_' + date_export + '_fig3D'
    plt.savefig(name_export_fig+'_0')








# Models
def function_model_inv(d, g, D, a, b, c):
    # function use for speed - grade - duration fitting
    return D/d + 1 / (a*g**2 + b*g + c)
    
def function_model_k(d, g, D, a, b, c, k):
    # function use for speed - grade - duration fitting
    return D/(d+k) + 1 / (a*g**2 + b*g + c)





# figure for visualization
fig = plt.figure('fitting', figsize=(9, 6.5))
ax = plt.axes(projection='3d')

# fitting model configuration___________________________________________________________________________________________
if model_chosen == 'inverse':
    model = Model(function_model_inv, independent_vars=['d', 'g'])
elif model_chosen == 'with k':
    model = Model(function_model_k, independent_vars=['d', 'g'])

D0, a0, b0, c0 = 100, 0.05, 0, 5 # set the starting point for the fitting
apex_min, apex_max = -20, 20
delta_max = 0
D_min, a_min = 0, 0
params = Parameters()
params.add('apex', value=-b0 / (2*a0), min=apex_min, max=apex_max)  # apex is located between grade of -20 and +20%
params.add('delta',  value=b0**2 - 4*a0*c0, max=delta_max)  # delta = b²-4ac < 0 => the polynomial function is strictly positive
params.add('D', value=D0,  min=D_min)
params.add('a', value=a0,  min=a_min)
params.add('b', expr='-apex*2*a')  # b = apex*2*a
params.add('c', expr='(b ** 2 - delta) / (4 * a)')  # c = (b²-delta) / (4a)

if model_chosen == 'with k':
    k0 = 30
    k_min, k_max = 0, 800
    params.add('k', value=k0,  min=k_min, max=k_max)


# first step fitting with all record speed data_________________________________________________________________________
result1 = model.fit(speed, params, d=duration, g=grade)

# second step - removing outlier and fit again__________________________________________________________________________
residuals = result1.residual
z_residuals = (residuals - np.mean(residuals)) / np.std(residuals)  # z-score of the residuals
z_threshold = 2  # threshold to classify as outlier (2 = 95%; 3 = 99%)
is_outlier = (abs(z_residuals) > z_threshold)
ax.scatter3D(duration[is_outlier], grade[is_outlier], speed[is_outlier],
             marker='X', color='red', label='outliers')  # plot for visualization
# remove outlier for grade, speed and duration
grade2 = grade[~is_outlier]
speed2 = speed[~is_outlier]
duration2 = duration[~is_outlier]
result2 = model.fit(speed2, params, d=duration2, g=grade2)  # fit again without outliers

# third step only with positive residuals_______________________________________________________________________________
# reset fitting parameters so starting points are the coeficients determined from step 2 and boundaries
# correspond to CI95% of the parameters
params['D'].value = result2.params['D'].value
params['a'].value = result2.params['a'].value
params['apex'].value = result2.params['apex'].value
params['delta'].value = result2.params['delta'].value
params['D'].min = max(D_min, params['D'].value - 2 * result2.params['D'].stderr)
params['D'].max = params['D'].value + 2 * result2.params['D'].stderr
params['a'].min = max(a_min, params['a'].value - 2 * result2.params['a'].stderr)
params['a'].max = params['a'].value + 2 * result2.params['a'].stderr
params['apex'].min = max(apex_min, params['apex'].value - 2 * result2.params['apex'].stderr)
params['apex'].max = min(apex_max, params['apex'].value + 2 * result2.params['apex'].stderr)
params['delta'].min = params['delta'].value - 2 * result2.params['delta'].stderr
params['delta'].max = min(delta_max, params['delta'].value + 2 * result2.params['delta'].stderr)
if model_chosen == 'with k':
    params['k'].value = result2.params['k'].value
    params['k'].min = max(k_min, params['k'].value - 2 * result2.params['k'].stderr)
    params['k'].max = min(k_max, params['k'].value + 2 * result2.params['k'].stderr)

#remove negative residuals
residuals2 = result2.residual
# /!\ positive residuals correspond to residual < 0 because lmfit calculate residuals as predicted - observed
is_positive_res = (residuals2 < 0)
ax.scatter3D(duration2[~is_positive_res], grade2[~is_positive_res], speed2[~is_positive_res],
             marker='X', color='blue', label='negative residual')   # plot for visualization
# conserve only positive residuals for grade, speed and duration
grade3 = grade2[is_positive_res]
speed3 = speed2[is_positive_res]
duration3 = duration2[is_positive_res]
result3 = model.fit(speed3, params, d=duration3, g=grade3)

# print the report of all 3 fittings____________________________________________________________________________________
print('\n\n\nStep 1:')
print(result1.fit_report())
print('\n\n\nStep 2:')
print(result2.fit_report())
print('\n\n\nStep 3:')
print(result3.fit_report())

# plot the results for visualization ___________________________________________________________________________________
d_grid = np.arange(min(duration), max(duration), 10)
g_grid = np.arange(min(grade), max(grade), 1)
d_grid, g_grid = np.meshgrid(d_grid, g_grid)
if model_chosen == 'inverse':
    s_surf = model.eval(d=d_grid, g=g_grid,
                    D=result3.params['D'].value,
                    a=result3.params['a'].value,
                    b=result3.params['b'].value,
                    c=result3.params['c'].value)
elif model_chosen == 'with k':
    s_surf = model.eval(d=d_grid, g=g_grid,
                    D=result3.params['D'].value,
                    a=result3.params['a'].value,
                    b=result3.params['b'].value,
                    c=result3.params['c'].value,
                    k=result3.params['k'].value)
    

ax.scatter3D(duration3, grade3, speed3, color='black', label='remaining data')
ax.legend()
surf1 = ax.plot_surface(d_grid, g_grid, s_surf,
                        cmap=cm.jet,
                        label='fits(d,g)',
                        alpha=0.5,
                        linewidth=0,
                        antialiased=True)
ax.set_xlabel('Duration (s)')
ax.set_ylabel('Grade (%)')
ax.set_zlabel('Speed (m/s)')
ax.set_title("Speed - Grade - Duration relationship\n"+file_name_origin_data)



if export: 
    # Export parameters
    name_export_param = name_file_export + '_' + date_export + '_fitting.txt'
    with open(name_export_param, 'w') as f:
        f.write("Original file (datas GPS-DR)"+file_name_origin_data+"\n")
        f.write('Pente entre ' + str(g_min_tresh) + ' et ' + str(g_max_tresh) + ' %\n')
        f.write('Temps entre ' + str(t_min_tresh) + ' et ' + str(t_max_tresh) + ' s\n')
        f.write('Vitesse entre ' + str(v_min_tresh) + ' et ' + str(v_max_tresh) + ' m/s\n')
        f.write("Modèle choisi: " + model_chosen + '\n')
        f.write('\n\n\n\nStep 1:\n')
        f.write(result1.fit_report())
        f.write('\n\n\n\nStep 2:\n')
        f.write(result2.fit_report())
        f.write('\n\n\n\nStep 3:\n')
        f.write(result3.fit_report())
    # Save fig
    for i in range(5):
        ax.view_init(elev=20, azim=-70+35*i)
        plt.savefig(name_export_fig+'_'+str(i+2))
    ax.view_init(elev=0, azim=-90)
    plt.savefig(name_export_fig+'_1')
    ax.view_init(elev=0, azim=0)
    plt.savefig(name_export_fig+'_7')
    

ax.view_init(elev=20, azim=-40)
plt.show()
