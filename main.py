import time
import datetime
import numpy as np

print("\n\n")
print("  ## ## ## ## ## ## ## ## ## ##  ")
print("#### START ENTIRE EXECUTION  ####")
print("  #############################  ")
start_whole_execution = time.time()
print("Launch execution:   " + datetime.datetime.now().strftime("%d-%m-%Y  %H:%M:%S"))

from e_class_graphic_interfaces import WindowInitParam
from main_computations import launch_process






##### _____ DEFAULT VARIABLES _____ ##### _______________________________________________

with_graphic_interface = False


export_results = False
export_df = {'interp':False, 'full':False, 'small':False}
max_len_plot = int(2*86400) # pts (integer)

csv_type_columns='complete' # 'complete or corrected depending on the file we import (see more in detail the function "first_import" in import_functions.py)
datetime_start_file = '2017-06-02 00:00:00' # 1st datetime of the file
# datetime_start_file = '15/5/2017 14:00:48' # 1st datetime of the file for Ibex1 corrected
# datetime_start_file = '16/5/2017 13:1:20' # 1st datetime of the file for Ibex2 corrected
# datetime_start_file = '16/5/2017 13:1:20' # 1st datetime of the file for Ibex3 corrected
# datetime_start_file = '26/5/2017 12:1:14' # 1st datetime of the file for Ibex6 corrected
# datetime_start_file = '26/5/2017 18:1:19' # 1st datetime of the file for Ibex8 corrected
# datetime_start_file = '26/5/2017 14:1:6' # 1st datetime of the file for Ibex9 corrected
interv_datetime_process = ['2017-06-21 00:00:00',
                        #    '2017-06-21 03:00:00',
                        #    '2017-06-21 06:00:00',
                           '2017-06-21 09:00:00']
# interv_datetime_process = []
# for k in range(11, 31): # june
#     interv_datetime_process.append('2017-06-' + str(k).zfill(2) + ' 00:00:00')

path_input = '../Data Dead-reckoning/Ibex complete data/Ibex1_complete_v2.csv'
# path_input = '../Data Dead-reckoning/Ibex complete data/Ibex1_complete_v2_20170621-000000_TO_20170621-090000_interpolated.json'
# path_input = '../Data Dead-reckoning/Ibex complete data/Ibex1_complete_v2_20170621-000000_TO_20170621-090000_full.json'
# path_input = '../Data Dead-reckoning/DR paths Ibex/Ibex1_2017_GPScorrectedDRpath.csv'
# path_input = '../Data Dead-reckoning/DR paths Ibex/Ibex2_2017_GPScorrectedDRpath.csv'
# path_input = '../Data Dead-reckoning/DR paths Ibex/Ibex3_2017_GPScorrectedDRpath.csv'
# path_input = '../Data Dead-reckoning/DR paths Ibex/Ibex8_Georefrenced path.csv'
# path_input = '../Data Dead-reckoning/DR paths Ibex/Ibex9_Georefrenced_path_v2.csv'

# type_import = 'csv'
type_import = 'auto'

sampling_period = 1     # sec
interp_method = 'cubic' # Method of interpolation: 'linear' or 'cubic'
dict_computations = {'Scan record velocity 2D':True, 'Scan record velocity 3D':True,
                     'Scan record force 3D':True}
dict_display_plots = {'Behaviour':False, 'Latitude-Longitude':False,
                      'Trajectory 2D':False, 'Grade':False, 'Trajectory 3D':False,
                      'Velocity':False, 'Force':False, 'Components of force':False, 
                      'Distributions (v-g)':False,
                      'Activity over day':False, 'Force-velocity':False,
                      'Records 2D':False, 'Records 3D':False, 'Records 3D force':False}
show_final_graphs = True

cutoff_mov = 5e-4    # in Hz, cutoff for the filter on movement
order_mov = 1
cutoff_lon, cutoff_lat = 0.05, 0.05   # in Hz, cutoff for the filter on position
order_lon, order_lat = 1, 1
cutoff_gra = 0.2    # in Hz, cutoff for the filter on grade
order_gra = 1
cutoff_vel = 0.2    # in Hz, cutoff for the filter on velocity
order_vel = 1
cutoff_for = 0.2    # in Hz, cutoff for the filter on force
order_for = 1

threshold_moving = 0.05 # between 0 and 1
threshold_std_g_scan_vgt = 5 # % (of grade)
times_scan_v = [5, 15, 30, 60, 90] + [k*60 for k in range(2, 31)]
times_scan_v_g = [5, 15, 30, 60, 90] + [k*60 for k in range(2, 31)]
grade_interv_borders_v_g = [i for i in range(-50, 51, 2)]

threshold_std_v_scan_Fvt = 5 # m/s
times_scan_f_v = [5, 15, 30, 60, 90] + [k*60 for k in range(2, 31)]
v_interv_borders_f_v = list(np.arange(0, 4, 0.2)) + list(np.arange(4, 16, 1))





##### _____ INPUT PARAMETERS _____ ##### ________________________________________________

if type_import == 'auto':
    type_import = 'csv'
    if path_input[-5:] == '.json':
        if path_input[-10:] == '_full.json':
            type_import = 'json full'
        else:
            type_import = 'json interpolated'

if with_graphic_interface:
    ini_cutoffs = [cutoff_lon, cutoff_lat, cutoff_mov, cutoff_gra, cutoff_vel, cutoff_for]
    ini_orders = [order_lon, order_lat, order_mov, order_gra, order_vel, order_for]
    filters_data=['longitude', 'latitude', 'behaviour', 'grade', 'velocity', 'force']

    win1 = WindowInitParam(ini_cutoffs=ini_cutoffs, ini_orders=ini_orders, filters_data=filters_data, 
                            ini_computations=dict_computations, ini_display_plots=dict_display_plots,
                            ini_threshold=threshold_moving, ini_input=path_input, ini_export=export_results,
                            ini_export_df = export_df,
                            ini_type_import=type_import)
    win1.mainloop()

    # Get all entry parameters
    path_input = win1.filename_input.get()
    type_import = win1.value_type_import
    sampling_period = float(win1.samp_per.get())
    interp_method = win1.interp_meth.get()
    for computation in win1.check_computations:
        dict_computations[computation] = win1.check_computations[computation].get()
    export_results = win1.check_export.get()
    for exp_df in win1.check_export_df:
        export_df[exp_df] = win1.check_export_df[exp_df].get()
    for graph in win1.check_plots:
        dict_display_plots[graph] = win1.check_plots[graph].get()
    
    cutoff_lon, cutoff_lat, cutoff_mov = float(win1.cutoffs[0].get()), float(win1.cutoffs[1].get()), float(win1.cutoffs[2].get())
    cutoff_gra, cutoff_vel, cutoff_for = float(win1.cutoffs[3].get()), float(win1.cutoffs[4].get()), float(win1.cutoffs[5].get())

    order_lon, order_lat, order_mov = int(win1.orders[0].get()), int(win1.orders[1].get()), int(win1.orders[2].get())
    order_gra, order_vel, order_for = int(win1.orders[3].get()), int(win1.orders[4].get()), int(win1.orders[5].get())

    threshold_moving = float(win1.threshold.get())

    with_graphic_interface = win1.interactive.get()


print("\n-- Parameters --")
print("INPUT path:           ", path_input)
print("Sampling period (s):  ", sampling_period)
print("Interpolation method: ", interp_method)
print("Computations:         ", dict_computations)
print("Graph displayed:      ", dict_display_plots)
print("Export dataframe:     ", export_df)
print("Export results:       ", export_results)










##### _____ PROCESS _____ ##### _________________________________________________________

for k in range(len(interv_datetime_process)-1):
    datetime_start_process = interv_datetime_process[k]
    datetime_end_process = interv_datetime_process[k+1]
    print('\n\n\nFrom', datetime_start_process, 'TO', datetime_end_process)
    launch_process(with_graphic_interface=with_graphic_interface, export_results=export_results, export_df=export_df, 
                  path_input=path_input, type_import=type_import, csv_type_columns=csv_type_columns,
                  max_len_plot=max_len_plot, datetime_start_file=datetime_start_file,
                  datetime_start_process=datetime_start_process, datetime_end_process=datetime_end_process,
                  sampling_period=sampling_period, interp_method=interp_method, 
                  dict_computations=dict_computations, dict_display_plots=dict_display_plots,
                  show_final_graphs=show_final_graphs,
                  cutoff_mov=cutoff_mov, cutoff_lon=cutoff_lon, cutoff_lat=cutoff_lat,
                  cutoff_gra=cutoff_gra, cutoff_vel=cutoff_vel, cutoff_for=cutoff_for,
                  order_mov=order_mov, order_lon=order_lon, order_lat=order_lat,
                  order_gra=order_gra, order_vel=order_vel, order_for=order_for,
                  threshold_moving=threshold_moving, 
                  threshold_std_g_scan_vgt=threshold_std_g_scan_vgt,
                  times_scan_v=times_scan_v, times_scan_v_g=times_scan_v_g,
                  grade_interv_borders_v_g=grade_interv_borders_v_g, 
                  threshold_std_v_scan_Fvt=threshold_std_v_scan_Fvt,
                  times_scan_f_v=times_scan_f_v, v_interv_borders_f_v=v_interv_borders_f_v)
        




print("\n\n\n\n")
print("   # # # # # # # # # # # # # #   ")
print("#### END OF ENTIRE EXECUTION ####")
print("   ###########################   ")
total_time = time.time()-start_whole_execution
print("TIME FOR THE ENTIRE EXECUTION:     "+\
    str(datetime.timedelta(seconds=(total_time))))
