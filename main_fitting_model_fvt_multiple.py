import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from lmfit import Model, Parameters
import os
import datetime
from tqdm import tqdm





##### _____ DEFAULT VARIABLES _____ #####
export = False
show_plots = True

slope_min_threshold = -5 # %
# Threshold force = Cr*rendement + g*sin(atan(-slope_seuil))*abs(slope_seuil)/slope_seuil
f_min_tresh = 3.35*0.25 + 9.81*np.sin(np.arctan(slope_min_threshold)/100)
f_max_tresh = np.inf
t_min_tresh, t_max_tresh = 15, np.inf
v_min_tresh, v_max_tresh = 0.4, np.inf

num_ibex = '1corrected'
folder_name = 'data/20220704_open_sample_fvt/Ibex'+num_ibex+'/RecFvt/'
all_files_name = np.char.array(os.listdir(folder_name))

# files_name = folder_name + all_files_name[[i%2==0 for i in range(len(all_files_name))]] # even files
# name_file_export = 'data/20220704_open_sample_fvt/2-fitting even/Ibex'+num_ibex+'_even'
# files_name = folder_name + all_files_name[[i%2==1 for i in range(len(all_files_name))]] # odd files
# name_file_export = 'data/20220704_open_sample_fvt/2-fitting odd/Ibex'+num_ibex+'_odd'
files_name = folder_name + all_files_name # all files
name_file_export = 'data/20220704_open_sample_fvt/2-fitting all/Ibex'+num_ibex+'_all'

# model_chosen = 'inverse'
model_chosen = 'with k'


# origin file of data GPS-DR
file_name_origin_data = pd.read_csv(files_name[0], nrows=1, header=None)[0][0]


print("\n"+file_name_origin_data+"\n")
print('Temps entre', t_min_tresh, 'et', t_max_tresh, 's')
print('Vitesse entre', v_min_tresh, 'et', v_max_tresh, 'm/s')
print('Force entre', f_min_tresh, 'et', f_max_tresh, 'N/kg')
print("Modèle choisi:", model_chosen)
print("Export des résultats ?", export)




# init compiled records
duration_ini = np.array([5, 15, 30, 60, 90] + [k*60 for k in range(2, 31)])
speed_interv_borders = np.array(list(np.arange(0, 4, 0.2)) + list(np.arange(4, 16, 1)))
speed_ini = (speed_interv_borders[1:] + speed_interv_borders[:-1]) / 2
duration_full, speed_full = np.meshgrid(duration_ini, speed_ini)
duration_full, speed_full = duration_full.flatten(), speed_full.flatten()
# [t1, t2, ... tn, ... t1, t2, ... tn]
# [v1, v1, v1, ..., v1, v2, v2,  ... vn, vn, ... vn, vn]
force_full = np.ones(duration_full.shape)*np.nan
# t_start_full = np.chararray(force_full.shape, itemsize=len('2017-06-02 00:00:00'))
# t_end_full = np.chararray(force_full.shape, itemsize=len('2017-06-02 00:00:00'))

##### _____ START EXECUTION _____ #####
for file in tqdm(files_name):
    df = pd.read_csv(file, header=1)
    rows_delete = df[(df['t']<t_min_tresh) | (df['t']>t_max_tresh) | \
                     (df['F']<f_min_tresh) | (df['F']>f_max_tresh) | \
                     (df['v']<v_min_tresh) | (df['v']>v_max_tresh)].index
    force_delete = df['F'][rows_delete]
    speed_delete = df['v'][rows_delete]
    duration_delete = df['t'][rows_delete]
    # t_starts = df['t_start'][rows_delete]
    # t_ends = df['t_end'][rows_delete]

    df.drop(rows_delete, inplace=True)
    this_force = df.F.values
    this_speed = df.v.values
    this_duration = df.t.values
    # this_t_start = df.t_start.values
    # this_t_end = df.t_end.values
    
    for k in range(len(this_force)):
        idx_duration = np.where(duration_ini == this_duration[k])[0][0]
        idx_speed = np.where((speed_interv_borders[:-1] <= this_speed[k]) & \
                             (speed_interv_borders[1:] >= this_speed[k]))[0][0]
        idx_full = idx_duration + duration_ini.size * idx_speed
        if not(np.isfinite(force_full[idx_full])):
            force_full[idx_full] = this_force[k]
            speed_full[idx_full] = this_speed[k]
            # t_start_full[idx_full] = this_t_start[k]
            # t_end_full[idx_full] = this_t_end[k]
        elif this_force[k] >= force_full[idx_full]:
            force_full[idx_full] = this_force[k]
            speed_full[idx_full] = this_speed[k]
            # t_start_full[idx_full] = this_t_start[k]
            # t_end_full[idx_full] = this_t_end[k]

idx_not_na = np.where(np.isfinite(force_full))[0]
force = force_full[idx_not_na]
speed = speed_full[idx_not_na]
duration = duration_full[idx_not_na]
# t_start = t_start_full[idx_not_na]
# t_end = t_end_full[idx_not_na]
# print(t_start[force>10][:10])
# print(t_end[force>10][:10])
# print(force[force>10][:10])

print(len(duration), list(duration[:10]))
print(len(speed), list(speed[:10]))
print(len(force), list(force[:10]))

fig = plt.figure('Scan records 3D', figsize=(9, 6.5))
ax = plt.axes(projection='3d')
ax.scatter3D(duration, speed, force, label='data kept')
ax.scatter3D(duration_delete, speed_delete, force_delete, marker='X', color='orange', label='data ignored')
ax.set_xlabel('Time (s)')
ax.set_ylabel('Velocity (m/s)')
ax.set_zlabel('Force (N/kg)')
ax.set_title("Records' surface")
ax.legend()

ax.view_init(elev=30, azim=-30)
if export:
    date_export = datetime.datetime.now().strftime("D%Y%m%d-H%H%M%S")
    name_export_fig = name_file_export + '_' + date_export + '_fig3D'
    plt.savefig(name_export_fig+'_0')








# Models
def function_fvt_inv(d, v, D_F,  F_0c, D_v,  v_0c):
    # function use for speed - grade - duration fitting
    return (D_F/d + F_0c) * (1 - v / (D_v/d + v_0c))

def function_fvt_k(d, v, D_F,  F_0c, D_v,  v_0c, k):
    # function use for speed - grade - duration fitting
    return (D_F/(d+k) + F_0c) * (1 - v / (D_v/(d+k) + v_0c))




# figure for visualization
fig = plt.figure('fitting', figsize=(9, 6.5))
ax = plt.axes(projection='3d')

# fitting model configuration___________________________________________________________________________________________
if model_chosen == 'inverse':
    model = Model(function_fvt_inv, independent_vars=['d', 'v'])
elif model_chosen == 'with k':
    model = Model(function_fvt_k, independent_vars=['d', 'v'])


D_F_ini,  F_0c_ini, D_v_ini,  v_0c_ini = 100, 10, 100, 10 # set the starting point for the fitting
D_F_min,  F_0c_min, D_v_min,  v_0c_min = 0, 0, 0, 0
params = Parameters()
params.add('D_F', value=D_F_ini, min=D_F_min)
params.add('F_0c', value=F_0c_ini, min=F_0c_min)
params.add('D_v', value=D_v_ini, min=D_v_min)
params.add('v_0c', value=v_0c_ini, min=v_0c_min)
if model_chosen == 'with k':
    k0 = 30
    k_min, k_max = 0, 1000
    params.add('k', value=k0,  min=k_min, max=k_max)


# first step fitting with all record speed data_________________________________________________________________________
result1 = model.fit(force, params, d=duration, v=speed)

# second step - removing outlier and fit again__________________________________________________________________________
residuals = result1.residual
z_residuals = (residuals - np.mean(residuals)) / np.std(residuals)  # z-score of the residuals
z_threshold = 2  # threshold to classify as outlier (2 = 95%; 3 = 99%)
is_outlier = (abs(z_residuals) > z_threshold)
ax.scatter3D(duration[is_outlier], speed[is_outlier], force[is_outlier],
             marker='X', color='red', label='outliers')  # plot for visualization
# remove outlier for force, speed and duration
force2 = force[~is_outlier]
speed2 = speed[~is_outlier]
duration2 = duration[~is_outlier]
result2 = model.fit(force2, params, d=duration2, v=speed2)  # fit again without outliers

# third step only with positive residuals_______________________________________________________________________________
# reset fitting parameters so starting points are the coeficients determined from step 2 and boundaries
# correspond to CI95% of the parameters
params['D_F'].value = result2.params['D_F'].value
params['F_0c'].value = result2.params['F_0c'].value
params['D_v'].value = result2.params['D_v'].value
params['v_0c'].value = result2.params['v_0c'].value
params['D_F'].min = max(D_F_min, params['D_F'].value - 2 * result2.params['D_F'].stderr)
params['D_F'].max = params['D_F'].value + 2 * result2.params['D_F'].stderr
params['F_0c'].min = max(F_0c_min, params['F_0c'].value - 2 * result2.params['F_0c'].stderr)
params['F_0c'].max = params['F_0c'].value + 2 * result2.params['F_0c'].stderr
params['D_v'].min = max(D_v_min, params['D_v'].value - 2 * result2.params['D_v'].stderr)
params['D_v'].max = params['D_v'].value + 2 * result2.params['D_v'].stderr
params['v_0c'].min = max(v_0c_min, params['v_0c'].value - 2 * result2.params['v_0c'].stderr)
params['v_0c'].max = params['v_0c'].value + 2 * result2.params['v_0c'].stderr
if model_chosen == 'with k':
    params['k'].value = result2.params['k'].value
    params['k'].min = max(k_min, params['k'].value - 2 * result2.params['k'].stderr)
    params['k'].max = min(k_max, params['k'].value + 2 * result2.params['k'].stderr)

#remove negative residuals
residuals2 = result2.residual
# /!\ positive residuals correspond to residual < 0 because lmfit calculate residuals as predicted - observed
is_positive_res = (residuals2 < 0)
ax.scatter3D(duration2[~is_positive_res], speed2[~is_positive_res], force2[~is_positive_res],
             marker='X', color='blue', label='negative residual')   # plot for visualization
# conserve only positive residuals for force, speed and duration
force3 = force2[is_positive_res]
speed3 = speed2[is_positive_res]
duration3 = duration2[is_positive_res]
result3 = model.fit(force3, params, d=duration3, v=speed3)

# print the report of all 3 fittings____________________________________________________________________________________
print('\n\n\nStep 1:')
print(result1.fit_report())
print('\n\n\nStep 2:')
print(result2.fit_report())
print('\n\n\nStep 3:')
print(result3.fit_report())

# plot the results for visualization ___________________________________________________________________________________
d_grid = np.arange(min(duration), max(duration), 1)
v_grid = np.arange(min(speed), max(speed), 0.1)
d_grid, v_grid = np.meshgrid(d_grid, v_grid)
if model_chosen == 'inverse':
    s_surf = model.eval(d=d_grid, v=v_grid,
                    D_F=result3.params['D_F'].value,
                    F_0c=result3.params['F_0c'].value,
                    D_v=result3.params['D_v'].value,
                    v_0c=result3.params['v_0c'].value)
elif model_chosen == 'with k':
    s_surf = model.eval(d=d_grid, v=v_grid,
                    D_F=result3.params['D_F'].value,
                    F_0c=result3.params['F_0c'].value,
                    D_v=result3.params['D_v'].value,
                    v_0c=result3.params['v_0c'].value,
                    k=result3.params['k'].value)
    

ax.scatter3D(duration3, speed3, force3, color='black', label='remaining data')
ax.legend()
# d_grid[s_surf<0]
# v_grid[s_surf<0]
s_surf[s_surf<0] = np.nan
surf1 = ax.plot_surface(d_grid, v_grid, s_surf,
                        cmap=cm.jet,
                        label='fits(d,v)',
                        alpha=0.5,
                        linewidth=0,
                        antialiased=True)
ax.set_xlabel('Duration (s)')
ax.set_ylabel('Speed (m/s)')
ax.set_zlabel('Force (N/kg)')
ax.set_title("Force - Speed - Duration relationship\n"+file_name_origin_data)
ax.set_xlim([0, duration.max()])
ax.set_ylim([0, speed.max()])
ax.set_zlim([0, force.max()])



if export: 
    # Export parameters
    name_export_param = name_file_export + '_' + date_export + '_fitting.txt'
    with open(name_export_param, 'w') as f:
        f.write("Original file (datas GPS-DR)   "+file_name_origin_data+"\n")
        f.write("Files record used: "+str(files_name)+"\n")
        f.write('Temps entre ' + str(t_min_tresh) + ' et ' + str(t_max_tresh) + ' s\n')
        f.write('Vitesse entre ' + str(v_min_tresh) + ' et ' + str(v_max_tresh) + ' m/s\n')
        f.write('Force entre ' + str(f_min_tresh) + ' et ' + str(f_max_tresh) + ' N/kg\n')
        f.write('\n\n\n\nStep 1:\n')
        f.write(result1.fit_report())
        f.write('\n\n\n\nStep 2:\n')
        f.write(result2.fit_report())
        f.write('\n\n\n\nStep 3:\n')
        f.write(result3.fit_report())
    # Save fig
    ax.view_init(elev=0, azim=0)
    plt.savefig(name_export_fig+'_1')
    for i in range(4):
        ax.view_init(elev=20, azim=0+30*i)
        plt.savefig(name_export_fig+'_'+str(i+2))
    ax.view_init(elev=0, azim=90)
    plt.savefig(name_export_fig+'_6')
    

ax.view_init(elev=20, azim=-40)
if show_plots:
    plt.show()
