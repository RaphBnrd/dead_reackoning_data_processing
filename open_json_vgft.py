import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np

max_len_plot = int(2*86400)
idx_start = 0  *86400  +  10 *3600  +  0  *60  +  0
idx_end =   1  *86400  +  10 *3600  +  0  *60  +  58

datetimes, slope, altitude, force, speed = [], [], [], [], []
folder_input = 'data/20220704_open_sample_fvt/Ibex1/df_vgtf/'

for file in os.listdir(folder_input):
    path_input = folder_input + file
    print('File:', path_input)

    df = pd.read_json(path_input)['data']
    print(df.head())
    this_datetimes = pd.to_datetime(np.arange(pd.to_datetime(df['Time metadata']['start']).value,
                                              pd.to_datetime(df['Time metadata']['end']).value, 
                                              float(df['Time metadata']['sampling_period(s)'])*1e9
                                              )
                                    )
    datetimes += list(pd.to_datetime(this_datetimes))
    force += df['Force']
    speed += df['Speed']
    slope += df['Slope']
    altitude += df['Altitude']

idx_end = min(idx_end, len(datetimes), idx_start+max_len_plot)

plt.figure('Speed')
plt.plot(datetimes[idx_start:idx_end+1], speed[idx_start:idx_end+1])
plt.ylabel('Speed (m/s)')

plt.figure('Slope')
plt.plot(datetimes[idx_start:idx_end+1], slope[idx_start:idx_end+1])
plt.ylabel('Slope (%)')

plt.figure('Altitude')
plt.plot(datetimes[idx_start:idx_end+1], altitude[idx_start:idx_end+1])
plt.ylabel('Altitude (m)')

plt.figure('Force')
plt.plot(datetimes[idx_start:idx_end+1], force[idx_start:idx_end+1])
plt.ylabel('Force (N/kg)')

plt.figure('Force-velocity')
plt.scatter(speed[idx_start:idx_end+1], force[idx_start:idx_end+1], alpha=0.1)
plt.xlabel('Velocity (m/s)')
plt.ylabel('Force (N/kg)')


plt.show()
