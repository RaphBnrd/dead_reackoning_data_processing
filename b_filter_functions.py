from g_computation_functions import butter_lowpass_filter
import numpy as np


def filter_behaviour(df, fs, cutoff_mov, order_mov, threshold_moving):
    """
    Filters the behaviour (is_moving) and get the instants when the animal
    will be considered as moving (when the filtered signal is above a threshold)
    
    Input:
        . df is the dataframe containing all the data of the animal 
          (at least 'DateTime' and 'Behaviour')
        . fs is the sampling frequency (1/sampling_period in Hz)
        . cutoff_mov is the cutoff chosen for the low-pass filter (Hz)
        . order is the order chosen for the low-pass filter
        . threshold_moving is the threshold on the filtered behaviour
          to consider the animal as moving
    Output:
        . idxs_start_moving is the list of index when the animal is considered
          as starting to move
        . idxs_end_moving is the list of index when the animal is considered
          as ending to move
        . is_moving_filtered is the series of data of moving behaviour filtered
    """

    is_moving_filtered = butter_lowpass_filter((df['Behaviour']==3), cutoff_mov, fs, order_mov)
    # Boolean_IsMoving
    bool_is_moving_filtered = (is_moving_filtered >= threshold_moving) * 1
    # Delta_IsMoving
    delta_is_moving = np.array([None] + list(np.diff(bool_is_moving_filtered)))

    idxs_start_moving = list(np.where(delta_is_moving==1)[0]) # Not Moving at k-1 and Moving at k
    idxs_end_moving = list(np.where(delta_is_moving==-1)[0]) # Moving at k-1 and Not Moving at k
    if bool_is_moving_filtered[0] == True:
        idxs_start_moving = [0] + idxs_start_moving # idxs_start_moving are inside the duration (to call df[idxs_start_moving:])
    if bool_is_moving_filtered[df.shape[0]-1] == True:
        idxs_end_moving = idxs_end_moving + [df.shape[0]] # idxs_end_moving are outside the duration (to call df[:idxs_end_moving])
    if len(idxs_start_moving) != len(idxs_end_moving):
        print("Intervalls of move seems false... (not the same length for the lists of starts and ends)")
    if len(idxs_start_moving) == 0:
        idxs_start_moving = [1]
        idxs_end_moving = [df.shape[0]-1]
    idxs_start_moving[0] = max(1, idxs_start_moving[0])

    return idxs_start_moving, idxs_end_moving, is_moving_filtered, bool_is_moving_filtered