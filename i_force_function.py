import numpy as np
from g_computation_functions import movmean
import matplotlib.pyplot as plt

def add_force_developped(df, column_grade='Grade_filtered', column_v='Velocity_filtered',
                         column_date_time='DateTime', Cr=3.35, metab_efficiency=0.25):
    """
    Compute the force developped by the animal, as a sum of energy expended per (and per kilogram)
    Contains 3 components: cost of locomotion, potential energy of gravity, and
    kinetic energy:
    F = Cr*efficiency + g*sin(alpha) + delta_v
    Cr = 3.35 N/kg for goat, metabolic efficiency = 0.25, g = 9.81 m/s²
    Input:
        . df is the dataframe containing all the data of the animal
        . column_grade is the name of the column containing grade
        . column_v is the name of the column containing velocity
        . column_date_time is the name of the column containing datetimes
        . Cr is the cost of locomotion
        . metab_efficiency is the metabolic efficiency of the animal
    Output:
        . df is the dataframe containing all the data of the animal including the new column Force
        . force_loc is the component of the force linked to the cost of locomotion
        . force_gra is the component of the force linked to the grade (potential energy of gravity)
        . force_acc is the component of the force linked to the acceleration
    """
    g = 9.81 # m/s² = N/kg

    idx_first_grade_filter = np.where(np.isfinite(df[column_grade]))[0][0]
    idx_last_grade_filter = np.where(np.isfinite(df[column_grade]))[0][-1]
    # Component from the cost of locomotion
    force_loc = np.ones(df.shape[0]) * Cr * metab_efficiency
    # Component from the potential energy of gravity
    grade = np.array(df[column_grade][idx_first_grade_filter:idx_last_grade_filter+1]) # between k and k-1
    alpha = np.arctan(grade/100)
    force_gra = np.zeros(df.shape[0])
    force_gra[idx_first_grade_filter:idx_last_grade_filter+1] = g * np.sin(alpha)
    force_gra[:idx_first_grade_filter] = np.nan
    force_gra[idx_last_grade_filter+1:] = np.nan
    # Component from the acceleration (kinetic energy)
    dv = np.array(df[column_v].diff()) # v between k and k-1 (NaN in position 1 and 2) | NB: [v is x between i and i-1] --> if dt constant: dv(k) = x(k)-2x(k-1)+x(k-2) / dt
    dt = df[column_date_time].diff().dt.seconds # dt between k and k-1 (NaN in 1st position)
    mean2_dt = movmean(np.array(dt)[1:], 2) # mean dt between k and k+2 = ([t_k+2]-[t_k])/2 (length=df.shape[0]-2)
    force_acc = np.zeros(df.shape[0])
    force_acc[1:force_acc.shape[0]-1] = dv[2:] / mean2_dt

    # Combine the components
    idx_first_force = max(1, idx_first_grade_filter)
    idx_last_force = min(force_acc.shape[0]-2, idx_last_grade_filter)
    force_loc[:idx_first_force] = np.nan
    force_loc[idx_last_force+1:] = np.nan
    force_gra[:idx_first_force] = np.nan
    force_gra[idx_last_force+1:] = np.nan
    force_acc[:idx_first_force] = np.nan
    force_acc[idx_last_force+1:] = np.nan
        
    force = force_loc + force_gra + force_acc
    df['Force'] = force
    
    return df, force_loc, force_gra, force_acc