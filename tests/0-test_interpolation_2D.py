import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate

x = [0]*5 + [1]*5 + [2]*5 + [3]*5 + [4]*5
y = [1, 1.5, 2, 2.5, 3] * 5
u = [2, 1, 0, -2, -4,
     1, 0.5, 0, -1, -2,
     0, 0, 0, 0, 0,
     -1, -0.5, 0, 1, 2,
     -2, -1, 0, 2, 4]
v = [1, 2, 4, 2, 1] * 5

plt.figure(1)
plt.quiver(x, y, u, v)
n = 20
xx = np.linspace(min(x), max(x), n)
yy = np.linspace(min(y), max(y), n)
xx_grid, yy_grid = np.meshgrid(xx, yy)

points = np.transpose(np.vstack((x, y)))
print(points)
u_interp = interpolate.griddata(points, u, (xx_grid, yy_grid), method='cubic')
v_interp = interpolate.griddata(points, v, (xx_grid, yy_grid), method='cubic')

print(u_interp.shape, v_interp.shape)
print(xx_grid.shape, yy_grid.shape)

plt.figure(2)
plt.quiver(xx_grid, yy_grid, u_interp, v_interp)

# 2D independant
f_u = interpolate.interp2d(x, y, u, kind='cubic')
f_v = interpolate.interp2d(x, y, v, kind='cubic')
print(xx.shape, yy.shape)
print(xx_grid.shape, yy_grid.shape)
u_interp2D = f_u(xx.flatten(), yy.flatten())
u_interp2D = u_interp2D.reshape(xx_grid.shape)
v_interp2D = f_v(xx.flatten(), yy.flatten())
v_interp2D = v_interp2D.reshape(xx_grid.shape)

print(u_interp2D.shape, v_interp2D.shape)
print(xx.shape, yy.shape)

plt.figure(3)
plt.quiver(xx, yy, u_interp2D, v_interp2D)

# 2*1D independant
print('fig4')
points = np.transpose(np.vstack((x, y)))
u_interp7 = interpolate.griddata(points, u, (xx_grid, yy_grid), method='cubic')
v_interp7 = interpolate.griddata(points, v, (xx_grid, yy_grid), method='cubic')

print(u_interp7.shape, v_interp7.shape)
print(xx_grid.shape, yy_grid.shape)

plt.figure(4)
plt.quiver(xx, yy, u_interp7, v_interp7)


plt.show()