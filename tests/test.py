# from scipy import signal, interpolate
# import matplotlib.pyplot as plt
# import numpy as np
# # import pandas as pd
# from mpl_toolkits import mplot3d


##### POWER SPECTRAL DENSITY #####

# fs = 1e1                # Hz
# N = 4e2                 # pts
# freq = 0.1              # Hz
# time = np.arange(N)/fs

# x0 = np.sin(2*np.pi*freq*time)
# x1 = x0 + 0.5*np.sin(2*np.pi*5*freq*time)
# x2 = x0 + np.random.uniform(-0.2,0.2, len(time))

# f0, Pxx_den0 = signal.periodogram(x0, fs)
# f1, Pxx_den1 = signal.periodogram(x1, fs)
# f2, Pxx_den2 = signal.periodogram(x2, fs)


# fig, axs = plt.subplots(3, 2)
# axs[0, 0].plot(time, x0)
# axs[0, 0].set_ylabel('Signal x0 [V]')
# axs[0, 1].semilogy(f0, Pxx_den0)
# axs[0, 1].set_xlim(0, 2)
# axs[0, 1].set_ylim(1e-35, 1)
# axs[0, 1].set_ylabel('PSD x0 [V**2/Hz]')

# axs[1, 0].plot(time, x1)
# axs[1, 0].set_ylabel('Signal x1 [V]')
# axs[1, 1].semilogy(f1, Pxx_den1)
# axs[1, 1].set_xlim(0, 2)
# axs[0, 1].set_ylim(1e-35, 1)
# axs[1, 1].set_ylabel('PSD x1 [V**2/Hz]')

# axs[2, 0].plot(time, x2)
# axs[2, 0].set_ylabel('Signal x2 [V]')
# axs[2, 1].semilogy(f2, Pxx_den2)
# axs[2, 1].set_xlim(0, 2)
# axs[0, 1].set_ylim(1e-35, 1)
# axs[2, 1].set_ylabel('PSD x2 [V**2/Hz]')

# axs[2, 0].set_xlabel('Time [s]')
# axs[2, 1].set_xlabel('frequency [Hz]')


# plt.show()



##### INTERPOLATION #####

# x1 = np.linspace(0, 10, num=11, endpoint=True)
# y1 = np.cos(-x1**2/9.0)
# f1_1 = interpolate.interp1d(x1, y1)
# f1_2 = interpolate.interp1d(x1, y1, kind='cubic')
# xnew1 = np.linspace(0, 10, num=41, endpoint=True)
# plt.plot(x1, y1, 'o', xnew1, f1_1(xnew1), '-', xnew1, f1_2(xnew1), '--')
# plt.show()

# x2 = np.array([0,2])
# y2 = np.array([0,1])
# f2_1 = interpolate.interp1d(x2, y2)
# # f2_2 = interpolate.interp1d(x2, y2, kind='cubic')
# xnew2 = np.linspace(0, 2, num=41, endpoint=True)
# plt.plot(x2, y2, 'o', xnew2, f2_1(xnew2), '-')#, xnew2, f2_2(xnew2), '--')
# plt.legend(['data', 'linear', 'cubic'], loc='best')
# plt.show()


# x3 = np.array([0*np.pi/3, 1*np.pi/3, 2*np.pi/3, 3*np.pi/3, 4*np.pi/3, 5*np.pi/3, 6*np.pi/3])
# y3 = np.array([np.sin(0*np.pi/3), np.sin(1*np.pi/3), np.sin(2*np.pi/3), np.sin(3*np.pi/3),
#                np.nan, np.sin(5*np.pi/3),np.sin(6*np.pi/3) ])
# # f3_1 = interpolate.interp1d(x3, y3)
# # f3_2 = interpolate.interp1d(x3, y3, kind='cubic')
# # xnew3 = np.linspace(0, 2*np.pi, num=41, endpoint=True)

# x3_pd, y3_pd = pd.Series(x3), pd.Series(y3)
# print(x3_pd, y3_pd)
# print(type(x3_pd), type(y3_pd))
# y3_interp_1 = y3_pd.interpolate(method='linear')
# y3_interp_2 = y3_pd.interpolate(method='polynomial', order=3)

# # plt.plot(x3, y3, 'o', xnew3, f3_1(xnew3), '-', xnew3, f3_2(xnew3), '--')
# # plt.legend(['data', 'linear', 'cubic'], loc='best')
# # plt.title("With scipy")
# # plt.show()
# plt.plot(x3_pd, y3_pd, 'o', x3_pd, y3_interp_1, '-', x3_pd, y3_interp_2, '--')
# plt.legend(['data', 'linear', 'cubic'], loc='best')
# plt.title("With pandas")
# plt.show()






##### Inster row in a dataframe #####
# import pandas as pd

# df = pd.DataFrame({'Date':['10/2/2011', '12/2/2011', '13/2/2011', '14/2/2011'],
#                     'Event':['Music', 'Poetry', 'Theatre', 'Comedy'],
#                     'Cost':[10000, 5000, 15000, 2000]})
# print(df)
# print(df.columns)

# row_number = 2
# row_value = ['11/2/2011', 'Wrestling', 12000]

# df1 = df[0:row_number]
# df2 = df[row_number:]

# pd.set_option('mode.chained_assignment',None)
# df1.loc[row_number]=row_value
# pd.set_option('mode.chained_assignment','warn')
# df_result = pd.concat([df1, df2])
# df_result.index = [*range(df_result.shape[0])]

# print(df_result)





##### PLOT 3D #####

# times = [30, 60, 120, 360, 720]
# grade_borders = [-20, -15, -10, -5, 0, 5, 10, 15, 20]
# v_rec3D = [[0.49879392, 0.60466068, 0.41898108, 0.25107215, 0.25838843],
#            [0.43740542, 0.58114184, 0.4265174,  0.27394326, 0.24973142],
#            [0.42127539, 0.52986752, 0.42645098, 0.27035052, 0.22198206],
#            [0.43967734, 0.47567716, 0.41386884, 0.25154219, 0.21034744],
#            [0.38316821, 0.42127332, 0.39002961, 0.25259981, 0.19276793],
#            [0.36182028, 0.37274255, 0.34693837, 0.2412264,  0.20092374],
#            [0.35316915, 0.32228696, 0.30710297, 0.22641914, 0.2010904 ],
#            [0.3460658,  0.28829684, 0.2443662,  0.2188888,  0.20069233]]

# grade_centers = [np.mean([grade_borders[k], grade_borders[k+1]]) \
#                  for k in range(len(grade_borders)-1)]
# t = []
# g = []
# v = []
# for l in range(len(times)):
#     for k in range(len(grade_centers)):
#         t.append(times[l])
#         g.append(grade_centers[k])
#         v.append(v_rec3D[k][l])
# fig = plt.figure()
# ax = plt.axes(projection='3d')
# ax.scatter3D(t, g, v)#, c=zdata, cmap='Greens')
# ax.set_xlabel('Time (s)')
# ax.set_ylabel('Grade (%)')
# ax.set_zlabel('Velocity (m/s)')
# ax.set_title("Records' surface")
# plt.show()





##### SCATTER PLOT #####

# x = np.array([1, 2, 3])
# y = 2*x
# plt.scatter(x, y, s=200, c='green', alpha=0.5)
# plt.show()





##### FITTING DATA #####

import numpy as np
from scipy.optimize import curve_fit
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# ## TEST 1
# # test function
# def function(data, a, b, c):
#     x = data[0]
#     y = data[1]
#     return a * (x**b) * (y**c)

# # setup test data
# raw_data = [2.0, 2.0, 2.0], [1.5, 1.5, 1.5], [0.5, 0.5, 0.5],[3.0, 2.0, 1.0], [3.0, 2.0, 1.0],\
#        [3.0, 2.0, 1.0], [2.4, 2.5, 2.2], [2.4, 3.0, 2.5], [4.0, 3.3, 8.0]

# # convert data into proper format
# x_data = []
# y_data = []
# z_data = []
# for item in raw_data:
#     x_data.append(item[0])
#     y_data.append(item[1])
#     z_data.append(item[2])

# # get fit parameters from scipy curve fit
# parameters, covariance = curve_fit(function, [x_data, y_data], z_data)

# # create surface function model
# # setup data points for calculating surface model
# model_x_data = np.linspace(min(x_data), max(x_data), 30)
# model_y_data = np.linspace(min(y_data), max(y_data), 30)
# # create coordinate arrays for vectorized evaluations
# X, Y = np.meshgrid(model_x_data, model_y_data)
# # calculate Z coordinate array
# Z = function(np.array([X, Y]), *parameters)

# # setup figure object
# fig = plt.figure()
# # setup 3d object
# ax = Axes3D(fig)
# # plot surface
# ax.plot_surface(X, Y, Z)
# # plot input data
# ax.scatter(x_data, y_data, z_data, color='red')
# # set plot descriptions
# ax.set_xlabel('X data')
# ax.set_ylabel('Y data')
# ax.set_zlabel('Z data')

# plt.show()


## TEST 2

# times = [120, 300, 480, 660, 840, 1020]
# grade_border = [-50, -48, -46, -44, -42, -40, -38, -36, -34, -32, -30, -28, -26, -24,
#               -22, -20, -18, -16, -14, -12, -10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10,
#               12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50]

# v_rec3D =  np.array(
#            [[0.70226408, 0.59523769, 0.50094266, 0.48505594, 0.45230414,     None],  
#             [0.52821934, 0.59356659, 0.50940959, 0.47148308, 0.44624587, 0.15192597],   # -45
#             [0.63436852, 0.59623048, 0.48300684, 0.46183297, 0.44280226,     None],
#             [0.55970029, 0.59152046, 0.46022844, 0.45211722, 0.42799325, 0.40928916],
#             [0.46625553, 0.55652137, 0.46062502, 0.42907086, 0.41571858, 0.4014534 ],
#             [0.47251427, 0.55131656, 0.45785359, 0.41682745, 0.41511115, 0.35168877],
#             [0.43217532, 0.53748926, 0.40178779, 0.4160738 , 0.39168699, 0.38877265],   # -35
#             [0.42999561, 0.39891468, 0.44448776, 0.40131029, 0.38346102, 0.3716706 ],
#             [0.42556665, 0.49614784, 0.43188912, 0.38699701, 0.37604554, 0.36668369],
#             [0.41922481, 0.39524709, 0.43451515, 0.38570331, 0.36297286, 0.35787579],
#             [0.41708197, 0.39852034, 0.43261657, 0.38361592, 0.36311021, 0.34444141],
#             [0.55208459, 0.39879064, 0.43030074, 0.37301113, 0.34514907, 0.32995558],   # -25
#             [0.5798743 , 0.378835  , 0.38623319, 0.36876664, 0.33689617, 0.31763176],
#             [0.56914673, 0.42185151, 0.3024883 , 0.34556618, 0.33138042, 0.30877101],
#             [0.52907855, 0.44139009, 0.35376949, 0.3232819 , 0.32526107, 0.30403265],
#             [0.44437086, 0.43750599, 0.35017999, 0.32411912, 0.30048685, 0.30238638],
#             [0.43225369, 0.45325466, 0.42099854, 0.38311438, 0.33915591, 0.28361804],   # -15
#             [0.42586319, 0.45827681, 0.42002944, 0.37917303, 0.33458218, 0.27515377],
#             [0.42622295, 0.44658973, 0.34268268, 0.36314303, 0.33663814, 0.31438815],
#             [0.51940951, 0.42511565, 0.3270902 , 0.34113055, 0.33334933, 0.30517429],
#             [0.50125286, 0.41698448, 0.32189148, 0.3233487 , 0.33200277, 0.30770782],
#             [0.68949974, 0.40878112, 0.39128839, 0.35606555, 0.31537876, 0.30261373],   # -5
#             [0.7690573 , 0.38925671, 0.37344101, 0.3681171 , 0.37068366, 0.35819098],
#             [0.77045086, 0.49608066, 0.51321391, 0.39911272, 0.35637183, 0.35048134],
#             [0.82837674, 0.60777238, 0.5357306 , 0.46465402, 0.45466729, 0.41358314],
#             [0.86711418, 0.65279083, 0.55592357, 0.5420616 , 0.50367069, 0.47134094],
#             [1.02142567, 0.68760621, 0.62013351, 0.55951919, 0.61568705, 0.56676384],   # 5
#             [1.02746352, 0.64744649, 0.64092175, 0.68844124, 0.61468163, 0.55689379],
#             [0.70698118, 0.74101104, 0.67562302, 0.63574472, 0.54205786, 0.52229445],
#             [0.76535019, 0.55476138, 0.34038455, 0.29864383, 0.29059328, 0.27491041],
#             [0.77622095, 0.31256921, 0.32512221, 0.30330722, 0.2944668 , 0.27922292],
#             [0.63474767, 0.34393735, 0.30051066, 0.29036775, 0.29001826, 0.27800829],   # 15
#             [0.52891072, 0.34884177, 0.29264021, 0.27539103, 0.27457226, 0.26801757],
#             [0.33134619, 0.35623748, 0.29518689, 0.26416493, 0.2327678 , 0.26741487],
#             [0.32706156, 0.36949616, 0.25667729, 0.24305947, 0.2306657 , 0.22343848],
#             [0.31833868, 0.36988634, 0.2562664 , 0.24369274, 0.23331036, 0.23005597],
#             [0.3298301 , 0.3612868 , 0.27023637, 0.24761051, 0.23598738, 0.18338884],   # 25
#             [0.34171081, 0.33467989, 0.27279573, 0.22935608, 0.18762435, 0.19524027],
#             [0.35890686, 0.33624408, 0.20120157, 0.19494276, 0.18769894, 0.20040642],
#             [0.36519071, 0.3370287 , 0.20506792, 0.19924351, 0.19960818, 0.19592579],
#             [0.36348361, 0.23867317, 0.21195102, 0.1943699 , 0.20257838, 0.19181654],
#             [0.39110953, 0.2588906 , 0.2299643 , 0.20011189, 0.2035634 , 0.19839884],   # 35
#             [0.40863955, 0.27307431, 0.2372929 , 0.21656236, 0.20700306, 0.20349477],
#             [0.41622693, 0.24217422, 0.21741677, 0.22389432, 0.20990177, 0.20501717],
#             [0.25211249, 0.25371558, 0.24918623, 0.23304404, 0.2157514 , 0.17543053],
#             [0.23814985, 0.26211369, 0.25018407, 0.23344646, 0.21607448, 0.17440015],
#             [0.25717384, 0.25854038, 0.25341383, 0.20821151, 0.1975782 , 0.19183376],   # 45
#             [0.28974342, 0.25963784, 0.22734086, 0.21701287, 0.21087484, 0.20585536]])









## TEST 3

def func_model(data, D, beta_0, beta_1, beta_2):
    t = data[0] # x=t (ou d)
    g = data[1] # y=g
    return D/t + 1 / (beta_0*g**2 + beta_1*g + beta_2)

times = [120, 300, 480, 660, 840, 1020]
grade_centers = [k for k in range(-5, 27, 2)]

v_rec3D_short = np.array([
            [0.68949974, 0.40878112, 0.39128839, 0.35606555, 0.31537876, 0.30261373],   # -5
            [0.7690573 , 0.38925671, 0.37344101, 0.3681171 , 0.37068366, 0.35819098],
            [0.77045086, 0.49608066, 0.51321391, 0.39911272, 0.35637183, 0.35048134],
            [0.82837674, 0.60777238, 0.5357306 , 0.46465402, 0.45466729, 0.41358314],
            [0.86711418, 0.65279083, 0.55592357, 0.5420616 , 0.50367069, 0.47134094],
            [1.02142567, 0.68760621, 0.62013351, 0.55951919, 0.61568705, 0.56676384],   # 5
            [1.02746352, 0.64744649, 0.64092175, 0.68844124, 0.61468163, 0.55689379],
            [0.70698118, 0.74101104, 0.67562302, 0.63574472, 0.54205786, 0.52229445],
            [0.76535019, 0.55476138, 0.34038455, 0.29864383, 0.29059328, 0.27491041],
            [0.77622095, 0.31256921, 0.32512221, 0.30330722, 0.2944668 , 0.27922292],
            [0.63474767, 0.34393735, 0.30051066, 0.29036775, 0.29001826, 0.27800829],   # 15
            [0.52891072, 0.34884177, 0.29264021, 0.27539103, 0.27457226, 0.26801757],
            [0.33134619, 0.35623748, 0.29518689, 0.26416493, 0.2327678 , 0.26741487],
            [0.32706156, 0.36949616, 0.25667729, 0.24305947, 0.2306657 , 0.22343848],
            [0.31833868, 0.36988634, 0.2562664 , 0.24369274, 0.23331036, 0.23005597],
            [0.3298301 , 0.3612868 , 0.27023637, 0.24761051, 0.23598738, 0.18338884]])   # 25


print(grade_centers)
print(times)

t, g, v = [], [], []
for l in range(len(times)):
    for k in range(len(grade_centers)):
        t.append(times[l])
        g.append(grade_centers[k])
        v.append(v_rec3D_short[k][l])

x_data, y_data, z_data = t, g, v

# for k in range(len(t)):
#     print(t[k], g[k], v[k])

parameters, covariance = curve_fit(func_model, [x_data, y_data], z_data, 
                                   p0=[250, 0.05, -1, 5],
                                #    bounds=([0,0,0,0], [1000,30,2,1],
                                   bounds=([0,0,-2,-10], [2000,5,2,10]))
print("\n\nParamètres du fitting : ")
print(parameters)
print("\n")
model_x_data = np.linspace(min(x_data), max(x_data), 50)
model_y_data = np.linspace(min(y_data), max(y_data), 50)
X, Y = np.meshgrid(model_x_data, model_y_data)
Z = func_model(np.array([X, Y]), *parameters)

residuals = np.array(z_data) - func_model(np.array([x_data, y_data]), *parameters)
print(residuals)

# Z_test = func_model(np.array([X, Y]), 0, 2, 1, 2)

fig = plt.figure()
ax = plt.axes(projection='3d')
# ax.plot_surface(X, Y, Z_test)
ax.plot_surface(X, Y, Z)
ax.scatter3D(x_data, y_data, z_data, color='red')
ax.set_xlabel('Time (s)')
ax.set_ylabel('Grade (%)')
ax.set_zlabel('Velocity (m/s)')

plt.show()


