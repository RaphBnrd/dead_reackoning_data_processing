import urllib.request
import json

import pandas as pd
import numpy as np
from scipy import interpolate

import pygmt
from pyproj import Transformer

import plotly.graph_objects as go
import plotly.figure_factory as ff
import matplotlib.pyplot as plt

type_import_elevation = 'pygmt'
# type_import_elevation = 'database'



# _____ INPUT DATA ____________________________________________________________
path_input = 'data/data_small/Ibex1_complete_v2_short2.csv'
df = pd.read_csv(path_input, na_values="NA")
df['DateTime'] = pd.to_datetime(df['DateTime'])

pts_real_lat = df['DDMT_Latitude']
pts_real_lon = df['DDMT_Longitude']
pts_real_alt = df['Altitude']

transformer = Transformer.from_crs("epsg:4326", "epsg:3857")
pts_real_x, pts_real_y = transformer.transform(pts_real_lat, pts_real_lon)
df['X'], df['Y'] = pts_real_x, pts_real_y


# _____ DEFINE BORDERS OF THE REGION + WHERE TO COMPUTE GRADE _________________
minlon, maxlon = 6.03, 6.06
minlat, maxlat = 45.21, 45.22


# _____ GET ELEVATION FROM PYGMT ___________________________________________
if type_import_elevation == 'pygmt':
    grid = pygmt.datasets.load_earth_relief(
                "01s",
                region=[minlon, maxlon, minlat, maxlat],
                registration="gridline",
                use_srtm=True,
    )

    lat_small = grid.coords['lat'].values
    lon_small = grid.coords['lon'].values
    elev = grid.data





# _____ GET ELEVATION FROM DATABASE ___________________________________________
elif type_import_elevation == 'database':
    n = 30

    lat_small = np.linspace(minlat, maxlat, n)
    lon_small = np.linspace(minlon, maxlon, n)
    print(lat_small.shape, lon_small.shape)
    lat, lon = np.meshgrid(lat_small, lon_small)
    lat, lon = lat.flatten(), lon.flatten()
    print(lat.shape, lon.shape)
    print(lat[:5], lon[:5])

    #CONSTRUCT JSON
    d_ar=[{}]*lat.size
    for i in range(lat.size):
        d_ar[i]={"latitude":lat[i],"longitude":lon[i]}
    location={"locations":d_ar}
    json_data=json.dumps(location,skipkeys=int).encode('utf8')

    #SEND REQUEST 
    url="https://api.open-elevation.com/api/v1/lookup"
    response = urllib.request.Request(url,json_data,headers={'Content-Type': 'application/json'})
    fp=urllib.request.urlopen(response)

    #RESPONSE PROCESSING
    res_byte=fp.read()
    res_str=res_byte.decode("utf8")
    js_str=json.loads(res_str)
    #print (js_mystr)
    fp.close()

    #GETTING ELEVATION 
    response_len=len(js_str['results'])
    elev_list=[]
    for j in range(response_len):
        elev_list.append(js_str['results'][j]['elevation'])

    elev = np.array(elev_list).reshape((n, n))
    # columns = different lat ; rows = different rows


print("Latitudes:", lat_small.shape, lat_small[:5])
print("Longitudes:", lon_small.shape, lon_small[:5])
print("Elevations:", elev.shape)






lat_grid, lon_grid = np.meshgrid(lat_small, lon_small)
x_grid, y_grid = transformer.transform(lat_grid.flatten(), lon_grid.flatten())
x_grid, y_grid = x_grid.reshape(lat_grid.shape), y_grid.reshape(lat_grid.shape)



# _____ PLOT ELEVATION ________________________________________________________

fig = go.Figure(data=[go.Surface(z=elev, x=x_grid, y=y_grid)])
fig.update_layout(title='Elevation '+type_import_elevation,
                  scene = dict(
                    xaxis_title='x (m)',
                    yaxis_title='y (m)',
                    zaxis_title='Elevation (m)',
                    # aspectmode='manual',
                    # aspectratio=dict(x=1, y=1, z=2),
                    aspectmode='data'))
fig.show()





# _____ INTERPOLATE ELEVATION _________________________________________________
print("\nInterpolation de l'altitude")

x_new = np.linspace(x_grid.min(), x_grid.max(), 1000)
y_new = np.linspace(y_grid.min(), y_grid.max(), 1000)

x_new_grid, y_new_grid = np.meshgrid(x_new, y_new)

points = np.transpose(np.vstack((x_grid.flatten(), y_grid.flatten())))
elev_new = interpolate.griddata(points, elev.flatten(), 
                                (x_new_grid.flatten(), y_new_grid.flatten()),
                                method='cubic')
elev_new = elev_new.reshape(x_new_grid.shape)

fig = go.Figure(data=[go.Surface(z=elev_new, x=x_new, y=y_new)])
fig.update_layout(title='Elevation interp '+type_import_elevation,
                  scene = dict(
                    xaxis_title='x (m)',
                    yaxis_title='y (m)',
                    zaxis_title='Elevation (m)',
                    aspectmode='data'))
fig.show()


print("x interpolés:", x_new.shape, x_new[:5])
print("y interpolés:", y_new.shape, y_new[:5])
print("Elevations interpolées:", elev_new.shape)


# _____ INTERPOLATE ELEVATION ON DF ___________________________________________
print("\nInterpolation de l'altitude sur le DataFrame")
points = np.transpose(np.vstack((lat_grid.flatten(), lon_grid.flatten())))
elev_interp_df = interpolate.griddata(points, elev.flatten(), 
                                      (df['DDMT_Longitude'], df['DDMT_Latitude']),
                                      method='cubic')
df['Z'] = elev_interp_df

plt.figure()
plt.plot(df['Z'], df['Altitude'])
plt.xlabel('Z')
plt.ylabel('Altitude')
plt.show()





# _____ COMPUTE AND PLOT THE GRADIENT _________________________________________
print("\nCalcul du gradient sur l'interpolation")
# on the complete data
grad_y, grad_x = np.gradient(elev_new)
print("Gradient x (sur l'interpolation):", grad_x.shape, grad_x[:5])
print("Gradient y (sur l'interpolation):", grad_y.shape, grad_y[:5])
print("Gradient x (sur l'interpolation) min/max:", grad_x.min(), grad_x.max())
print("Gradient y (sur l'interpolation) min/max:", grad_y.min(), grad_y.max())





# # only on few data (for the plot)
# few_x_new = x_new_grid, y_new_grid
# few_y_new = np.linspace(min(y_small), max(y_small), 10)
# few_x_new_grid, few_y_new_grid = np.meshgrid(few_x_new, few_y_new)
# few_elev_new = f_elev(few_x_new, few_y_new)
# few_grad_y, few_grad_x = np.gradient(few_elev_new)

f = ff.create_quiver(x_new_grid, y_new_grid, grad_x, grad_y,
                       scale=.25,
                       arrow_scale=.4,
                       name='quiver',
                       line_width=1)
trace1 = f.data[0]
trace2 = go.Contour(z=elev_new, x=x_new, y=y_new)
data=[trace1,trace2]
fig = go.FigureWidget(data)
fig.update_layout(title='Gradient '+type_import_elevation,
                  scene = dict(
                    xaxis_title='x (m)',
                    yaxis_title='y (m)',
                    aspectmode='data'))
fig.show()






# _____ INTERPOLATE GRADIENT TO GET SLOPE _____________________________________



# f_grad = interpolate.interp2d(x_new, y_new, elev, kind='cubic')


# grad_y, grad_x = np.gradient(elev_new)


