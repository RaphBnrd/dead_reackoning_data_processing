import pandas as pd
import numpy as np
import math
from scipy import interpolate

import rasterio as rio
import pygmt
from pyproj import Transformer

import plotly.express as px
import plotly.graph_objects as go
import plotly.figure_factory as ff
import matplotlib.pyplot as plt



transformer_latlon_to_xy = Transformer.from_crs("epsg:4326", "epsg:3857")
transformer_xy_to_latlon = Transformer.from_crs("epsg:3857", "epsg:4326")


# _____ INPUT DATA ____________________________________________________________
# path_input = 'data/data_small/Ibex1_complete_v2_short2.csv'
path_input = 'data/data_small/Ibex1_complete_v2_short3.csv'
# path_input = '../Data Dead-reckoning/Ibex complete data/Ibex1_complete_v2.csv'

df = pd.read_csv(path_input, na_values="NA")
df['DateTime'] = pd.to_datetime(df['DateTime'])
df.dropna(inplace=True, subset = ['DDMT_Latitude', 'DDMT_Longitude'])

pts_real_x, pts_real_y = transformer_latlon_to_xy.transform(df['DDMT_Latitude'], df['DDMT_Longitude'])
df['X'], df['Y'] = pts_real_x, pts_real_y


# _____ GET DATA FROM THE REGION ______________________________________________

filename = 'mnt_BELLEDONNE_1m_l93_raphael.tif'
ref_map_lon = (5.9825, 6.1621)
ref_map_lat = (45.1871, 45.2803)
ref_map_x = (934178.57, 947858.61) # m
ref_map_y = (6458621.84, 6469512.86) # m
step_acquisition = 1 # m 

print('\n\nOuverture du MNT')
dem = rio.open(filename)

elev = dem.read(1).astype('float64')
print(elev.shape)

print('\n\nCalcul de la grille')
x_small = np.arange(math.ceil(ref_map_x[0]), ref_map_x[1], step_acquisition)
y_small = np.arange(math.ceil(ref_map_y[0]), ref_map_y[1], step_acquisition)

x_grid, y_grid = np.meshgrid(x_small, y_small)


print(x_grid.shape, y_grid.shape)
print(x_grid[:2, :2], y_grid[:2, :2])


# _____ COMPUTE Z _____________________________________________________________
print('\n\nCalcul de Z')
points = np.transpose(np.vstack((x_grid.flatten(), y_grid.flatten())))
elev_interp_df = interpolate.griddata(points, elev.flatten(), 
                                      (df['X'], df['Y']),
                                      method='cubic')
df['Z'] = elev_interp_df

print(df.shape)
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].head(5))
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*1])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*2])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*3])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*4])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*5])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[df['Z']==min(df['Z'])])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[df['Z']==max(df['Z'])])




# _____ PLOTS _________________________________________________________________
# fig = px.scatter_3d(df, x='DDMT_Longitude', y='DDMT_Latitude', z='Altitude')
# fig.show()

# fig = px.scatter_3d(df, x='X', y='Y', z='Z')
# fig.show()

plt.figure()
plt.plot(df['X'], df['DDMT_Longitude'])
plt.xlabel('X')
plt.ylabel('DDMT_Longitude')

plt.figure()
plt.plot(df['Y'], df['DDMT_Latitude'])
plt.xlabel('Y')
plt.ylabel('DDMT_Latitude')

plt.figure()
plt.plot(df['Z'], df['Altitude'], 'o', alpha=1/255)
plt.plot([min(df['Z']), max(df['Z'])], [min(df['Z']), max(df['Z'])], 'r--')
plt.xlabel('Z')
plt.ylabel('Altitude')


plt.show()
