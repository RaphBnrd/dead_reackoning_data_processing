import pandas as pd
import numpy as np
import datetime

path_csv = 'data/Ibex1_complete_v2_short1.csv'
df = pd.read_csv(path_csv, na_values="NA", usecols=['DateTime', 'AltChnage', 'Distance', 'Behaviour'])
df['DateTime'] = pd.to_datetime(df['DateTime']) # this column's type is datetime (it will be usefull to compute durations, ...)

print(df.head())
print(datetime.datetime.fromisoformat('2017-06-02 00:00:04'))
print(np.array(df['DateTime'])[4])
print(df['DateTime'][4])
print(datetime.datetime.fromisoformat('2017-06-02 00:00:04') == np.array(df['DateTime'])[4])
print(datetime.datetime.fromisoformat('2017-06-02 00:00:04') == df['DateTime'][4])


print(np.where(np.array(df['DateTime']) == np.array(df['DateTime'])[4]))

print(np.where(df['DateTime'] == datetime.datetime.fromisoformat('2017-06-02 00:00:04')))


