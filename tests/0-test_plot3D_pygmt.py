import plotly.graph_objs as go
import pygmt
import numpy as np
from pyproj import Transformer
from scipy import interpolate

minlon, maxlon = 6.01, 6.02
minlat, maxlat = 45.21, 45.23

grid = pygmt.datasets.load_earth_relief(
                "01s",
                region=[minlon, maxlon, minlat, maxlat],
                registration="gridline",
                use_srtm=True
            )

lon_small = grid.coords['lon'].values
lat_small = grid.coords['lat'].values
elev_grid = grid.data

lon_grid, lat_grid = np.meshgrid(lon_small, lat_small)

transformer_from_latlon_to_xy = Transformer.from_crs("epsg:4326", "epsg:3857")
x_grid_flat, y_grid_flat = transformer_from_latlon_to_xy.transform(lat_grid.flatten(), lon_grid.flatten())


horizontal_step_interp = 10
x_each_step = np.arange(min(x_grid_flat)-horizontal_step_interp, max(x_grid_flat)+horizontal_step_interp, horizontal_step_interp)
y_each_step = np.arange(min(y_grid_flat)-horizontal_step_interp, max(y_grid_flat)+horizontal_step_interp, horizontal_step_interp)
x_each_step_grid, y_each_step_grid = np.meshgrid(x_each_step, y_each_step)

# Interpolate map elevation on the grid each step horizontal_step_interp (in m)
points = np.transpose(np.vstack((x_grid_flat, y_grid_flat)))
elev_interp_each_step = interpolate.griddata(points, elev_grid.flatten(), 
                                             (x_each_step_grid, y_each_step_grid),
                                             method='cubic')
    


print(x_each_step_grid.shape, y_each_step_grid.shape)
print(elev_interp_each_step.shape)


layout = go.Layout(width = 700, height =700,
                             title_text='Chasing global Minima')
fig = go.Figure(data=[go.Surface(x=x_each_step_grid-x_each_step_grid.min(), 
                                 y=y_each_step_grid-y_each_step_grid.min(),
                                 z=elev_interp_each_step, colorscale = 'Blues')], layout=layout)

fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                  highlightcolor="limegreen", project_z=True))

x_scatter = np.array(x_grid_flat)[::50]
y_scatter = np.array(y_grid_flat)[::50]
z_scatter = np.array(elev_grid.flatten())[::50] + 100
fig.add_scatter3d(x=x_scatter-x_each_step_grid.min(), 
                  y=y_scatter-y_each_step_grid.min(),
                  z=z_scatter, 
                  mode='lines', line=dict(color='#101010', width=4),
                  marker=dict(size=2, color=elev_grid.flatten(),               
                              colorscale='Reds'))

fig.show()