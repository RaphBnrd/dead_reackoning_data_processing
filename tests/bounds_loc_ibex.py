import pandas as pd


path_input = ['../Data Dead-reckoning/Ibex complete data/Ibex1_complete_v2.csv',
              '../Data Dead-reckoning/Ibex complete data/Ibex2_complete_v2.csv',
              '../Data Dead-reckoning/Ibex complete data/Ibex3_complete_v2.csv',
              '../Data Dead-reckoning/Ibex complete data/Ibex6_complete_v2.csv',
              '../Data Dead-reckoning/Ibex complete data/Ibex8_complete_v2.csv',
              '../Data Dead-reckoning/Ibex complete data/Ibex9_complete_v2.csv']

min_lats = []
max_lats = []
min_lons = []
max_lons = []

for path in path_input:
    df = pd.read_csv(path, na_values="NA", usecols=['DateTime', 'DDMT_Longitude', 'DDMT_Latitude', 'Altitude', 'AltChnage', 'Distance'])
    min_lats.append(min(df['DDMT_Latitude']))
    max_lats.append(max(df['DDMT_Latitude']))
    min_lons.append(min(df['DDMT_Longitude']))
    max_lons.append(max(df['DDMT_Longitude']))

print('Min lats:', min_lats)
print('Max lats:', max_lats)
print('Min lons:', min_lons)
print('Max lons:', max_lons)

print('Min lats:', min(min_lats))
print('Max lats:', max(max_lats))
print('Min lons:', min(min_lons))
print('Max lons:', max(max_lons))



