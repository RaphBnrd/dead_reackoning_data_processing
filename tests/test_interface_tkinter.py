import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd
from tkinter.messagebox import showinfo

# create the root window

class WindowFileCsv(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title('Choix du csv')
        self.geometry('420x150')
        self.filename_csv = tk.StringVar()
        entry_file_path = ttk.Entry(self, textvariable=self.filename_csv, width=50)
        entry_file_path.place(x=10, y=50)
        browse_button = ttk.Button(self, text='Parcourir', command=self.select_file)
        browse_button.place(x=320, y=48)
        validate_button = ttk.Button(self, text='Valider', command=self.validate_csv)
        validate_button.place(x=150, y=90)
    def select_file(self):
        filetypes = (('csv files', '*.csv'), ('All files', '*.*'))
        self.filename_csv.set(fd.askopenfilename(title='Open a file', initialdir='', filetypes=filetypes))
    def validate_csv(self):
        print("Chemin du csv: \n", self.filename_csv.get())
        self.destroy()

root = WindowFileCsv()
root.mainloop()
print("Je récupère le fichier : \n", root.filename_csv.get())


app = tk.Tk() 
app.geometry('200x100')

labelTop = tk.Label(app,
                    text = "Choose your favourite month")
labelTop.grid(column=0, row=0)

comboExample = ttk.Combobox(app, 
                            values=[
                                    "January", 
                                    "February",
                                    "March",
                                    "April"])
print(dict(comboExample)) 
comboExample.current(1)
comboExample.set("Coucou")
comboExample.grid(column=0, row=1)

print(comboExample.current(), comboExample.get())

app.mainloop()