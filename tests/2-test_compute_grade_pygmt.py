import pandas as pd
import numpy as np
import math
from scipy import interpolate

import pygmt
from pyproj import Transformer, Geod

import plotly.express as px
import plotly.graph_objects as go
import plotly.figure_factory as ff
import matplotlib.pyplot as plt


# _____ INPUT DATA ____________________________________________________________
print('\n\n\n ### IMPORT DATA ###')

# path_input = 'data/data_small/Ibex1_complete_v2_short2.csv'
path_input = 'data/data_small/Ibex1_complete_v2_short3.csv'
# path_input = '../Data Dead-reckoning/Ibex complete data/Ibex1_complete_v2.csv'

df = pd.read_csv(path_input, na_values="NA")
df['DateTime'] = pd.to_datetime(df['DateTime'])
df.dropna(inplace=True, subset = ['DDMT_Latitude', 'DDMT_Longitude'])



print('\n\n\n ### CONVERT LAT-LON INTO X-Y ###')

transformer_from_latlon_to_xy = Transformer.from_crs("epsg:4326", "epsg:3857")
pts_real_x, pts_real_y = transformer_from_latlon_to_xy.transform(df['DDMT_Latitude'], df['DDMT_Longitude'])
df['X'], df['Y'] = pts_real_x, pts_real_y


# _____ GET DATA FROM THE REGION ______________________________________________
print('\n\n\n ### IMPORT MAP ELEVATION DATA ###')

minlon, maxlon = min(df['DDMT_Longitude']), max(df['DDMT_Longitude'])
minlat, maxlat = min(df['DDMT_Latitude']), max(df['DDMT_Latitude'])

minlon, maxlon = math.floor(minlon * 100.0) / 100.0, math.ceil(maxlon * 100.0) / 100.0
minlat, maxlat = math.floor(minlat * 100.0) / 100.0, math.ceil(maxlat * 100.0) / 100.0


grid = pygmt.datasets.load_earth_relief(
            "01s",
            region=[minlon, maxlon, minlat, maxlat],
            registration="gridline",
            use_srtm=True
        )

lat_small = grid.coords['lat'].values
lon_small = grid.coords['lon'].values
elev = grid.data

lon_grid, lat_grid = np.meshgrid(lon_small, lat_small)
print(lat_grid[:3, :3])
print(lon_grid[:3, :3])
print(elev.shape, lat_grid.shape, lon_grid.shape)


# _____ COMPUTE Z _____________________________________________________________
print('\n\n\n ### COMPUTE Z ON THE DATAFRAME ###')

print('Latitude interval (map): ', lat_grid.min(), lat_grid.max())
print('Latitude interval (df):  ', min((df['DDMT_Latitude'])), max((df['DDMT_Latitude'])))
print('Longitude interval (map):', lon_grid.min(), lon_grid.max())
print('Longitude interval (df): ', min((df['DDMT_Longitude'])), max((df['DDMT_Longitude'])))

points = np.transpose(np.vstack((lat_grid.flatten(), lon_grid.flatten())))
elev_interp_df = interpolate.griddata(points, elev.flatten(), 
                                      (np.array(df['DDMT_Latitude']), np.array(df['DDMT_Longitude'])),
                                      method='cubic')
df['Z'] = elev_interp_df

print('\nDataframe:    ', df.shape)
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].head(5))
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*1])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*2])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*3])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*4])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*5])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[df['Z']==min(df['Z'])])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[df['Z']==max(df['Z'])])



# _____ COMPUTE GRADIENT ______________________________________________________
print('\n\n\n ### COMPUTE GRADIENT EACH STEP ###')

# gradient has to be computed each meters (not each arc second)
#   > we have to interpolate elevation each meter
x_from_map, y_from_map = transformer_from_latlon_to_xy.transform(lat_grid.flatten(), lon_grid.flatten())

step_inter_grid = 10 # m
x_each_step = np.arange(min(df['X'])-step_inter_grid, max(df['X'])+step_inter_grid, step_inter_grid)
y_each_step = np.arange(min(df['Y'])-step_inter_grid, max(df['Y'])+step_inter_grid, step_inter_grid)
x_each_step_grid, y_each_step_grid = np.meshgrid(x_each_step, y_each_step)
print('Size of x-y', x_each_step.shape, y_each_step.shape)
print(x_each_step_grid.shape, y_each_step_grid.shape)

points = np.transpose(np.vstack((x_from_map, y_from_map)))
elev_interp_each_step = interpolate.griddata(points, elev.flatten(), 
                                              (x_each_step_grid, y_each_step_grid),
                                              method='cubic')

gradient_y_each_step, gradient_x_each_step = np.gradient(elev_interp_each_step)
gradient_x_each_step = gradient_x_each_step / step_inter_grid
gradient_y_each_step = gradient_y_each_step / step_inter_grid
# Now, we have the real gradient of z (in m/m) each step

print('\nSize of the gradients', gradient_x_each_step.shape, gradient_y_each_step.shape)

print('Gradient each step')
print(gradient_x_each_step[:5, :5])
print(gradient_y_each_step[:5, :5])


print('\n\n\n ### COMPUTE GRADIENT ON THE DATAFRAME ###')

points = np.transpose(np.vstack((x_each_step_grid.flatten(), y_each_step_grid.flatten())))
gradient_x_interp_df = interpolate.griddata(points,
                                            gradient_x_each_step.flatten(), 
                                            (np.array(df['X']), 
                                                np.array(df['Y'])),
                                            method='cubic')
gradient_y_interp_df = interpolate.griddata(points,
                                            gradient_y_each_step.flatten(), 
                                            (np.array(df['X']), 
                                                np.array(df['Y'])),
                                            method='cubic')
df['GradientX'] = gradient_x_interp_df
df['GradientY'] = gradient_y_interp_df
# we have the real gradient at the position of the animal at each instant

print('GradientX', df['GradientX'][:5], df['GradientX'][-5:])
print('GradientY', df['GradientY'][:5], df['GradientY'][-5:])



# _____ COMPUTE SLOPE _________________________________________________________
print('\n\n\n ### COMPUTE AZIMUTH ON THE DATAFRAME ###')

# scalar product of direction of the animal and gradient
# AZIMUTH
geodesic = Geod(ellps='WGS84')
fwd_azimuth, back_azimuth, distance = geodesic.inv(np.array(df['DDMT_Longitude'][:-1]),
                                                   np.array(df['DDMT_Latitude'][:-1]),
                                                   np.array(df['DDMT_Longitude'][1:]),
                                                   np.array(df['DDMT_Latitude'][1:]))
df['Distance_comp'] = [None] + list(distance)
# geodesic.inv azimuth: NORTH = 0° ; EAST = 90° ; WEST = -90°, SOUTH = 180°
# here, we want 0° = x_dir = EAST ; 90° = y_dir = NORTH ; -90° = -y_dir = SOUTH
#   > we take -azimuth to rotate in counter-clock direction 
#         and +90° to set the reference at the EAST 
azimuth = - np.array(fwd_azimuth) + 90
df['Azimuth_comp'] = list(azimuth) + [None]

print('Azimuth_comp', df['Azimuth_comp'][:5], df['Azimuth_comp'][-5:])


print('\n\n\n ### COMPUTE SLOPE ON THE DATAFRAME ###')

# DIRECTION (norm = 1)
# matrix 2 rows and df.shape[0] columns
directions = np.vstack(( np.cos( np.array(df['Azimuth_comp']) * np.pi/180 ), 
                                      np.sin( np.array(df['Azimuth_comp']) * np.pi/180 )
                        )) 
# GRADIENTS AT EACH INSTANT
# matrix 2 rows and df.shape[0] columns
gradients_df = np.vstack((df['GradientX'], df['GradientY']))

# SLOPE
# it's the scalar product between direction and gradient at each instant
# slope = np.diag(np.dot(directions, gradients_df)) * 100     # *100 to get it into %
slope = np.sum(directions * gradients_df, axis=0) * 100

df['GradeFromTopo'] = slope

print('GradeFromTopo', df['GradeFromTopo'][:5], df['GradeFromTopo'][-5:])






# # _____ PLOTS _________________________________________________________________
print('\n\n\n ### PLOTS ###')

# # fig = px.scatter_3d(df, x='DDMT_Longitude', y='DDMT_Latitude', z='Altitude')
# # fig.show()

# # fig = px.scatter_3d(df, x='X', y='Y', z='Z')
# # fig.show()

plt.figure()
plt.plot(df['X'], df['DDMT_Longitude'])
plt.xlabel('X')
plt.ylabel('DDMT_Longitude')

plt.figure()
plt.plot(df['Y'], df['DDMT_Latitude'])
plt.xlabel('Y')
plt.ylabel('DDMT_Latitude')

plt.figure()
plt.plot(df['Z'], df['Altitude'], 'o', alpha=1/255)
plt.plot([min(df['Z']), max(df['Z'])], [min(df['Z']), max(df['Z'])], 'r--')
plt.xlabel('Z')
plt.ylabel('Altitude')

plt.figure()
plt.plot(df['DateTime'], df['GradeFromTopo'])
plt.xlabel('Time')
plt.ylabel('GradeFromTopo (%)')


plt.show()
