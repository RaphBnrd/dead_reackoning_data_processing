import numpy as np
from lmfit import Model
import matplotlib.pyplot as plt


def oscil(t, A, T):
    return A*np.sin(2*np.pi/T * t)*1/(t+1)

# np.random.seed(1)
t_max = 200
t = np.linspace(0, (t_max)**(1/1), 500)
t = t**1
amp, per = 3.5, 61
y = []
for x in t:
    if x < 1e-5:
        y.append(oscil(x, amp, per) + amp/10*np.random.uniform(-1e-5, 1e-5))
    else:
        y.append(oscil(x, amp, per) + amp/10*np.random.uniform(-1/x, 1/x))
y = np.array(y)
# len(t), ) / t
print(t)
print(y)

# plot data
plt.plot(t, y, 'bo', label='data', alpha=0.1)


# build Model
hmodel = Model(oscil)
# create lmfit Parameters, named from the arguments of `hyperbolic_equation`
# note that you really must provide initial values.
params = hmodel.make_params(A=5, T=57)
# set bounds on parameters
params['A'].min=0
params['T'].min=0

# do fit, print resulting parameters
result = hmodel.fit(y, params, t=t)
print(result.fit_report())

# plot best fit: not that great of fit, really
plt.plot(t, result.best_fit, 'r--', label='fit')

# calculate the (1 sigma) uncertainty in the predicted model
# and plot that as a confidence band
dy = result.eval_uncertainty(result.params, sigma=1)   
print("\n\nConfidence intervalls:")
print(dy)
print("\n\n")
plt.fill_between(t,
                 result.best_fit-dy,
                 result.best_fit+dy,
                 color="#AB8888",
                 label='uncertainty band of fit')

# now evaluate the model for other values, predicting future values
future_t = np.linspace(t_max+1, 2*t_max+1, 300)
future_y = result.eval(t=future_t)

plt.plot(future_t, future_y, 'k--', label='prediction')

# ...and calculate the 1-sigma uncertainty in the future prediction
# for 95% confidence level, you'd want to use `sigma=2` here:
future_dy = result.eval_uncertainty(t=future_t, sigma=1)

print("### Prediction\n# Day  Prod     Uncertainty")

for t, y, eps in zip(future_t, future_y, future_dy):
    print(" {:.2f}   {:.5f} +/- {:.5f}".format(t, y, eps))

plt.fill_between(future_t,
                 future_y-future_dy,
                 future_y+future_dy,
                 color="#ABABAB",
                 label='uncertainty band of prediction')

plt.legend()
plt.show()