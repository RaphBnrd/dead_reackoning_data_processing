# from lmfit import Model
# from numpy import exp, linspace, random
# import matplotlib.pyplot as plt

# # define model
# def gaussian(x, amp, cen, wid):
#     return amp * exp(-(x-cen)**2 / wid)
# gmodel = Model(gaussian)
# print(f'parameter names: {gmodel.param_names}')
# print(f'independent variables: {gmodel.independent_vars}')
# # define parameters
# params = gmodel.make_params(cen=0.3, amp=3, wid=1.25)

# # evaluate the model
# x_eval = linspace(0, 10, 201)
# y_eval = gmodel.eval(params, x=x_eval)

# # fit
# result = gmodel.fit(y, params, x=x)



# # plt.figure()
# # plt.scatter(x_eval,y_eval)
# # plt.show()






import matplotlib.pyplot as plt
from numpy import exp, loadtxt, pi, sqrt

from lmfit import Model

data = loadtxt('test_fit_model/model_1d_gauss.dat')
x = data[:, 0]
y = data[:, 1]


def gaussian(x, amp, cen, wid):
    """1-d gaussian: gaussian(x, amp, cen, wid)"""
    return (amp / (sqrt(2*pi) * wid)) * exp(-(x-cen)**2 / (2*wid**2))


gmodel = Model(gaussian)
result = gmodel.fit(y, x=x, amp=5, cen=5, wid=1)

print(result.fit_report())
print(result.params)
print(result.params['amp'].value)

plt.plot(x, y, 'o')
plt.plot(x, result.init_fit, '--', label='initial fit')
plt.plot(x, result.best_fit, '-', label='best fit')
plt.legend()
plt.show()