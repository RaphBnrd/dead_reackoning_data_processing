import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt


"""
See this for confidence intervalls : 
https://stackoverflow.com/questions/39434402/how-to-get-confidence-intervals-from-curve-fit
"""




times = [120, 300, 480, 660, 840, 1020]
grade_centers = [k for k in range(-5, 27, 2)]

v_rec3D_short = np.array([
            [0.68949974, 0.40878112, 0.39128839, 0.35606555, 0.31537876, 0.30261373],   # -5
            [0.7690573 , 0.38925671, 0.37344101, 0.3681171 , 0.37068366, 0.35819098],
            [0.77045086, 0.49608066, 0.51321391, 0.39911272, 0.35637183, 0.35048134],
            [0.82837674, 0.60777238, 0.5357306 , 0.46465402, 0.45466729, 0.41358314],
            [0.86711418, 0.65279083, 0.55592357, 0.5420616 , 0.50367069, 0.47134094],
            [1.02142567, 0.68760621, 0.62013351, 0.55951919, 0.61568705, 0.56676384],   # 5
            [1.02746352, 0.64744649, 0.64092175, 0.68844124, 0.61468163, 0.55689379],
            [0.70698118, 0.74101104, 0.67562302, 0.63574472, 0.54205786, 0.52229445],
            [0.76535019, 0.55476138, 0.34038455, 0.29864383, 0.29059328, 0.27491041],
            [0.77622095, 0.31256921, 0.32512221, 0.30330722, 0.2944668 , 0.27922292],
            [0.63474767, 0.34393735, 0.30051066, 0.29036775, 0.29001826, 0.27800829],   # 15
            [0.52891072, 0.34884177, 0.29264021, 0.27539103, 0.27457226, 0.26801757],
            [0.33134619, 0.35623748, 0.29518689, 0.26416493, 0.2327678 , 0.26741487],
            [0.32706156, 0.36949616, 0.25667729, 0.24305947, 0.2306657 , 0.22343848],
            [0.31833868, 0.36988634, 0.2562664 , 0.24369274, 0.23331036, 0.23005597],
            [0.3298301 , 0.3612868 , 0.27023637, 0.24761051, 0.23598738, 0.18338884]])   # 25

def func_model(data, D, beta_0, beta_1, beta_2):
    t = data[0] # x=t (ou d)
    g = data[1] # y=g
    return D/t + 1 / (beta_0*g**2 + beta_1*g + beta_2)



t, g, v = [], [], []
for l in range(len(times)):
    for k in range(len(grade_centers)):
        t.append(times[l])
        g.append(grade_centers[k])
        v.append(v_rec3D_short[k][l])

x_data, y_data, z_data = t, g, v


###### First fitting ######
# Fit
parameters1, covariance1 = curve_fit(func_model, [x_data, y_data], z_data, 
                                     p0=[250, 0.05, -1, 5],
                                     # bounds=([0,0,0,0], [1000,30,2,1],
                                     bounds=([0,0,-2,-10], [2000,5,2,10]))
print("\n\nParamètres du fitting 1 : ")
print(parameters1)
residuals1 = np.array(z_data) - func_model(np.array([x_data, y_data]), *parameters1)
print("\nRésidus : ")
print(residuals1)
print("\nCovariance : ")
print(covariance1)

np.sqrt(np.diagonal(covariance1))

# Surface
model_x_data = np.linspace(min(x_data), max(x_data), 50)
model_y_data = np.linspace(min(y_data), max(y_data), 50)
X, Y = np.meshgrid(model_x_data, model_y_data)
Z1 = func_model(np.array([X, Y]), *parameters1)


# ###### Second fitting (without outlayers) ######
# # Fit
# parameters1, covariance1 = curve_fit(func_model, [x_data, y_data], z_data, 
#                                      p0=[250, 0.05, -1, 5],
#                                      # bounds=([0,0,0,0], [1000,30,2,1],
#                                      bounds=([0,0,-2,-10], [2000,5,2,10]))
# print("\n\nParamètres du fitting 1 : ")
# print(parameters1)
# print("\n")
# residuals1 = np.array(z_data) - func_model(np.array([x_data, y_data]), *parameters1)

# # Surface
# model_x_data = np.linspace(min(x_data), max(x_data), 50)
# model_y_data = np.linspace(min(y_data), max(y_data), 50)
# X, Y = np.meshgrid(model_x_data, model_y_data)
# Z1 = func_model(np.array([X, Y]), *parameters1)








# Plot
fig = plt.figure()
ax = plt.axes(projection='3d')
# ax.plot_surface(X, Y, Z_test)
ax.plot_surface(X, Y, Z1)
ax.scatter3D(x_data, y_data, z_data, color='red')
ax.set_xlabel('Time (s)')
ax.set_ylabel('Grade (%)')
ax.set_zlabel('Velocity (m/s)')

plt.show()

