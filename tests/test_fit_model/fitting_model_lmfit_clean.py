import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from lmfit import Model, Parameters


def function_model(d, g, D, a, b, c):
    # function use for speed - grade - duration fitting
    return D/d + 1 / (a*g**2 + b*g + c)


# figure for visualization
fig = plt.figure('fitting')
ax = plt.axes(projection='3d')

# import data and create grade, speed and duration______________________________________________________________________
# df = pd.read_csv(r'tests/test_fit_model/test_fitting_data_stv2.csv')
df = pd.read_csv('data/20220610-10h/Record3D_D20220610-H093256.csv', header=1)
df.drop(df[df['t'] < 100].index, inplace=True)
# grade = df.s.values
grade = df.g.values
speed = df.v.values
duration = df.t.values
print(df.head())

# fitting model configuration___________________________________________________________________________________________
model = Model(function_model, independent_vars=['d', 'g'])
D0, a0, b0, c0 = 100, 0.05, 0, 5 # set the starting point for the fitting
params = Parameters()
params.add('apex', value=-b0 / (2*a0), min=-20, max=20)  # apex is situated between grade of -20 and +20%
params.add('delta',  value=b0**2 - 4*a0*c0, max=0)  # delta = b²-4ac < 0 => the polynomial function is strictly positive
params.add('D', value=D0,  min=0)
params.add('a', value=a0,  min=0)
params.add('b', expr='-apex*2*a')  # b = apex*2*a
params.add('c', expr='(b ** 2 - delta) / (4 * a)')  # c = (b²-delta) / (4a)

# first step fitting with all record speed data_________________________________________________________________________
result1 = model.fit(speed, params, d=duration, g=grade)

# second step - removing outlier and fit again__________________________________________________________________________
residuals = result1.residual
z_residuals = (residuals - np.mean(residuals)) / np.std(residuals)  # z-score of the residuals
z_threshold = 2  # threshold to classify as outlier (2 = 95%; 3 = 99%)
is_outlier = (abs(z_residuals) > z_threshold)
ax.scatter3D(duration[is_outlier], grade[is_outlier], speed[is_outlier],
             marker='X', color='red', label='outliers')  # plot for visualization
# remove outlier for grade, speed and duration
grade2 = grade[~is_outlier]
speed2 = speed[~is_outlier]
duration2 = duration[~is_outlier]
result2 = model.fit(speed2, params, d=duration2, g=grade2)  # fit again without outliers

# third step only with positive residuals_______________________________________________________________________________
# reset fitting parameters so starting points are the coeficients determined from step 2 and boundaries
# correspond to CI95% of the parameters
params['D'].value = result2.params['D'].value
params['a'].value = result2.params['a'].value
params['apex'].value = result2.params['apex'].value
params['delta'].value = result2.params['delta'].value
params['D'].min = params['D'].value - 2 * result2.params['D'].stderr
params['D'].max = params['D'].value + 2 * result2.params['D'].stderr
params['a'].min = params['a'].value - 2 * result2.params['a'].stderr
params['a'].max = params['a'].value + 2 * result2.params['a'].stderr
params['apex'].min = params['apex'].value - 2 * result2.params['apex'].stderr
params['apex'].max = params['apex'].value + 2 * result2.params['apex'].stderr
params['delta'].min = params['delta'].value - 2 * result2.params['delta'].stderr
params['delta'].max = params['delta'].value + 2 * result2.params['delta'].stderr
#remove negative residuals
residuals2 = result2.residual
# /!\ positive residuals correspond to residual < 0 because lmfit calculate residuals as predicted - observed
is_positive_res = (residuals2 < 0)
ax.scatter3D(duration2[~is_positive_res], grade2[~is_positive_res], speed2[~is_positive_res],
             marker='X', color='blue', label='negative residual')   # plot for visualization
# conserve only positive residuals for grade, speed and duration
grade3 = grade2[is_positive_res]
speed3 = speed2[is_positive_res]
duration3 = duration2[is_positive_res]
result3 = model.fit(speed3, params, d=duration3, g=grade3)

# print the report of all 3 fittings____________________________________________________________________________________
print(result1.fit_report())
print(result2.fit_report())
print(result3.fit_report())

# plot the results for visualization ___________________________________________________________________________________
d_grid = np.arange(min(duration), max(duration), 10)
g_grid = np.arange(min(grade), max(grade), 1)
d_grid, g_grid = np.meshgrid(d_grid, g_grid)
s_surf = model.eval(d=d_grid, g=g_grid,
                    D=result1.params['D'].value,
                    a=result1.params['a'].value,
                    b=result1.params['b'].value,
                    c=result1.params['c'].value)
ax.scatter3D(duration3, grade3, speed3, color='black', label='remaining data')
ax.legend()
surf1 = ax.plot_surface(d_grid, g_grid, s_surf,
                        cmap=cm.jet,
                        label='fits(d,g)',
                        alpha=0.5,
                        linewidth=0,
                        antialiased=True)
ax.set_xlabel('Duration (s)')
ax.set_ylabel('Grade (%)')
ax.set_zlabel('Speed (m/s)')
ax.set_title("Speed - Grade - Duration relationship")
plt.show()