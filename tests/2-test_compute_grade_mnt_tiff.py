import pandas as pd
import numpy as np
import math
from scipy import interpolate
import rasterio as rio

from pyproj import Transformer

import plotly.express as px
import plotly.graph_objects as go
import plotly.figure_factory as ff
import matplotlib.pyplot as plt


# _____ INPUT DATA ____________________________________________________________
print('\n\n\n ### IMPORT DATA ###')

# path_input = 'data/data_small/Ibex1_complete_v2_short2.csv'
path_input = 'data/data_small_1/Ibex1_complete_v2_short3.csv'
# path_input = '../Data Dead-reckoning/Ibex complete data/Ibex1_complete_v2.csv'

df = pd.read_csv(path_input, na_values="NA")
df['DateTime'] = pd.to_datetime(df['DateTime'])
df.dropna(inplace=True, subset = ['DDMT_Latitude', 'DDMT_Longitude'])



print('\n\n\n ### CONVERT LAT-LON INTO X-Y ###')

transformer_from_latlon_to_xy = Transformer.from_crs("epsg:4326", "epsg:2154")
pts_real_x, pts_real_y = transformer_from_latlon_to_xy.transform(df['DDMT_Latitude'], df['DDMT_Longitude'])
df['X'], df['Y'] = pts_real_x, pts_real_y


# _____ GET DATA FROM THE REGION ______________________________________________
print('\n\n\n ### IMPORT MAP ELEVATION DATA ###')

min_x, max_x = min(df['X']), max(df['X'])
min_y, max_y = min(df['Y']), max(df['Y'])
min_x, max_x = math.floor(min_x * 100.0) / 100.0, math.ceil(max_x * 100.0) / 100.0
min_y, max_y = math.floor(min_y * 100.0) / 100.0, math.ceil(max_y * 100.0) / 100.0

filename = 'mnt_BELLEDONNE_1m_l93_raphael.tif'
ref_map_x = (934179, 947858)
ref_map_y = (6458622, 6469512)
step = 1 # m

dem = rio.open(filename)
elev = dem.read(1).astype('float64')
x_small = np.arange(ref_map_x[0], ref_map_x[1]+1, step)
y_small = np.arange(ref_map_y[0], ref_map_y[1]+1, step)


x_grid, y_grid = np.meshgrid(x_small, y_small)
print(x_grid[:3, :3])
print(y_grid[:3, :3])
print(elev.shape, x_grid.shape, y_grid.shape)


# _____ MASK CLOSE TO THE TRAJECTORY __________________________________________
print('\n\n\n ### MASK CLOSE TO THE TRAJECTORY ###')

mask_traj = np.zeros(elev.shape)
x0, y0 = 0, 1
x, y = np.array(df['X']), np.array(df['Y'])
"""
We need to complete this part (compute a mask raster around the trajectory)
"""




# _____ COMPUTE Z _____________________________________________________________
print('\n\n\n ### COMPUTE Z ON THE DATAFRAME ###')

print('X interval (map):', x_grid.min(), x_grid.max())
print('X interval (df): ', min((df['X'])), max((df['X'])))
print('Y interval (map): ', y_grid.min(), y_grid.max())
print('Y interval (df):  ', min((df['Y'])), max((df['Y'])))

points = np.transpose(np.vstack((x_grid.flatten(), y_grid.flatten())))
elev_interp_df = interpolate.griddata(points, elev.flatten(), 
                                      (np.array(df['X']), np.array(df['Y'])),
                                      method='cubic')
df['Z'] = elev_interp_df

print('\nDataframe:    ', df.shape)
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].head(5))
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*1])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*2])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*3])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*4])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[3600*4*5])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[df['Z']==min(df['Z'])])
print(df[['DDMT_Longitude','DDMT_Latitude','Altitude', 'Z', 'X', 'Y']].loc[df['Z']==max(df['Z'])])



# _____ COMPUTE GRADIENT ______________________________________________________
print('\n\n\n ### COMPUTE GRADIENT EACH STEP ###')

gradient_y_each_step, gradient_x_each_step = np.gradient(elev)
gradient_x_each_step = gradient_x_each_step / step
gradient_y_each_step = gradient_y_each_step / step
# Now, we have the real gradient of z (in m/m) each step

print('\nSize of the gradients', gradient_x_each_step.shape, gradient_y_each_step.shape)

print('Gradient each step')
print(gradient_x_each_step[:5, :5])
print(gradient_y_each_step[:5, :5])


print('\n\n\n ### COMPUTE GRADIENT ON THE DATAFRAME ###')

points = np.transpose(np.vstack((x_grid.flatten(), y_grid.flatten())))
gradient_x_interp_df = interpolate.griddata(points,
                                            gradient_x_each_step.flatten(), 
                                            (np.array(df['X']), np.array(df['Y'])),
                                            method='cubic')
gradient_y_interp_df = interpolate.griddata(points,
                                            gradient_y_each_step.flatten(), 
                                            (np.array(df['X']), np.array(df['Y'])),
                                            method='cubic')
df['GradientX'] = gradient_x_interp_df
df['GradientY'] = gradient_y_interp_df
# we have the real gradient at the position of the animal at each instant

print('GradientX', df['GradientX'][:5], df['GradientX'][-5:])
print('GradientY', df['GradientY'][:5], df['GradientY'][-5:])



# _____ COMPUTE SLOPE _________________________________________________________
print('\n\n\n ### COMPUTE AZIMUTH ON THE DATAFRAME ###')

# scalar product of direction of the animal and gradient
# AZIMUTH
x_diff, y_diff = np.diff(df['X']), np.diff(df['Y'])
y_sign = np.sign(y_diff)
idx_arctan_not_def = np.where(x_diff==0)[0]
# We will get some NA values where arctan is not defined (+/- pi/2)
azimuth_comp = np.arctan( (y_diff) / (x_diff) ) * 180/np.pi + (x_diff < 0) * 180
azimuth_comp[idx_arctan_not_def] = 90 * y_sign[idx_arctan_not_def]
# to avoid problems of bounds between 359° and 1° for example (now azimuth can be between -inf and +inf)
azimuth_comp = np.rad2deg(np.unwrap(np.deg2rad(azimuth_comp)))
df['Azimuth_comp'] = list(azimuth_comp) + [None]

print('Azimuth_comp', df['Azimuth_comp'][:5], df['Azimuth_comp'][-5:])


print('\n\n\n ### COMPUTE SLOPE ON THE DATAFRAME ###')

# DIRECTION (norm = 1)
# matrix 2 rows and df.shape[0] columns
directions = np.vstack(( np.cos( np.array(df['Azimuth_comp']) * np.pi/180 ), 
                                      np.sin( np.array(df['Azimuth_comp']) * np.pi/180 )
                        )) 
# GRADIENTS AT EACH INSTANT
# matrix 2 rows and df.shape[0] columns
gradients_df = np.vstack((df['GradientX'], df['GradientY']))

# SLOPE
# it's the scalar product between direction and gradient at each instant
# slope = np.diag(np.dot(directions, gradients_df)) * 100     # *100 to get it into %
slope = np.sum(directions * gradients_df, axis=0) * 100

df['GradeFromTopo'] = slope

print('GradeFromTopo', df['GradeFromTopo'][:5], df['GradeFromTopo'][-5:])






# # _____ PLOTS _________________________________________________________________
print('\n\n\n ### PLOTS ###')

# # fig = px.scatter_3d(df, x='DDMT_Longitude', y='DDMT_Latitude', z='Altitude')
# # fig.show()

# # fig = px.scatter_3d(df, x='X', y='Y', z='Z')
# # fig.show()

plt.figure()
plt.plot(df['X'], df['DDMT_Longitude'])
plt.xlabel('X')
plt.ylabel('DDMT_Longitude')

plt.figure()
plt.plot(df['Y'], df['DDMT_Latitude'])
plt.xlabel('Y')
plt.ylabel('DDMT_Latitude')

plt.figure()
plt.plot(df['Z'], df['Altitude'], 'o', alpha=1/255)
plt.plot([min(df['Z']), max(df['Z'])], [min(df['Z']), max(df['Z'])], 'r--')
plt.xlabel('Z')
plt.ylabel('Altitude')

plt.figure()
plt.plot(df['DateTime'], df['GradeFromTopo'])
plt.xlabel('Time')
plt.ylabel('GradeFromTopo (%)')


plt.show()
