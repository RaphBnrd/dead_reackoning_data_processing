
import numpy as np
import rasterio as rio
from rasterio.plot import show
import matplotlib.pyplot as plt
import matplotlib as mpl


filename = 'mnt_BELLEDONNE_1m_l93_raphael.tif'
# ref_map_lon = (5.9825, 6.1621)
# ref_map_lat = (45.1871, 45.2803)
# ref_map_x = (934178.57, 947858.61)
# ref_map_y = (6458621.84, 6469512.86)

ref_map_x = (934179, 947858)
ref_map_y = (6458622, 6469512)
step = 1 # m

x_small = np.arange(ref_map_x[0], ref_map_x[1]+1, step)
y_small = np.arange(ref_map_y[0], ref_map_y[1]+1, step)

print(y_small.shape, x_small.shape)
print(x_small[:3], x_small[-3:])
print(y_small[:3], y_small[-3:])


dem = rio.open(filename)
dem_array = dem.read(1).astype('float64')

print(dem_array.shape)
# print(dem_array[:5, :10])
print(dem_array.min(), dem_array.max())

# fig, ax = plt.subplots(1, figsize=(12, 12))
# show(dem_array, cmap='Greys_r', ax=ax)
# show(dem_array, contour=True, ax=ax, linewidths=0.7)




# No. of Bands and Image resolution
# print(dem.count, dem.height, dem.width)
# Coordinate Reference System
# print(dem.crs)

plt.figure()
clev = np.arange(dem_array[:1000, :1000].min()-0.1,dem_array[:1000, :1000].max()+0.1,.1)
plt.contourf(x_small[:1000], y_small[:1000], dem_array[:1000, :1000]) #, clev)
plt.colorbar(label='Z')
# plt.colorbar(mpl.cm.ScalarMappable(norm=mpl.colors.Normalize(vmin=dem_array[:1000, :1000].min()-0.1,
#                                                              vmax=dem_array[:1000, :1000].max()+0.1)), 
#             label='Z')
plt.xlabel('x')
plt.ylabel('y')

plt.show()


















# dem_richdem = rd.rdarray(dem_array, no_data=-9999)


# tfile = tiff.imread(filename)
# print(tfile.shape)
# print(tfile[10, 10])
# tiff.imshow(tfile)


# # Which band are you interested. 
# # 1 if there is only one band
# band_of_interest = 1

# # Row and Columns of the raster you want to know
# # the value
# row_of_interest = 30
# column_of_interest = 50

# # open the raster and close it automatically
# # See https://stackoverflow.com/questions/1369526
# with rasterio.open(filename) as dataset:
#     band = dataset.read(band_of_interest)
#     value_of_interest = band(row_of_interest, column_of_interest)

# print(value_of_interest)