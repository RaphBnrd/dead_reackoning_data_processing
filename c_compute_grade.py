import numpy as np
import math
from scipy import interpolate
import pygmt
from pyproj import Transformer


def get_elevation_map(df, column_lon='Longitude', 
                          column_lat='Latitude'):
    """
    Returns the elevation on the moving space on the grid each arc second
    Returns also longitude and latitude grids
      (columns = different longitudes)
      (rows = different latitudes)
    Input:
        . df is the dataframe containing all the data of the animal
        . column_lon is the name of the column containing longitudes
        . column_lat is the name of the column containing latitudes
    Output:
        . lon_grid is the grid of all longitudes of import each arc second (in °)
        . lat_grid is the grid of all latitudes of import each arc second (in °)
        . elev_grid is the corresponding elevation on the grid each arc second (in meters)
    """
    # Borders of the zone of interest (rounded at 0.01° larger)
    minlon, maxlon = min(df[column_lon]), max(df[column_lon])
    minlat, maxlat = min(df[column_lat]), max(df[column_lat])
    minlon, maxlon = math.floor(minlon * 100.0) / 100.0 - 0.01, math.ceil(maxlon * 100.0) / 100.0 + 0.01
    minlat, maxlat = math.floor(minlat * 100.0) / 100.0 - 0.01, math.ceil(maxlat * 100.0) / 100.0 + 0.01
    # Grid imported
    grid = pygmt.datasets.load_earth_relief(
                "01s",
                region=[minlon, maxlon, minlat, maxlat],
                registration="gridline",
                use_srtm=True
            )

    lon_small = grid.coords['lon'].values
    lat_small = grid.coords['lat'].values
    elev_grid = grid.data

    lon_grid, lat_grid = np.meshgrid(lon_small, lat_small)

    return lon_grid, lat_grid, elev_grid


def add_columns_topo_animal(df, column_lon='Longitude', column_lat='Latitude',
                            column_lon_filtered='Longitude_filtered',
                            column_lat_filtered='Latitude_filtered'):
    """
    Adds 6 columns to the dataframe corresponding to
      - 'X' and 'Y': the x and y position in meters (converted from longitude, latitude)
      - 'Z': the altitude interpolated (on the raw trajectory) from the map imported
      - 'X_from_filter' and 'Y_from_filter': the x and y position in meters 
        (converted from longitude filtered, latitude filtered)
      - 'Z_from_filter': the altitude interpolated (on the filtered trajectory) from the map imported
    Interpolation method is cubic
    Input:
        . df is the dataframe containing all the data of the animal
        . column_lon is the name of the column containing raw longitudes
        . column_lat is the name of the column containing raw latitudes
        . column_lon_filtered is the name of the column containing filtered longitudes
        . column_lat_filtered is the name of the column containing filtered latitudes
    Output:
        . df is the dataframe containing all the data of the animal with new columns
        . lon_grid is the grid of all longitudes of import each arc second (in °)
        . lat_grid is the grid of all latitudes of import each arc second (in °)
        . x_grid is the grid of all x coordinates of import each arc second (in m)
        . y_grid is the grid of all y coordinates of import each arc second (in m)
        . elev_grid is the corresponding elevation on the grid each arc second (in meters)
    """
    transformer_from_latlon_to_xy = Transformer.from_crs("epsg:4326", "epsg:2154")
    lon_grid, lat_grid, elev_grid = get_elevation_map(df, column_lon=column_lon, column_lat=column_lat)

    # Add columns x and y
    x_converted, y_converted = transformer_from_latlon_to_xy.transform(df[column_lat], df[column_lon])
    df['X'], df['Y'] = x_converted, y_converted
    # Add column z
    points_latlon_grid = np.transpose(np.vstack((lat_grid.flatten(), lon_grid.flatten())))
    elev_interp_df = interpolate.griddata(points_latlon_grid, elev_grid.flatten(), 
                                          (np.array(df[column_lat]), np.array(df[column_lon])),
                                          method='cubic')
    df['Z'] = elev_interp_df

    # Add columns x and y from_filter
    x_from_filter_converted, y_from_filter_converted = transformer_from_latlon_to_xy.transform(df[column_lat_filtered], df[column_lon_filtered])
    df['X_from_filter'], df['Y_from_filter'] = x_from_filter_converted, y_from_filter_converted
    # Add column z from_filter
    points_latlon_grid = np.transpose(np.vstack((lat_grid.flatten(), lon_grid.flatten())))
    elev_from_filter_interp_df = interpolate.griddata(points_latlon_grid, elev_grid.flatten(), 
                                          (np.array(df[column_lat_filtered]), np.array(df[column_lon_filtered])),
                                          method='cubic')
    df['Z_from_filter'] = elev_from_filter_interp_df

    # Convert lat lon into x y for the grid of topography
    x_grid_flat, y_grid_flat = transformer_from_latlon_to_xy.transform(lat_grid.flatten(), lon_grid.flatten())
    x_grid, y_grid = x_grid_flat.reshape(lon_grid.shape), y_grid_flat.reshape(lat_grid.shape)

    return df, lon_grid, lat_grid, x_grid, y_grid, elev_grid


def add_column_grade_simple(df, columnX='X_from_filter', columnY='Y_from_filter',
                            columnZ='Z_from_filter'):
    """
    Adds column Grade to the dataframe ( = delta_h / dist 2D)
    Input:
        . df is the dataframe containing all the data of the animal
        . columnX is the name of the column containing X data
        . columnY is the name of the column containing Y data
        . columnZ is the name of the column containing Z data
    Output:
        . df is the dataframe containing all the data of the animal with new columns (Grade)
        . idx_first_grade is the index of the first finite value of the grade
        . idx_last_grade is the index of the last finite value of the grade
    """
    # Compute horizontal distance and delta altitude
    dx, dy = df[columnX].diff(), df[columnY].diff() # between k and k-1
    dist_2D = np.sqrt(dx**2 + dy**2) # between k and k-1
    delta_h = df[columnZ].diff() # between k and k-1
    # Get the grade
    grade = delta_h / dist_2D * 100
    # 0/0 gives NaN, x/0 gives +/- inf   -->   we replace it by the prevo=ious value of grade
    grade[~np.isfinite(grade)] = np.nan
    grade = grade.ffill()

    df['Grade'] = grade # between k and k-1

    idx_first_grade = np.where(np.isfinite(df['Grade']))[0][0]
    idx_last_grade = np.where(np.isfinite(df['Grade']))[0][-1]

    return df, idx_first_grade, idx_last_grade
