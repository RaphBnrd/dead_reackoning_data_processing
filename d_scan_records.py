import numpy as np
from g_computation_functions import movmean, movstd


def scan_records_2D(df, sampling_period, 
                    t_window_widths, idxs_start_moving, idxs_end_moving, 
                    column_records='Velocity_filtered', column_time='DateTime'):
    """
    We scan the records of a signal on a certain time window
    Input:
        . df is the dataframe containing all the data of the animal
        . sampling_period is the sampling perdiod of the dataframe
        . t_window_widths is the list of time window width for the scan (in sec)
        . idxs_start_moving is the list of index when the animal is considered
          as starting to move (start indices of clusters we will scan)
        . idxs_end_moving is the list of index when the animal is considered
          as ending to move (start indices of clusters we will scan)
        . column_records is the name of the column containing the value of 
        interest for our records
        . column_time is the name of the column containing the datetimes of the data
    Output:
        . t2 is the list of time windows with which we search records
        . rec2 is the list of records on these time windows
        . t_start2 is the list of beginning times of the windows where we found each records
        . t_end2 is the list of ending times of the windows where we found each records
    """
    # Init the list of data on records (each index corresponds to the window width in t_window_widths)
    all_data_rec2 = [None for l in range(len(t_window_widths))]
    # all_data_rec2 will store (record, (start_rec, end_rec)) for the record for each time window
    for l in range(len(t_window_widths)):
        t_scan = t_window_widths[l]
        idx_len_window = int(t_scan/sampling_period)
        # For all samples where the animal is moving
        for k in range(len(idxs_start_moving)):
            idx_min_window, idx_max_window = idxs_start_moving[k], idxs_end_moving[k] # NB: idx_max_window is outside the window possible range
            if (df[column_time][idx_max_window-1]-df[column_time][idx_min_window]).delta/1e9 >= t_scan: # if the sample is big enough
                # We compute moving mean in this part of the sample
                # idx k in mov_mean is at k+idx_min_window+(idx_len_window//2) in df
                mov_mean_value = movmean(np.array(df[column_records][idx_min_window:idx_max_window]), idx_len_window)
                this_rec = max(mov_mean_value)
                idx_movmean_rec = np.where(mov_mean_value == this_rec)[0][0]
                start_rec = df[column_time][idx_movmean_rec+idx_min_window]
                end_rec = df[column_time][idx_movmean_rec+idx_min_window+idx_len_window-1]
                # Is it a record compared to other samples (other sections/cluster of the data)
                is_record = False
                if all_data_rec2[l] == None: # if this case is nan
                    is_record = True
                elif this_rec > all_data_rec2[l][0]:
                    is_record = True
                # If it's a record, we put it in the list
                if is_record:
                    all_data_rec2[l] = (this_rec, (start_rec, end_rec))
    t2 = []
    rec2 = []
    t_start2, t_end2 = [], []
    for k in range(len(t_window_widths)):
        if all_data_rec2[k] != None:
            t2.append(t_window_widths[k])
            rec2.append(all_data_rec2[k][0])
            t_start2.append(all_data_rec2[k][1][0])
            t_end2.append(all_data_rec2[k][1][1])
    
    return t2, rec2, t_start2, t_end2





def scan_records_3D(df, sampling_period, 
                    t_window_widths, condition_interval_borders,
                    threshold_std_cond, idxs_start_moving, idxs_end_moving, 
                    column_records='Velocity_filtered', column_condition='Grade_filtered', 
                    column_time='DateTime'):
    """
    We scan the records of velocity on a certain time window for a range or grade
    Input:
        . df is the dataframe containing all the data of the animal
        . sampling_period is the sampling perdiod wanted after the
          interpolation (float in sec)
        . t_window_widths is the list of time window width for the scan (in sec)
        . condition_interval_borders is the list of interval borders of each scan
          intervals that constraint the scan (e.g. grade in %)
        . threshold_std_cond is the threshold of standard deviation (of the value that 
          correspond to interval conditions, e.g. grade) on which we can find
          a record (if the std is too high, we cannot consider the window as homogene enough)
          (e.g. std for grade in %)
        . idxs_start_moving is the list of index when the animal is considered
          as starting to move (start indices of clusters we will scan)
        . idxs_end_moving is the list of index when the animal is considered
          as ending to move (start indices of clusters we will scan)
        . column_records is the name of the column containing the value of 
          interest for our records
        . column_condition is the name of the column containing the value
          that conditions the search in the scan (e.g. grade)
        . column_time is the name of the column containing the datetimes of the data
    Output:
        . t3 is the list of time windows with which we search records
        . cond3 is the list of grades found for the records
        . rec3 is the list of velocity records on these time windows for these grades
        . t_start3 is the list of beginning times of the windows where we found each records
        . t_end3 is the list of ending times of the windows where we found each records
    """
    # Init the list of list of data on records 
    # (each column index corresponds to the window width in t_window_widths)
    # (each row index corresponds to the interval in condition_interval_borders)
    all_data_rec3 = [[None for l in range(len(t_window_widths))] \
                    for k in range(len(condition_interval_borders)-1)]
    # all_data_rec3 will store (record, conditioned_value, (start_rec, end_rec)) for the record of each time window and each condition range
    for l in range(len(t_window_widths)):
        t_scan = t_window_widths[l]
        idx_len_window = int(t_scan/sampling_period)
        # For all samples where the animal is moving
        for k in range(len(idxs_start_moving)):
            idx_min_window, idx_max_window = idxs_start_moving[k], idxs_end_moving[k] # NB: idx_max_window is outside the window possible range
            if (df[column_time][idx_max_window-1]-df[column_time][idx_min_window]).delta/1e9 >= t_scan:
                # We compute moving means in this part of the sample
                # idx k in mov_mean is at k+idx_min_window+(idx_len_window//2) in df
                mov_mean_value = movmean(np.array(df[column_records][idx_min_window:idx_max_window]), idx_len_window)
                mov_mean_cond = movmean(np.array(df[column_condition][idx_min_window:idx_max_window]), idx_len_window)
                mov_std_cond = movstd(np.array(df[column_condition][idx_min_window:idx_max_window]), idx_len_window)
                # For each condition range, we search the record
                for k in range(len(condition_interval_borders)-1):
                    # Build a boolean array with True where mov_mean_cond is in the right interval
                    mov_mean_cond_interv = (mov_mean_cond>=condition_interval_borders[k]) & (mov_mean_cond<condition_interval_borders[k+1])
                    # We only keep time windows where the std of the condition is less than threshold_std_cond
                    mov_mean_cond_bool = mov_mean_cond_interv & (mov_std_cond <= threshold_std_cond)
                    is_record = False
                    if mov_mean_cond_bool.any(): # If we have data in this range of condition
                        this_rec = max(mov_mean_value[mov_mean_cond_bool])
                        # Is it a record compared to other samples (other sections/cluster of the data)
                        if all_data_rec3[k][l] == None: # if this case is nan
                            is_record = True
                        elif this_rec > all_data_rec3[k][l][0]:
                            is_record = True
                    if is_record:
                        # Search in the moving_mean the idxs where we have the condition interval and the record
                        idxs_this_rec = np.where((mov_mean_value==this_rec) & # we have this max 
                                                mov_mean_cond_bool)[0]         # we have this grade range
                        start_rec = df[column_time][idxs_this_rec[0]+idx_min_window]
                        end_rec = df[column_time][idxs_this_rec[0]+idx_min_window+idx_len_window-1]
                        # The condition value is the mean of the value for each records (if we have several records in this sample)
                        cond_value_this_rec = np.mean(mov_mean_cond[idxs_this_rec])
                        all_data_rec3[k][l] = (this_rec, cond_value_this_rec, (start_rec, end_rec))

    t3, cond3, rec3 = [], [], []
    t_start3, t_end3 = [], []
    for l in range(len(t_window_widths)):
        for k in range(len(condition_interval_borders)-1):
            if all_data_rec3[k][l] != None:
                t3.append(t_window_widths[l])
                cond3.append(all_data_rec3[k][l][1])
                rec3.append(all_data_rec3[k][l][0])
                t_start3.append(all_data_rec3[k][l][2][0])
                t_end3.append(all_data_rec3[k][l][2][1])
    
    return t3, cond3, rec3, t_start3, t_end3
