import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from lmfit import Model, Parameters

import datetime
import tkinter as tk


"""
Look at this for the method : 
https://lmfit.github.io/lmfit-py/model.html
https://stackoverflow.com/questions/59889441/prediction-intervals-for-hyperbolic-curve-fit-scipy

examples here: https://github.com/lmfit/lmfit-py/tree/master/examples
"""




##### _____ DEFAULT VARIABLES _____ #####
export = False
interactive_entry = False

f_min_tresh, f_max_tresh = 0, np.inf
t_min_tresh, t_max_tresh = 50, np.inf
v_min_tresh, v_max_tresh = 0, np.inf

file_name = 'data/20220704_open_sample_fvt/Ibex1_complete_v2_20170602-000000_TO_20170701-000000_RecordFvt_D20220701-H171047.csv'
name_file_export = 'data/20220704_open_sample_fvt/Ibex1_fitFvt_20170602-000000_TO_20170701-000000'





##### _____ INPUTS _____ #####
if interactive_entry:
    def button_select_csv():
        filetypes = (('csv files', '*.csv'), ('All files', '*.*'))
        var_file_name.set(tk.filedialog.askopenfilename(title='Select a file', initialdir='', filetypes=filetypes))
    win_entry = tk.Tk()
    win_entry.title('Import csv')
    # Input file
    var_file_name = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_file_name, width=50).grid(row=1, column=1, columnspan=3, padx=10, pady=10)
    var_file_name.set(file_name)
    tk.Button(win_entry, text='Browse', command=button_select_csv).grid(row=1, column=4, padx=10, pady=10)
    # Entry thresholds grades
    tk.Label(win_entry, text="Threshold force (min-max): ").grid(row=2, column=1, sticky=tk.W, padx=10, pady=10)
    var_f_min_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_f_min_tresh, width=5).grid(row=2, column=2, padx=10, pady=10)
    var_f_min_tresh.set(f_min_tresh)
    var_f_max_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_f_max_tresh, width=5).grid(row=2, column=3, padx=10, pady=10)
    var_f_max_tresh.set(f_max_tresh)
    # Entry thresholds times
    tk.Label(win_entry, text="Threshold time (min-max): ").grid(row=3, column=1, sticky=tk.W, padx=10, pady=10)
    var_t_min_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_t_min_tresh, width=5).grid(row=3, column=2, padx=10, pady=10)
    var_t_min_tresh.set(t_min_tresh)
    var_t_max_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_t_max_tresh, width=5).grid(row=3, column=3, padx=10, pady=10)
    var_t_max_tresh.set(t_max_tresh)
    # Entry thresholds velocities
    tk.Label(win_entry, text="Threshold velocity (min-max): ").grid(row=4, column=1, sticky=tk.W, padx=10, pady=10)
    var_v_min_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_v_min_tresh, width=5).grid(row=4, column=2, padx=10, pady=10)
    var_v_min_tresh.set(v_min_tresh)
    var_v_max_tresh = tk.StringVar()
    tk.Entry(win_entry, textvariable=var_v_max_tresh, width=5).grid(row=4, column=3, padx=10, pady=10)
    var_v_max_tresh.set(v_max_tresh)
    # Export?
    var_export = tk.BooleanVar()
    var_export.set(export)
    tk.Checkbutton(win_entry, variable = var_export,onvalue = True, 
                   text="Export results and figures", offvalue = False
                   ).grid(row=6, column=1, columnspan=4, pady=10)
    # Validate button
    tk.Button(win_entry, text='Launch fitting', command=win_entry.destroy
                ).grid(row=7, column=1, columnspan=4, pady=30)
    win_entry.mainloop()




    # File with result
    file_name = var_file_name.get()
    print(file_name)

    # Thresholds
    g_min_tresh = float(var_f_min_tresh.get())
    g_max_tresh = float(var_f_max_tresh.get())
    t_min_tresh = float(var_t_min_tresh.get())
    t_max_tresh = float(var_t_max_tresh.get())
    v_min_tresh = float(var_v_min_tresh.get())
    v_max_tresh = float(var_v_max_tresh.get())

    # Export
    export = var_export.get()
    





# origin file of data GPS-DR
file_name_origin_data = pd.read_csv(file_name, nrows=1, header=None)[0][0]


print("\n"+file_name_origin_data+"\n")
print('Temps entre', t_min_tresh, 'et', t_max_tresh, 's')
print('Vitesse entre', v_min_tresh, 'et', v_max_tresh, 'm/s')
print('Force entre', f_min_tresh, 'et', f_max_tresh, 'N/kg')
print("Export des résultats ?", export)




##### _____ START EXECUTION _____ #####
df = pd.read_csv(file_name, header=1)
rows_delete = df[(df['t']<t_min_tresh) | (df['t']>t_max_tresh) | \
                 (df['F']<f_min_tresh) | (df['F']>f_max_tresh) | \
                 (df['v']<v_min_tresh) | (df['v']>v_max_tresh)].index
force_delete = df['F'][rows_delete]
speed_delete = df['v'][rows_delete]
duration_delete = df['t'][rows_delete]

df.drop(rows_delete, inplace=True)
force = df.F.values
speed = df.v.values
duration = df.t.values
print(df.head())

fig = plt.figure('Scan records 3D', figsize=(9, 6.5))
ax = plt.axes(projection='3d')
ax.scatter3D(duration, speed, force, label='data kept')
ax.scatter3D(duration_delete, speed_delete, force_delete, marker='X', color='orange', label='data ignored')
ax.set_xlabel('Time (s)')
ax.set_ylabel('Velocity (m/s)')
ax.set_zlabel('Force (N/kg)')
ax.set_title("Records' surface")
ax.legend()

ax.view_init(elev=30, azim=-30)
if export:
    date_export = datetime.datetime.now().strftime("D%Y%m%d-H%H%M%S")
    name_export_fig = name_file_export + '_' + date_export + '_fig3D'
    plt.savefig(name_export_fig+'_0')








# Models
def function_fvt(d, v, D_F,  F_0c, D_v,  v_0c):
    # function use for speed - grade - duration fitting
    return (D_F/d + F_0c) * (1 - v / (D_v/d + v_0c))




# figure for visualization
fig = plt.figure('fitting', figsize=(9, 6.5))
ax = plt.axes(projection='3d')

# fitting model configuration___________________________________________________________________________________________
model = Model(function_fvt, independent_vars=['d', 'v'])


D_F_ini,  F_0c_ini, D_v_ini,  v_0c_ini = 100, 10, 100, 10 # set the starting point for the fitting
D_F_min,  F_0c_min, D_v_min,  v_0c_min = 0, 0, 0, 0
params = Parameters()
params.add('D_F', value=D_F_ini, min=D_F_min)
params.add('F_0c', value=F_0c_ini, min=F_0c_min)
params.add('D_v', value=D_v_ini, min=D_v_min)
params.add('v_0c', value=v_0c_ini, min=v_0c_min)


# first step fitting with all record speed data_________________________________________________________________________
result1 = model.fit(force, params, d=duration, v=speed)

# second step - removing outlier and fit again__________________________________________________________________________
residuals = result1.residual
z_residuals = (residuals - np.mean(residuals)) / np.std(residuals)  # z-score of the residuals
z_threshold = 2  # threshold to classify as outlier (2 = 95%; 3 = 99%)
is_outlier = (abs(z_residuals) > z_threshold)
ax.scatter3D(duration[is_outlier], speed[is_outlier], force[is_outlier],
             marker='X', color='red', label='outliers')  # plot for visualization
# remove outlier for force, speed and duration
force2 = force[~is_outlier]
speed2 = speed[~is_outlier]
duration2 = duration[~is_outlier]
result2 = model.fit(force2, params, d=duration2, v=speed2)  # fit again without outliers

# third step only with positive residuals_______________________________________________________________________________
# reset fitting parameters so starting points are the coeficients determined from step 2 and boundaries
# correspond to CI95% of the parameters
params['D_F'].value = result2.params['D_F'].value
params['F_0c'].value = result2.params['F_0c'].value
params['D_v'].value = result2.params['D_v'].value
params['v_0c'].value = result2.params['v_0c'].value
params['D_F'].min = max(D_F_min, params['D_F'].value - 2 * result2.params['D_F'].stderr)
params['D_F'].max = params['D_F'].value + 2 * result2.params['D_F'].stderr
params['F_0c'].min = max(F_0c_min, params['F_0c'].value - 2 * result2.params['F_0c'].stderr)
params['F_0c'].max = params['F_0c'].value + 2 * result2.params['F_0c'].stderr
params['D_v'].min = max(D_v_min, params['D_v'].value - 2 * result2.params['D_v'].stderr)
params['D_v'].max = params['D_v'].value + 2 * result2.params['D_v'].stderr
params['v_0c'].min = max(v_0c_min, params['v_0c'].value - 2 * result2.params['v_0c'].stderr)
params['v_0c'].max = params['v_0c'].value + 2 * result2.params['v_0c'].stderr

#remove negative residuals
residuals2 = result2.residual
# /!\ positive residuals correspond to residual < 0 because lmfit calculate residuals as predicted - observed
is_positive_res = (residuals2 < 0)
ax.scatter3D(duration2[~is_positive_res], speed2[~is_positive_res], force2[~is_positive_res],
             marker='X', color='blue', label='negative residual')   # plot for visualization
# conserve only positive residuals for force, speed and duration
force3 = force2[is_positive_res]
speed3 = speed2[is_positive_res]
duration3 = duration2[is_positive_res]
result3 = model.fit(force3, params, d=duration3, v=speed3)

# print the report of all 3 fittings____________________________________________________________________________________
print('\n\n\nStep 1:')
print(result1.fit_report())
print('\n\n\nStep 2:')
print(result2.fit_report())
print('\n\n\nStep 3:')
print(result3.fit_report())

# plot the results for visualization ___________________________________________________________________________________
d_grid = np.arange(min(duration), max(duration), 1)
v_grid = np.arange(min(speed), max(speed), 0.1)
d_grid, v_grid = np.meshgrid(d_grid, v_grid)
s_surf = model.eval(d=d_grid, v=v_grid,
                    D_F=result3.params['D_F'].value,
                    F_0c=result3.params['F_0c'].value,
                    D_v=result3.params['D_v'].value,
                    v_0c=result3.params['v_0c'].value)

ax.scatter3D(duration3, speed3, force3, color='black', label='remaining data')
ax.legend()
# d_grid[s_surf<0]
# v_grid[s_surf<0]
s_surf[s_surf<0] = np.nan
surf1 = ax.plot_surface(d_grid, v_grid, s_surf,
                        cmap=cm.jet,
                        label='fits(d,v)',
                        alpha=0.5,
                        linewidth=0,
                        antialiased=True)
ax.set_xlabel('Duration (s)')
ax.set_ylabel('Speed (m/s)')
ax.set_zlabel('Force (N/kg)')
ax.set_title("Force - Speed - Duration relationship\n"+file_name_origin_data)
ax.set_xlim([0, duration.max()])
ax.set_ylim([0, speed.max()])
ax.set_zlim([0, force.max()])



if export: 
    # Export parameters
    name_export_param = name_file_export + '_' + date_export + '_fitting.txt'
    with open(name_export_param, 'w') as f:
        f.write("Original file (datas GPS-DR)"+file_name_origin_data+"\n")
        f.write('Temps entre ' + str(t_min_tresh) + ' et ' + str(t_max_tresh) + ' s\n')
        f.write('Vitesse entre ' + str(v_min_tresh) + ' et ' + str(v_max_tresh) + ' m/s\n')
        f.write('Force entre ' + str(f_min_tresh) + ' et ' + str(f_max_tresh) + ' N/kg\n')
        f.write('\n\n\n\nStep 1:\n')
        f.write(result1.fit_report())
        f.write('\n\n\n\nStep 2:\n')
        f.write(result2.fit_report())
        f.write('\n\n\n\nStep 3:\n')
        f.write(result3.fit_report())
    # Save fig
    ax.view_init(elev=0, azim=0)
    plt.savefig(name_export_fig+'_1')
    for i in range(4):
        ax.view_init(elev=20, azim=0+30*i)
        plt.savefig(name_export_fig+'_'+str(i+2))
    ax.view_init(elev=0, azim=90)
    plt.savefig(name_export_fig+'_6')
    

ax.view_init(elev=20, azim=-40)
plt.show()
