from pylab import show
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import plotly.graph_objs as go
import tkinter as tk
from tkinter import filedialog as fd
from tkinter import ttk
import datetime
from functools import partial

from scipy import signal
from g_computation_functions import butter_lowpass_filter





class WindowInitParam(tk.Tk):
    def __init__(self, ini_cutoffs=[], ini_orders=[], filters_data=[], 
                 ini_computations={}, ini_display_plots={}, 
                 ini_threshold=None, ini_input='', ini_export=False, 
                 ini_export_df={'interp':False, 'full':False, 'small':False},
                 ini_type_import='csv'):
        tk.Tk.__init__(self)
        self.title('Entry parameters')
        # INPUT file
        tk.Label(self, text="INPUT File: ").grid(row=0, column=0, sticky=tk.W, padx=10)
        self.filename_input = tk.StringVar()
        self.filename_input.set(ini_input)
        tk.Entry(self, textvariable=self.filename_input, width=50).grid(row=1, column=0, columnspan=5, sticky=tk.E, padx=5)
        tk.Button(self, text='Browse', command=self.button_select_file).grid(row=1, column=5, sticky=tk.E, padx=2)
        # Check first import
        tk.Label(self, text="Type of import: ").grid(row=2, column=0, columnspan=2)
        self.type_import = ttk.Combobox(self, values=['csv', 'json interpolated', 'json full'])
        self.type_import.set(ini_type_import)
        self.value_type_import = self.type_import
        self.type_import.grid(row=2, column=2, padx=2)
        # Sampling period
        tk.Label(self, text="Sampling period (s): ").grid(row=3, column=0, columnspan=2, pady=10)
        self.samp_per = tk.StringVar()
        self.samp_per.set("1")
        tk.Entry(self, textvariable=self.samp_per, width=6).grid(row=3, column=2, sticky=tk.E, pady=10)
        tk.Label(self, text="(Use dot for decimals)", font="Helvetica 8 italic").grid(row=3, column=3, columnspan=2, sticky=tk.W, pady=10)
        # Interpolation method
        tk.Label(self, text="Interpolation method: ").grid(row=4, column=0, columnspan=2, pady=10)
        val_interp_meth = ['cubic', 'linear']
        self.interp_meth = tk.StringVar()
        self.interp_meth.set(val_interp_meth[0])
        for k in range(len(val_interp_meth)):
            tk.Radiobutton(self, variable=self.interp_meth,
                           value=val_interp_meth[k], text=val_interp_meth[k]).grid(row=4, column=2+k, pady=10)
        # Computations that will be done
        tk.Label(self, text="Computations: ").grid(row=5, column=0, sticky=tk.SW, padx=10, pady=15)
        self.check_computations = {'Scan record velocity 2D':tk.BooleanVar(), 
                                   'Scan record velocity 3D':tk.BooleanVar()}
        for k in range(len(self.check_computations)):
            computation = list(self.check_computations.keys())[k]
            if computation in ini_computations:
                self.check_computations[computation].set(ini_computations[computation])
            else:
                self.check_computations[computation].set(False)
            tk.Checkbutton(self, text=computation, variable = self.check_computations[computation],
                            onvalue = True, offvalue = False).grid(row=6+(k//2), column=3*(k%2), columnspan=3)
        # Export parameter
        tk.Label(self, text="Export result files: ").grid(row=7+(len(self.check_computations)+1)//2, column=0, columnspan=2, pady=10)
        self.check_export = tk.BooleanVar()
        self.check_export.set(ini_export)
        tk.Checkbutton(self, variable = self.check_export, onvalue = True, 
                       offvalue = False).grid(row=7+(len(self.check_computations)+1)//2, column=2, sticky=tk.W, pady=10)
        # Dataframes that will be exported
        tk.Label(self, text="Dataframe exported: ").grid(row=8+(len(self.check_computations)+1)//2, column=0, sticky=tk.SW, padx=10, pady=15)
        self.check_export_df = {key:tk.BooleanVar() for key in ini_export_df.keys()}
        for k in range(len(self.check_export_df)):
            exp_df = list(self.check_export_df.keys())[k]
            if exp_df in ini_export_df:
                self.check_export_df[exp_df].set(ini_export_df[exp_df])
            else:
                self.check_export_df[exp_df].set(False)
            tk.Checkbutton(self, text=exp_df, variable = self.check_export_df[exp_df],
                            onvalue = True, offvalue = False).grid(row=9+(len(self.check_computations)+1)//2, column=2*(k%3), columnspan=2)

        # Plots that will be displayed
        tk.Label(self, text="Graph displayed: ").grid(row=1, column=7, sticky=tk.SW, padx=10, pady=15)
        self.check_plots = {key:tk.BooleanVar() for key in ini_display_plots.keys()}
        for k in range(len(self.check_plots)):
            graph = list(self.check_plots.keys())[k]
            if graph in ini_display_plots:
                self.check_plots[graph].set(ini_display_plots[graph])
            else:
                self.check_plots[graph].set(False)
            tk.Checkbutton(self, text=graph, variable = self.check_plots[graph],
                            onvalue = True, offvalue = False).grid(row=2+(k//3), column=7+2*(k%3), columnspan=2)
        
        # Default filter parameters
        tk.Label(self, text="Threshold moving: ").grid(row=1, column=14, sticky=tk.E, padx=10, pady=10)
        self.threshold = tk.StringVar()
        self.threshold.set(str(ini_threshold))
        tk.Entry(self, textvariable=self.threshold, width=6).grid(row=1, column=15, sticky=tk.W, padx=10, pady=10)
        tk.Label(self, text="Filter parameters").grid(row=2, column=14, padx=10, pady=10)
        tk.Label(self, text="Cutoffs (Hz)").grid(row=2, column=15, padx=10, pady=10)
        tk.Label(self, text="Orders").grid(row=2, column=16, padx=10, pady=10)
        self.cutoffs = []
        self.orders = []
        for k in range(len(filters_data)):
            tk.Label(self, text=filters_data[k]).grid(row=3+k, column=14, padx=10)
            self.cutoffs.append(tk.StringVar())
            self.cutoffs[k].set(str(ini_cutoffs[k]))
            tk.Entry(self, textvariable=self.cutoffs[k], width=6).grid(row=3+k, column=15, padx=10)
            self.orders.append(tk.StringVar())
            self.orders[k].set(str(ini_orders[k]))
            tk.Entry(self, textvariable=self.orders[k], width=6).grid(row=3+k, column=16, padx=10)
        self.interactive = tk.BooleanVar()
        self.interactive.set(True)
        tk.Radiobutton(self, variable=self.interactive, value=False, 
                       text="Use ONLY default filter parameters,\ndo not display graphic interface,\ngraphs at the end"
                       ).grid(row=3+len(filters_data), rowspan=2, column=14, columnspan=3, sticky=tk.S, pady=20)
        tk.Radiobutton(self, variable=self.interactive, value=True, 
                       text="Interactive entry for filtering,\ndisplay graphs during the simulation"
                       ).grid(row=5+len(filters_data), rowspan=2, column=14, columnspan=3, sticky=tk.S, pady=20)
    

        ttk.Separator(self, orient=tk.VERTICAL).grid(column=6, row=0, 
                                                     rowspan=max(9+(len(self.check_computations)+1)//2+(len(self.check_export_df)+2//3), (len(self.check_plots)+2)//3, 7+len(filters_data))-1,
                                                     sticky='ns')
        ttk.Separator(self, orient=tk.VERTICAL).grid(column=13, row=0, 
                                                     rowspan=max(9+(len(self.check_computations)+1)//2+(len(self.check_export_df)+2//3), (len(self.check_plots)+2)//3, 7+len(filters_data))-1,
                                                     sticky='ns')


        # Validation button
        validate_button = tk.Button(self, text='Launch', command=self.validate_input)
        validate_button.grid(row=max(9+(len(self.check_computations)+1)//2+(len(self.check_export_df)+2//3), (len(self.check_plots)+2)//3, 7+len(filters_data)), 
                             column=8, columnspan=3, pady=20)

    def button_select_file(self):
        filetypes = (('All files', '*.*'), ('csv files', '*.csv'), ('json files', '*.json'))
        self.filename_input.set(fd.askopenfilename(title='Select a file', initialdir='', filetypes=filetypes))
        if self.filename_input.get()[-5:] == '.json':
            if self.filename_input.get()[-10:] == '_full.json':
                self.value_type_import = 'json full'
            else:
                self.value_type_import = 'json interpolated'
        else:
            self.value_type_import = 'csv'
        self.type_import.set(self.value_type_import)
    def validate_input(self):
        self.value_type_import = self.type_import.get()
        self.destroy()




class InterfaceFiltering(tk.Tk):
    def __init__(self, vars=['latitude'],
                 window_title='', 
                 fs=1,
                 df=None, 
                 columns=['Latitude', 'Longitude', 'Altitude'],
                 times='DateTime',
                 idxs_time_limits=(0,10000),
                 max_len_plot=10000,
                 y_labels=["Latitude (°)", "Longitude (°)", "Altitude (m)"], 
                 freqs=[], #[f_lat, f_lon, f_alt], 
                 P=[], #[Pxx_den_lat, Pxx_den_lon, Pxx_den_alt],
                 cutoffs=[],
                 orders=[],
                 fig_title='Position', 
                 graph_title="Raw position of the animal"):
        tk.Tk.__init__(self)
        self.tk.call('tk', 'scaling', 2.0)
        self.title(window_title)
        self.vars = vars
        self.idxs_time_limits = idxs_time_limits
        self.time_limits = (df[times][idxs_time_limits[0]], df[times][idxs_time_limits[1]])
        self.max_len_plot = max_len_plot
        # Filter init
        self.fs = fs
        self.nyq = 0.5*fs
        self.df = df 
        self.columns = columns
        self.times = times
        self.y_labels = y_labels
        self.freqs = freqs
        self.P = P
        self.filtered = [None for k in range(len(self.columns))]
        self.cutoffs = cutoffs
        self.orders = orders
        self.fig_title = fig_title
        self.graph_title = graph_title
        self.display_view()

    def display_view(self):
        # Parameter window
        # Time limits
        tk.Label(self, text="Time limits of the plot (\"YYYY-MM-DD HH:MM:SS\")      from:").grid(row=0, column=0, columnspan=3, sticky=tk.W)
        self.var_time_start = tk.StringVar()
        self.var_time_start.set(str(self.time_limits[0]))
        tk.Entry(self, textvariable=self.var_time_start, width=20).grid(row=0, column=3, sticky=tk.W)
        tk.Label(self, text="to:").grid(row=1, column=2, sticky=tk.E)
        self.var_time_end = tk.StringVar()
        self.var_time_end.set(str(self.time_limits[1]))
        tk.Entry(self, textvariable=self.var_time_end, width=20).grid(row=1, column=3, sticky=tk.W)
        tk.Label(self, text="Maximum number of points: ").grid(row=2, column=0, sticky=tk.W)
        self.var_max_len_plot = tk.StringVar()
        self.var_max_len_plot.set(str(self.max_len_plot))
        tk.Entry(self, textvariable=self.var_max_len_plot, width=8).grid(row=2, column=1, sticky=tk.W)

        self.string_vars_cutoffs = {}
        self.string_vars_orders = {}            
        for k in range(len(self.vars)):
            # Cutoff
            tk.Label(self, text="Filter cutoff for "+str(self.vars[k])+" (Hz): ").grid(row=3+2*k, column=0, sticky=tk.W)
            self.string_vars_cutoffs[self.vars[k]] = tk.StringVar()
            self.string_vars_cutoffs[self.vars[k]].set(str(self.cutoffs[k]))
            tk.Entry(self, textvariable=self.string_vars_cutoffs[self.vars[k]], width=6).grid(row=3+2*k, column=1, sticky=tk.E)
            tk.Label(self, text="Max="+str(self.nyq)+"  (Use dot for decimals)", font="Helvetica 8 italic").grid(row=3+2*k, column=2, columnspan=2, sticky=tk.W)
            # Order
            tk.Label(self, text="Filter order for "+str(self.vars[k])+": ").grid(row=4+2*k, column=0, sticky=tk.W)
            self.string_vars_orders[self.vars[k]] = tk.StringVar()
            self.string_vars_orders[self.vars[k]].set(str(self.orders[k]))
            tk.Entry(self, textvariable=self.string_vars_orders[self.vars[k]], width=6).grid(row=4+2*k, column=1, sticky=tk.E)
            tk.Label(self, text="(a positive integer)", font="Helvetica 8 italic").grid(row=4+2*k, column=2, columnspan=2, sticky=tk.W)
            
        # Button show
        tk.Button(self, text='Show', command=self.filter_again).grid(row=4+2*len(self.vars), column=0, columnspan=4)
        # Button done
        tk.Button(self, text='Done', command=self.validate_filter).grid(row=5+2*len(self.vars), column=0, columnspan=4)
        
        self.filter_and_display()


    def filter_and_display(self):
        # --Compute psd filter
        self.freqs_filtered, self.P_filtered = [None for k in range(len(self.columns))], [None for k in range(len(self.columns))]
        for k in range(len(self.columns)):
            self.filtered[k] = butter_lowpass_filter(self.df[self.columns[k]][self.idxs_time_limits[0]:self.idxs_time_limits[1]+1], self.cutoffs[k], self.fs, self.orders[k])
            self.freqs_filtered[k], self.P_filtered[k] = signal.periodogram(self.filtered[k], self.fs)
        self.fig = plt.figure(self.fig_title, figsize=(6, 4))
        # --Plot graphs
        grid = gridspec.GridSpec(ncols=2, nrows=len(self.columns), width_ratios=[2, 1])
        axs = []
        for k in range(len(self.columns)):
            axs.append([plt.subplot(grid[k,0]), plt.subplot(grid[k,1])])
        axs = np.array(axs)
        axs[0, 0].set_title(self.graph_title+"only between\n"+str(self.time_limits[0])+" and "+str(self.time_limits[1]))
        for k in range(len(self.columns)):
            axs[k, 0].plot(self.df[self.times][self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                           self.df[self.columns[k]][self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                           label="raw")
            axs[k, 0].plot(self.df[self.times][self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                           self.filtered[k],
                           label="filtered")
            axs[k, 0].set_ylabel(self.y_labels[k])
            axs[k, 0].legend(loc='upper right')
        axs[len(self.columns)-1, 0].set_xlabel("Time")
        # Plot the Power Spectral Denisities
        axs[0, 1].set_title("Power spectral density of:\n"+self.graph_title+" (unit^2/Hz)")
        for k in range(len(self.columns)):
            axs[k, 1].plot(self.freqs[k], self.P[k], label="raw")
            axs[k, 1].plot(self.freqs_filtered[k], self.P_filtered[k], label="filtered")
            axs[k, 1].plot([self.cutoffs[k]]*2, 
                        [min(min(self.P[k]),min(self.P_filtered[k])), max(max(self.P[k]),max(self.P_filtered[k]))],
                        '--', label="cutoff")
            axs[k, 1].set_xlim([0, 2*self.cutoffs[k]])
            axs[k, 1].legend(loc='upper right')
        axs[len(self.columns)-1, 1].set_xlabel("Frequency (Hz)")


    def filter_again(self):
        plt.close(self.fig)
        # Cutoffs and orders
        for k in range(len(self.columns)):
            self.cutoffs[k] = float(self.string_vars_cutoffs[self.vars[k]].get())
            self.orders[k] = int(self.string_vars_orders[self.vars[k]].get())
        # Time window
        self.max_len_plot = int(self.var_max_len_plot.get())
        self.time_limits = (self.var_time_start.get(), self.var_time_end.get())
        idxs_start = np.where(self.df[self.times] == datetime.datetime.fromisoformat(self.time_limits[0]))[0]
        idxs_end = np.where(self.df[self.times] == datetime.datetime.fromisoformat(self.time_limits[1]))[0]
        if len(idxs_start) > 0:
            start = idxs_start[0]
        else:
            start = 0
        if len(idxs_end) > 0:
            end = min(idxs_end[0], self.max_len_plot+start)
        else:
            end = min(self.df.shape[0]-1, self.max_len_plot+start)
        self.idxs_time_limits = (start, end)
        self.time_limits = (str(self.df[self.times][start]), str(self.df[self.times][end]))
        self.print_param()
        self.filter_and_display()
        show()

    def validate_filter(self):
        self.destroy()
        plt.close(self.fig)
    
    def print_param(self):
        text_printrow1 = '               '
        text_printrow2 = 'Cutoffs:       '
        text_printrow3 = 'Orders:        '
        for var in self.vars:
            cutoff = self.string_vars_cutoffs[var].get()
            order = self.string_vars_orders[var].get()
            text_printrow1 += (var+' '*(30-len(var)))
            text_printrow2 += (cutoff+' '*(30-len(cutoff)))
            text_printrow3 += (order+' '*(30-len(order)))
        print(text_printrow1+'\n'+text_printrow2+'\n'+text_printrow3)










class InterfaceFilteringBehaviour(tk.Tk):
    def __init__(self,
                 fs=1,
                 df=None, 
                 col_behav=['Behaviour'],
                 times='DateTime',
                 idxs_time_limits=(0,10000),
                 max_len_plot=10000,
                 cutoff=None,
                 order=None,
                 threshold_moving = 0.02):
        tk.Tk.__init__(self)
        self.tk.call('tk', 'scaling', 2.0)
        self.title('Filter behaviour')
        self.idxs_time_limits = idxs_time_limits
        self.time_limits = (df[times][idxs_time_limits[0]], df[times][idxs_time_limits[1]])
        self.max_len_plot = max_len_plot
        # Filter init
        self.fs = fs
        self.nyq = 0.5*fs
        self.df = df 
        self.col_behav = col_behav
        self.is_moving = (df['Behaviour']==3)
        self.times = times
        self.cutoff = cutoff
        self.order = order
        self.threshold_moving = threshold_moving
        self.display_view()

    def display_view(self):
        # Parameter window
        # Time limits
        tk.Label(self, text="Time limits of the plot (\"YYYY-MM-DD HH:MM:SS\")      from:").grid(row=0, column=0, columnspan=3, sticky=tk.W)
        self.var_time_start = tk.StringVar()
        self.var_time_start.set(str(self.time_limits[0]))
        tk.Entry(self, textvariable=self.var_time_start, width=20).grid(row=0, column=3, sticky=tk.W)
        tk.Label(self, text="to:").grid(row=1, column=2, sticky=tk.E)
        self.var_time_end = tk.StringVar()
        self.var_time_end.set(str(self.time_limits[1]))
        tk.Entry(self, textvariable=self.var_time_end, width=20).grid(row=1, column=3, sticky=tk.W)
        tk.Label(self, text="Maximum number of points: ").grid(row=2, column=0, sticky=tk.W)
        self.var_max_len_plot = tk.StringVar()
        self.var_max_len_plot.set(str(self.max_len_plot))
        tk.Entry(self, textvariable=self.var_max_len_plot, width=8).grid(row=2, column=1, sticky=tk.W)
           
        # Cutoff
        tk.Label(self, text="Filter cutoff for behaviour (Hz): ").grid(row=3, column=0, sticky=tk.W)
        self.var_cutoff = tk.StringVar()
        self.var_cutoff.set(str(self.cutoff))
        tk.Entry(self, textvariable=self.var_cutoff, width=6).grid(row=3, column=1, sticky=tk.E)
        tk.Label(self, text="Max="+str(self.nyq)+"  (Use dot for decimals)", font="Helvetica 8 italic").grid(row=3, column=2, columnspan=2, sticky=tk.W)
        # Order
        tk.Label(self, text="Filter order for behaviour: ").grid(row=4, column=0, sticky=tk.W)
        self.var_order = tk.StringVar()
        self.var_order.set(str(self.order))
        tk.Entry(self, textvariable=self.var_order, width=6).grid(row=4, column=1, sticky=tk.E)
        tk.Label(self, text="(a positive integer)", font="Helvetica 8 italic").grid(row=4, column=2, columnspan=2, sticky=tk.W)
        # Threshold moving
        tk.Label(self, text="Behaviour threshold for moving: ").grid(row=5, column=0, sticky=tk.W)
        self.var_thresh = tk.StringVar()
        self.var_thresh.set(str(self.threshold_moving))
        tk.Entry(self, textvariable=self.var_thresh, width=6).grid(row=5, column=1, sticky=tk.E)
        tk.Label(self, text="(Use dot for decimals)", font="Helvetica 8 italic").grid(row=5, column=2, columnspan=2, sticky=tk.W)
            
        # Button show
        tk.Button(self, text='Show', command=self.filter_again).grid(row=6, column=0, columnspan=4)
        # Button done
        tk.Button(self, text='Done', command=self.validate_filter).grid(row=7, column=0, columnspan=4)
        
        self.filter_and_display()


    def filter_and_display(self):
        # --Compute psd filter
        self.is_mov_filtered = butter_lowpass_filter(self.is_moving.squeeze(), self.cutoff, self.fs, self.order)
        self.bool_is_mov_filtered = (self.is_mov_filtered >= self.threshold_moving)
        self.fig = plt.figure("Filter behaviour", figsize=(6, 4))
        # --Plot graphs
        grid = gridspec.GridSpec(ncols=2, nrows=1, width_ratios=[1, 2])
        axs = np.array([[plt.subplot(grid[0,0]), plt.subplot(grid[0,1])]])
        axs[0, 0].set_title("Behaviour of the animal")
        axs[0, 0].plot(self.df[self.times].squeeze()[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                       self.df[self.col_behav].squeeze()[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                       'o', alpha=0.005)
        axs[0, 0].set_xlabel("Time")
        axs[0, 0].set_ylabel("Behaviour (1=Resting, 2=Grazing, 3=Moving)")
        axs[0, 1].set_title("True Percentage of moving (%): " + \
                            str(round(np.mean(self.is_moving.squeeze())*100, 2)) + \
                            "\nFiltered Percentage of moving (%): " + \
                            str(round(np.mean(self.bool_is_mov_filtered)*100, 2)) + \
                            "\n(on the entire file)")
        axs[0, 1].plot(self.df[self.times].squeeze()[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                       self.is_moving.squeeze()[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1]*1,
                       'o', alpha=0.005, label='data')
        axs[0, 1].plot(self.df[self.times].squeeze()[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                       self.is_mov_filtered[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                       c='red', label='filtered')
        axs[0, 1].plot(self.df[self.times].squeeze()[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                       self.bool_is_mov_filtered[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1],
                       '--', c='red', label='boolean threshold')
        axs[0, 1].plot([min(self.df[self.times].squeeze()[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1]),
                        max(self.df[self.times].squeeze()[self.idxs_time_limits[0]:self.idxs_time_limits[1]+1])],
                       [self.threshold_moving]*2,
                       '--', c='grey', label='threshold')
        axs[0, 1].legend(loc='upper right')
        axs[0, 1].set_xlabel("Time")
        axs[0, 1].set_ylabel("Behaviour (1=Moving, 0=Other)")
    

    def filter_again(self):
        plt.close(self.fig)
        # Cutoff, order and threshold
        self.cutoff = float(self.var_cutoff.get())
        self.order = int(self.var_order.get())
        self.threshold_moving = float(self.var_thresh.get())
        # Time window
        self.max_len_plot = int(self.var_max_len_plot.get())
        self.time_limits = (self.var_time_start.get(), self.var_time_end.get())
        idxs_start = np.where(self.df[self.times] == datetime.datetime.fromisoformat(self.time_limits[0]))[0]
        idxs_end = np.where(self.df[self.times] == datetime.datetime.fromisoformat(self.time_limits[1]))[0]
        if len(idxs_start) > 0:
            start = idxs_start[0]
        else:
            start = 0
        if len(idxs_end) > 0:
            end = min(idxs_end[0], self.max_len_plot+start)
        else:
            end = min(self.df.shape[0]-1, self.max_len_plot+start)
        self.idxs_time_limits = (start, end)
        self.time_limits = (str(self.df[self.times][start]), str(self.df[self.times][end]))
        self.print_param()
        self.filter_and_display()
        show()

    def validate_filter(self):
        self.destroy()
        plt.close(self.fig)
    
    def print_param(self):
        text_printrow1 = '               '+'behavior'
        text_printrow2 = 'Cutoff:        '+self.var_cutoff.get()
        text_printrow3 = 'Order:         '+self.var_order.get()
        text_printrow4 = 'Threshold:     '+self.var_thresh.get()
        print(text_printrow1+'\n'+text_printrow2+'\n'+text_printrow3+'\n'+text_printrow4)








class InterfaceTrajectory2DSimple(tk.Tk):
    def __init__(self, df, idxs_time_limits, max_len_plot):
        tk.Tk.__init__(self)
        self.tk.call('tk', 'scaling', 2.0)
        self.title('Trajectory 2D')
        self.df = df
        self.idxs_time_limits = idxs_time_limits
        self.time_limits = (df['DateTime'][idxs_time_limits[0]], df['DateTime'][idxs_time_limits[1]])
        self.max_len_plot = max_len_plot
        self.display_view()

    def display_view(self):
        # Parameter window
        # Time limits
        tk.Label(self, text="Time limits of the plot (\"YYYY-MM-DD HH:MM:SS\")      from:").grid(row=0, column=0, columnspan=3, sticky=tk.W)
        self.var_time_start = tk.StringVar()
        self.var_time_start.set(str(self.time_limits[0]))
        tk.Entry(self, textvariable=self.var_time_start, width=20).grid(row=0, column=3, sticky=tk.W)
        tk.Label(self, text="to:").grid(row=1, column=2, sticky=tk.E)
        self.var_time_end = tk.StringVar()
        self.var_time_end.set(str(self.time_limits[1]))
        tk.Entry(self, textvariable=self.var_time_end, width=20).grid(row=1, column=3, sticky=tk.W)
        tk.Label(self, text="Maximum number of points: ").grid(row=2, column=0, sticky=tk.W)
        self.var_max_len_plot = tk.StringVar()
        self.var_max_len_plot.set(str(self.max_len_plot))
        tk.Entry(self, textvariable=self.var_max_len_plot, width=8).grid(row=2, column=1, sticky=tk.W)

        # Button show
        tk.Button(self, text='Show', command=self.display_again).grid(row=3, column=0, columnspan=4)
        # Button done
        tk.Button(self, text='Done', command=self.display_done).grid(row=4, column=0, columnspan=4)
        
        self.display_plot()


    def display_plot(self):
        plt.figure('Trajectory XY')
        plt.title('Trajectory XY (between ' + \
                   str(self.time_limits[0]) + ' and ' + \
                   str(self.time_limits[1]) + ')')
        plt.plot(self.df['X'][self.idxs_time_limits[0]:self.idxs_time_limits[1]], self.df['Y'][self.idxs_time_limits[0]:self.idxs_time_limits[1]], label='Raw')
        plt.plot(self.df['X_from_filter'][self.idxs_time_limits[0]:self.idxs_time_limits[1]], self.df['Y_from_filter'][self.idxs_time_limits[0]:self.idxs_time_limits[1]], label='From filtered lonlat')
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.axis('equal')
        plt.legend()


    def display_again(self):
        plt.close('all')
        # Time window
        self.max_len_plot = int(self.var_max_len_plot.get())
        self.time_limits = (self.var_time_start.get(), self.var_time_end.get())
        idxs_start = np.where(self.df['DateTime'] == datetime.datetime.fromisoformat(self.time_limits[0]))[0]
        idxs_end = np.where(self.df['DateTime'] == datetime.datetime.fromisoformat(self.time_limits[1]))[0]
        if len(idxs_start) > 0:
            start = idxs_start[0]
        else:
            start = 0
        if len(idxs_end) > 0:
            end = min(idxs_end[0], self.max_len_plot+start)
        else:
            end = min(self.df.shape[0]-1, self.max_len_plot+start)
        self.idxs_time_limits = (start, end)
        self.time_limits = (str(self.df['DateTime'][start]), str(self.df['DateTime'][end]))
        self.display_plot()
        show()

    def display_done(self):
        self.destroy()
        plt.close('all')










class InterfaceTrajectory3D(tk.Tk):
    def __init__(self, df, idxs_time_limits, max_len_plot, 
                 x_each_step_grid, y_each_step_grid, elev_interp_each_step):
        tk.Tk.__init__(self)
        self.tk.call('tk', 'scaling', 2.0)
        self.title('Trajectory 3D')
        self.df = df
        self.idxs_time_limits = idxs_time_limits
        self.time_limits = (df['DateTime'][idxs_time_limits[0]], df['DateTime'][idxs_time_limits[1]])
        self.max_len_plot = max_len_plot
        self.x_each_step_grid = x_each_step_grid
        self.y_each_step_grid = y_each_step_grid
        self.elev_interp_each_step = elev_interp_each_step
        self.display_view()

    def display_view(self):
        # Parameter window
        # Time limits
        tk.Label(self, text="Time limits of the plot (\"YYYY-MM-DD HH:MM:SS\")      from:").grid(row=0, column=0, columnspan=3, sticky=tk.W)
        self.var_time_start = tk.StringVar()
        self.var_time_start.set(str(self.time_limits[0]))
        tk.Entry(self, textvariable=self.var_time_start, width=20).grid(row=0, column=3, sticky=tk.W)
        tk.Label(self, text="to:").grid(row=1, column=2, sticky=tk.E)
        self.var_time_end = tk.StringVar()
        self.var_time_end.set(str(self.time_limits[1]))
        tk.Entry(self, textvariable=self.var_time_end, width=20).grid(row=1, column=3, sticky=tk.W)
        tk.Label(self, text="Maximum number of points: ").grid(row=2, column=0, sticky=tk.W)
        self.var_max_len_plot = tk.StringVar()
        self.var_max_len_plot.set(str(self.max_len_plot))
        tk.Entry(self, textvariable=self.var_max_len_plot, width=8).grid(row=2, column=1, sticky=tk.W)

        # Button show
        tk.Button(self, text='Show', command=self.display_again).grid(row=3, column=0, columnspan=4)
        # Button done
        tk.Button(self, text='Done', command=self.display_done).grid(row=4, column=0, columnspan=4)
        
        self.display_plot()


    def display_plot(self):
        idxs_in_plot = np.where((self.x_each_step_grid>min(self.df['X_from_filter'][self.idxs_time_limits[0]:self.idxs_time_limits[1]])-50) & \
                                (self.x_each_step_grid<max(self.df['X_from_filter'][self.idxs_time_limits[0]:self.idxs_time_limits[1]])+50) & \
                                (self.y_each_step_grid>min(self.df['Y_from_filter'][self.idxs_time_limits[0]:self.idxs_time_limits[1]])-50) & \
                                (self.y_each_step_grid<max(self.df['Y_from_filter'][self.idxs_time_limits[0]:self.idxs_time_limits[1]])+50))
        rows_in_plot, cols_in_plot = np.unique(idxs_in_plot[0]), np.unique(idxs_in_plot[1])
        self.fig = go.Figure(data=[go.Surface(x=self.x_each_step_grid[:, cols_in_plot][rows_in_plot], 
                                              y=self.y_each_step_grid[:, cols_in_plot][rows_in_plot],
                                              z=self.elev_interp_each_step[:, cols_in_plot][rows_in_plot], 
                                              colorscale='haline', opacity=0.65)])

        self.fig.add_scatter3d(x=self.df['X'][self.idxs_time_limits[0]:self.idxs_time_limits[1]],
                          y=self.df['Y'][self.idxs_time_limits[0]:self.idxs_time_limits[1]],
                          z=self.df['Z'][self.idxs_time_limits[0]:self.idxs_time_limits[1]],
                          mode='lines', line=dict(color='green', width=4),
                          name='Data used in script (z topography)',
                          hovertemplate='<b>%{text}</b><extra></extra>',
                          text = 'Time: ' + self.df['DateTime'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
                                  '<br> Lon: '+ self.df['Longitude_filtered'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
                                  '<br> Lat: ' + self.df['Latitude_filtered'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
                                  '<br> X: ' + self.df['X_from_filter'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
                                  '<br> Y: ' + self.df['Y_from_filter'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
                                  '<br> Z: ' + self.df['Z_from_filter'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str))
        # self.fig.add_scatter3d(x=self.df['X'][self.idxs_time_limits[0]:self.idxs_time_limits[1]],
        #                   y=self.df['Y'][self.idxs_time_limits[0]:self.idxs_time_limits[1]],
        #                   z=self.df['Altitude'][self.idxs_time_limits[0]:self.idxs_time_limits[1]],
        #                   mode='lines', line=dict(color='red', width=4),
        #                   name='Raw data',
        #                   hovertemplate='<b>%{text}</b><extra></extra>',
        #                   text = 'Time: ' + self.df['DateTime'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
        #                           '<br> Lon: '+ self.df['Longitude'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
        #                           '<br> Lat: ' + self.df['Latitude'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
        #                           '<br> X: ' + self.df['X'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
        #                           '<br> Y: ' + self.df['Y'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str) + \
        #                           '<br> Z: ' + self.df['Altitude'][self.idxs_time_limits[0]:self.idxs_time_limits[1]].astype(str))
        self.fig.update_layout(legend=dict(x=0.1), width = 1000, height =550,
                          scene_aspectmode='data',
                          title='3D trajectory (between ' + \
                          str(self.time_limits[0]) + ' and ' + \
                          str(self.time_limits[1]) + ')')
        self.fig.show()



    def display_again(self):
        # Time window
        self.max_len_plot = int(self.var_max_len_plot.get())
        self.time_limits = (self.var_time_start.get(), self.var_time_end.get())
        idxs_start = np.where(self.df['DateTime'] == datetime.datetime.fromisoformat(self.time_limits[0]))[0]
        idxs_end = np.where(self.df['DateTime'] == datetime.datetime.fromisoformat(self.time_limits[1]))[0]
        if len(idxs_start) > 0:
            start = idxs_start[0]
        else:
            start = 0
        if len(idxs_end) > 0:
            end = min(idxs_end[0], self.max_len_plot+start)
        else:
            end = min(self.df.shape[0]-1, self.max_len_plot+start)
        self.idxs_time_limits = (start, end)
        self.time_limits = (str(self.df['DateTime'][start]), str(self.df['DateTime'][end]))
        self.display_plot()

    def display_done(self):
        self.destroy()










class FinalNextPlot(tk.Tk):
    def __init__(self, poss_next_plots = ['Trajectory 2D', 'Trajectory 3D', 'Longitude-Latitude',
                                          'Grade', 'Velocity', 'Behaviour',
                                          'Scan record velocity 2D', 'Scan record velocity 3D']):
        tk.Tk.__init__(self)
        self.title('Choose next plot')
        # Button show next plot
        self.show_next_graphs = False
        self.next_display_graph = ''
        tk.Label(self, text="Next plot:").grid(row=0, column=0, sticky=tk.W, padx=20)
        for k in range(len(poss_next_plots)):
            tk.Button(self, text=poss_next_plots[k], 
                            command=partial(self.close_show_next_plot, poss_next_plots[k])
                            ).grid(row=(k//2)+1, column=(k%2), padx=10, pady=5)
        tk.Button(self, text='Finish execution', font='bold',
                        command=self.destroy
                        ).grid(row=(len(poss_next_plots)+1)//2+1, column=0, columnspan=2, padx=10, pady=5)
    
    def close_show_next_plot(self, plot):
        self.next_display_graph = plot
        self.show_next_graphs = True
        self.destroy()
