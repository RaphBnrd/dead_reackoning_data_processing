import pandas as pd
import numpy as np
from scipy import interpolate

def first_import_csv(path_input, sampling_period, interp_method, 
                     datetime_start_file, datetime_start_process, datetime_end_process,
                     csv_type_columns='complete'):
    """
    - We open the file path_input in a dataframe between datetime_start_process and datetime_end_process
    - We clean the dataframe 
        > remove repeated datetimes
        > remove rows with NA in some columns
    - We interpolate the dataframe in temporal
      considering a sampling_period and an interpolation method
    - We add a column 'IsMoving' corresponding to behaviour=3
    NB: this function is adapted to 2 types of files. It is possible to evolve the function
    in order to import new type of files. 
    
    Input:
        . path_input is a str (path of the input csv file)
        . sampling_period is the sampling perdiod wanted after the
          interpolation (float in sec)
        . interp_method is the interpolation method chosen for the
          scipy interpolation (str = 'cubic' or 'linear')
        . datetime_start_file is the first DateTime of the csv file
        . datetime_start_process is the first DateTime we want to import
        . datetime_end_process is the last DateTime we want to import
        . csv_type_columns is the type of csv we import depending on the columns there are
          ('corrected' if there are Date, Time, Latitude, Longitude, Altitude
          or 'complete' if there are DateTime, DDMT_Latitude, DDMT_Longitude, Altitude, Behaviour)
    Output:
        . df is the output dataframe interpolated. 
          It only contains columns 'DateTime', 'Behaviour', 'Longitude' and 'Latitude'
    """
    # Prepare indices import (start and end corresponds approximately to the datetimes we want to import)
    n_start = int((pd.Timestamp(datetime_start_process) \
                   - pd.Timestamp(datetime_start_file)).delta / (1e9*sampling_period))
    n_rows = int((pd.Timestamp(datetime_end_process) \
                  - pd.Timestamp(datetime_start_process)).delta / (1e9*sampling_period))+1

    # We create a dataframe from the csv file
    margin = 200000 # rows (to be sure that we import the datetimes we want, even if there are many repeated/missing datetimes before and after)
    df = pd.read_csv(path_input, na_values="NA", skiprows=range(1, max(1, n_start-margin)), nrows=n_rows+2*margin)
    if csv_type_columns == 'corrected':
        df['DateTime'] = df['Date'] + ' ' + df['Time']
        df['DateTime'] = pd.to_datetime(df['DateTime'], format= "%d/%m/%Y %H:%M:%S") # this column's type is datetime (it will be usefull to compute durations, ...)
    elif csv_type_columns == 'complete':
        df['DateTime'] = pd.to_datetime(df['DateTime'], format= "%Y-%m-%d %H:%M:%S") # this column's type is datetime (it will be usefull to compute durations, ...)
    
    df = df[(df['DateTime'] >= pd.Timestamp(datetime_start_process)) & \
            (df['DateTime'] <= pd.Timestamp(datetime_end_process))]
    df.reset_index(inplace=True)

    # We delete repeated datetimes (in different lines) or NA values in DateTime
    df['delta_time'] = df['DateTime'].diff().dt.seconds
    rows_delete_k = np.where((np.array(df['delta_time']) <= 0) & \
                            (np.isfinite(np.array((df['delta_time'])))) )[0]  # idx k where T_k-1 is not lower than T_k or [T_k]-[T_k-1] is not finite
    rows_delete_k_m1 = rows_delete_k - 1  # we also need to remove k-1
    rows_delete = np.unique(np.concatenate((rows_delete_k, rows_delete_k_m1)))
    df.drop(labels=rows_delete, axis=0, inplace=True)
    df.reset_index(inplace=True)
    df.drop(labels='index', axis=1, inplace=True)
    print('\nDeletion of non strictly growing datetimes')

    # We delete rows with NaN values in columns Latitude, Longitude
    if csv_type_columns == 'corrected':
        df.dropna(inplace=True, subset = ['Latitude', 'Longitude'])
    elif csv_type_columns == 'complete':
        df.dropna(inplace=True, subset = ['DDMT_Latitude', 'DDMT_Longitude'])
    df.reset_index(inplace=True)
    print('NaN values deleted (Latitude, Longitude)')

    # We interpolate datas to have a row each sampling period
    first_datetime = min(df['DateTime'])
    last_datetime = max(df['DateTime'])
    datetimes = pd.to_datetime(np.arange(first_datetime.value,
                                         last_datetime.value+sampling_period, 
                                         sampling_period*1e9))

    if csv_type_columns == 'corrected':
        raw_lon, raw_lat = df['Longitude'], df['Latitude']
    elif csv_type_columns == 'complete':
        raw_lon, raw_lat = df['DDMT_Longitude'], df['DDMT_Latitude']
    f_interp_lon = interpolate.interp1d(df['DateTime'].apply(lambda x: x.value), 
                                        raw_lon,
                                        kind=interp_method)
    lon = f_interp_lon(datetimes.to_series().apply(lambda x: x.value))
    f_interp_lat = interpolate.interp1d(df['DateTime'].apply(lambda x: x.value), 
                                        raw_lat,
                                        kind=interp_method)
    lat = f_interp_lat(datetimes.to_series().apply(lambda x: x.value))
        
    behaviour = 3*np.ones((len(datetimes))) # 3 corresponds to the beahviour "Moving"
    # The computations will be done on region globally in "moving" behaviour
    # To avoid ignored values, set behaviour to 3 by default (in 'corrected' file type for example)

    if csv_type_columns == 'complete':
        f_interp_behaviour = interpolate.interp1d(df['DateTime'].apply(lambda x: x.value), 
                                                  df['Behaviour'],
                                                  kind='nearest')
        behaviour = f_interp_behaviour(datetimes.to_series().apply(lambda x: x.value))

    df_interp = pd.DataFrame({'DateTime':datetimes,
                              'Behaviour':behaviour,
                              'Longitude':lon,
                              'Latitude':lat})
    df = df_interp
    print('Interpolation done (Longitude, Latitude, Behaviour)')

    return df



