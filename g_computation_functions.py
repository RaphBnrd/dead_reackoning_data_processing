import numpy as np
from scipy import signal
import matplotlib.pyplot as plt


##### _____ FUNCTIONS _____ #####

# Filter function
def butter_lowpass_filter(data, cutoff, nyq, order):
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = signal.butter(order, normal_cutoff, btype='low', analog=False)
    y = signal.filtfilt(b, a, data)
    return y

def butter_highpass_filter(data, cutoff, nyq, order):
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = signal.butter(order, normal_cutoff, btype='high', analog=False)
    y = signal.filtfilt(b, a, data)
    return y

def find_freq_fondamental(data, cutoff=0.00005, nyq=0.5*1, order=1, fs=1):
    # Filter the signal with high pass with a low frequency
    data_filter_highpass = butter_highpass_filter(data, cutoff, nyq, order)
    # Power Spectral Density (and freq) of the signal filtered
    f_data_filter_highpass, Pxx_den_data_filter_highpass = signal.periodogram(data_filter_highpass, fs)
    # Find the maximum of the psd (which is the power of the fondamental)
    idx_of_max = np.where(Pxx_den_data_filter_highpass == Pxx_den_data_filter_highpass.max())[0][0]
    freq_fond = f_data_filter_highpass[idx_of_max]

    # plot what happend to find the fondamental
    f_data, Pxx_den_data = signal.periodogram(data, fs)
    plt.figure('PDS of high pass filtered')
    plt.plot(f_data, Pxx_den_data, label='Raw signal')
    plt.plot(f_data_filter_highpass, Pxx_den_data_filter_highpass, label='High pass filtered')
    plt.legend()
    plt.xlim(0, 3*freq_fond)
    plt.show()


    return freq_fond


def movmean(T, m): 
    """
    At the index k of the output, we have the mean between k and k+m-1
    length of the output = len(T)-m+1
    """
    if m > T.shape[0]:
        return "the window of the moving average is too large"
    n = T.shape[0]

    sums = np.zeros(n-m+1) 
    sums[0] = np.sum(T[0:m])
    
    cumsum = np.cumsum(T) 
    cumsum = np.insert(cumsum, 0, 0)  # Insert a 0 at the beginning of the array
    
    sums = cumsum[m:] - cumsum[:-m] 
    return sums / m


def movstd(T, m):
    """
    At the index k of the output, we have the std between k and k+m-1
    length of the output = len(T)-m+1
    """
    if m > T.shape[0]:
        return "the window of the moving std is too large"
    n = T.shape[0]
    
    cumsum = np.cumsum(T) 
    cumsum_square = np.cumsum(T**2)
    
    cumsum = np.insert(cumsum, 0, 0)        # Insert a 0 at the beginning of the array 
    cumsum_square = np.insert(cumsum_square, 0, 0)  # Insert a 0 at the beginning of the array
    
    seg_sum = cumsum[m:] - cumsum[:-m]
    seg_sum_square = cumsum_square[m:] - cumsum_square[:-m]
    
    return np.sqrt(seg_sum_square/m - (seg_sum/m)**2)
