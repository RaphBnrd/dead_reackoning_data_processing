import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import plotly.graph_objs as go
import pandas as pd
import matplotlib.dates as mdates



def plot_graphs_behaviour(df, max_len_plot, threshold_moving, is_moving_filtered, bool_is_moving_filtered):
    plt.figure('Behaviour')
    grid = gridspec.GridSpec(ncols=2, nrows=1, width_ratios=[1, 2])
    axs = np.array([[plt.subplot(grid[0,0]), plt.subplot(grid[0,1])]])
    axs[0, 0].plot(df['DateTime'][:min(max_len_plot, df.shape[0])], df['Behaviour'][:min(max_len_plot, df.shape[0])], 'o', alpha=0.005)
    axs[0, 0].set_xlabel("Time")
    axs[0, 0].set_ylabel("Behaviour (1=Resting, 2=Grazing, 3=Moving)")
    axs[0, 0].set_title("Behaviour of the animal")
    axs[0, 1].plot(df['DateTime'][:min(max_len_plot, df.shape[0])], (df['Behaviour']==3)[:min(max_len_plot, df.shape[0])]*1, 'o', alpha=0.005, label='data')
    axs[0, 1].plot(df['DateTime'][:min(max_len_plot, df.shape[0])], is_moving_filtered[:min(max_len_plot, df.shape[0])], c='red', label='filtered')
    axs[0, 1].plot(df['DateTime'][:min(max_len_plot, df.shape[0])], bool_is_moving_filtered[:min(max_len_plot, df.shape[0])], '--', c='red', label='boolean threshold')
    axs[0, 1].plot([min(df['DateTime'][:min(max_len_plot, df.shape[0])]), max(df['DateTime'][:min(max_len_plot, df.shape[0])])], [threshold_moving]*2, '--', c='grey', label='threshold')
    axs[0, 1].legend(loc='upper right')
    axs[0, 1].set_xlabel("Time")
    axs[0, 1].set_ylabel("Behaviour (1=Moving, 0=Other)")
    axs[0, 1].set_title("Behaviour of the animal\n"+"Percentage of moving (%): " +\
                    str(round(np.mean(df['IsMoving'])*100, 2)))
            

def plot_graph_with_psd_no_filter(df, 
            columns=['DDMT_Latitude', 'DDMT_Longitude', 'Altitude'],
            times='DateTime',
            y_labels=["Latitude (°)", "Longitude (°)", "Altitude (m)"], 
            freqs=[], #[f_lat, f_lon, f_alt], 
            P=[], #[Pxx_den_lat, Pxx_den_lon, Pxx_den_alt],
            fig_title='Position', 
            graph_title="Raw position of the animal"):
    plt.figure(fig_title)
    grid = gridspec.GridSpec(ncols=2, nrows=len(columns), width_ratios=[2, 1])
    axs = []
    for k in range(len(columns)):
        axs.append([plt.subplot(grid[k,0]), plt.subplot(grid[k,1])])
    axs = np.array(axs)
    axs[0, 0].set_title(graph_title)
    for k in range(len(columns)):
        axs[k, 0].plot(df[times], df[columns[k]])
        axs[k, 0].set_ylabel(y_labels[k])
    axs[len(columns)-1, 0].set_xlabel("Time")
    # Plot the Power Spectral Denisities
    axs[0, 1].set_title("Power spectral density of:\n"+graph_title+" (unit^2/Hz)")
    for k in range(len(columns)):
        axs[k, 1].plot(freqs[k], P[k])
    axs[len(columns)-1, 1].set_xlabel("Frequency (Hz)")


def plot_graph_with_psd_with_filter(df, 
            columns=['DDMT_Latitude', 'DDMT_Longitude', 'Altitude'],
            times='DateTime',
            y_labels=["Latitude (°)", "Longitude (°)", "Altitude (m)"], 
            freqs=[], #[f_lat, f_lon, f_alt], 
            P=[], #[Pxx_den_lat, Pxx_den_lon, Pxx_den_alt],
            columns_filtered=['DDMT_Latitude_filtered', 'DDMT_Longitude_filtered', 'Altitude_filtered'],
            freqs_filtered=[], 
            P_filtered=[],
            cutoffs=[],
            fig_title='Position', 
            graph_title="Raw position of the animal"):
    plt.figure(fig_title)
    grid = gridspec.GridSpec(ncols=2, nrows=len(columns), width_ratios=[2, 1])
    axs = []
    for k in range(len(columns)):
        axs.append([plt.subplot(grid[k,0]), plt.subplot(grid[k,1])])
    axs = np.array(axs)
    axs[0, 0].set_title(graph_title)
    for k in range(len(columns)):
        axs[k, 0].plot(df[times], df[columns[k]], label="raw")
        axs[k, 0].plot(df[times], df[columns_filtered[k]], label="filtered")
        axs[k, 0].set_ylabel(y_labels[k])
        axs[k, 0].legend(loc='upper right')
    axs[len(columns)-1, 0].set_xlabel("Time")
    # Plot the Power Spectral Denisities
    axs[0, 1].set_title("Power spectral density of:\n"+graph_title+" (unit^2/Hz)")
    for k in range(len(columns)):
        axs[k, 1].plot(freqs[k], P[k], label="raw")
        axs[k, 1].plot(freqs_filtered[k], P_filtered[k], label="filtered")
        axs[k, 1].plot([cutoffs[k]]*2, 
                       [min(min(P[k]),min(P_filtered[k])), max(max(P[k]),max(P_filtered[k]))],
                       '--', label="cutoff")
        axs[k, 1].set_xlim([0, 2*cutoffs[k]])
        axs[k, 1].legend(loc='upper right')
    axs[len(columns)-1, 1].set_xlabel("Frequency (Hz)")


def plot_trajectory2D_simple(df, idx_plot_min, idx_plot_max):
    plt.figure('Trajectory XY')
    plt.title('Trajectory XY (between ' + \
               str(df['DateTime'][idx_plot_min]) + ' and ' + \
               str(df['DateTime'][idx_plot_max]) + ')')
    plt.plot(df['X'][idx_plot_min:idx_plot_max], df['Y'][idx_plot_min:idx_plot_max], label='Raw')
    plt.plot(df['X_from_filter'][idx_plot_min:idx_plot_max], df['Y_from_filter'][idx_plot_min:idx_plot_max], label='From filtered lonlat')
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.axis('equal')
    plt.legend()


def plot_trajectory3D(df, idx_plot_min, idx_plot_max, 
                      x_each_step_grid, y_each_step_grid, elev_interp_each_step):
    idxs_in_plot = np.where((x_each_step_grid>min(df['X_from_filter'][idx_plot_min:idx_plot_max])-50) & \
                            (x_each_step_grid<max(df['X_from_filter'][idx_plot_min:idx_plot_max])+50) & \
                            (y_each_step_grid>min(df['Y_from_filter'][idx_plot_min:idx_plot_max])-50) & \
                            (y_each_step_grid<max(df['Y_from_filter'][idx_plot_min:idx_plot_max])+50))
    rows_in_plot, cols_in_plot = np.unique(idxs_in_plot[0]), np.unique(idxs_in_plot[1])
    fig = go.Figure(data=[go.Surface(x=x_each_step_grid[:, cols_in_plot][rows_in_plot], 
                                     y=y_each_step_grid[:, cols_in_plot][rows_in_plot],
                                     z=elev_interp_each_step[:, cols_in_plot][rows_in_plot], 
                                     colorscale='haline', opacity=0.65)])

    fig.add_scatter3d(x=df['X'][idx_plot_min:idx_plot_max], y=df['Y'][idx_plot_min:idx_plot_max], z=df['Z'][idx_plot_min:idx_plot_max], 
                      mode='lines', line=dict(color='green', width=4),
                      name='Data used in script (z topography)',
                      hovertemplate='<b>%{text}</b><extra></extra>',
                      text = 'Time: ' + df['DateTime'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> Lon: '+ df['DDMT_Longitude'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> Lat: ' + df['DDMT_Latitude'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> X: ' + df['X'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> Y: ' + df['Y'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> Z: ' + df['Z'][idx_plot_min:idx_plot_max].astype(str))
    fig.add_scatter3d(x=df['X'][idx_plot_min:idx_plot_max], y=df['Y'][idx_plot_min:idx_plot_max], z=df['Altitude'][idx_plot_min:idx_plot_max], 
                      mode='lines', line=dict(color='red', width=4),
                      name='Raw data (altitude)',
                      hovertemplate='<b>%{text}</b><extra></extra>',
                      text = 'Time: ' + df['DateTime'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> Lon: '+ df['DDMT_Longitude'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> Lat: ' + df['DDMT_Latitude'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> X: ' + df['X'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> Y: ' + df['Y'][idx_plot_min:idx_plot_max].astype(str) + \
                             '<br> Z: ' + df['Altitude'][idx_plot_min:idx_plot_max].astype(str))
    fig.update_layout(legend=dict(x=0.1), width = 1000, height =550,
                      scene_aspectmode='data',
                      title='3D trajectory (between ' + \
                      str(df['DateTime'][idx_plot_min]) + ' and ' + \
                      str(df['DateTime'][idx_plot_max]) + ')')

    fig.show()


def plot_components_force(df, force_loc, force_gra, force_acc):
    plt.figure('Components of the force')
    plt.title('Components of the force (before filtering)')
    plt.stackplot(df['DateTime'], force_loc, force_gra, force_acc, labels=['Locomotion','Slope','Acceleration'])
    # plt.plot(df['DateTime'], df['Force'], color='k', alpha=0.5, label='Global Force')
    plt.legend()
    plt.ylabel('Force (N/kg)')


def plot_distribution_v_g(df):
    # Velocity distribution
    plt.figure('Histogram velocities')
    plt.title('Number of instant per velocity')
    plt.hist(df['Velocity_filtered'], bins=100)
    plt.xlabel('Velocity filtered (m/s)')

    # Grade distribution
    plt.figure('Histogram grades')
    plt.title('Number of instant per grade')
    plt.hist(df['Grade_filtered'], bins=100)
    plt.xlabel('grade filtered (m/s)')


def plot_activity_over_day(df, is_moving_filtered):
    # Velocity and behaviour over a day
    fig_v, ax_v = plt.subplots(1,1, num='Velocity over days')
    ax_v.set_title('Velocity over days')
    fig_b, ax_b = plt.subplots(1,1, num='Is_moving filtered over days')
    ax_b.set_title('Is_moving filtered over days')
    seconds_of_day = np.array(3600 * df['DateTime'].dt.hour + \
                            60 * df['DateTime'].dt.minute + \
                            df['DateTime'].dt.second)
    for date in np.unique(df['DateTime'].dt.date):
        t_data = pd.to_datetime(seconds_of_day[df['DateTime'].dt.date==date]*1e9)
        v_data = np.array(df['Velocity_filtered'])[df['DateTime'].dt.date==date]
        b_data = np.array(is_moving_filtered)[df['DateTime'].dt.date==date]
        ax_v.plot(t_data, v_data, alpha=0.05, color='k')
        ax_b.plot(t_data, b_data, alpha=0.2, color='k')
    ax_v.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
    ax_b.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
    ax_v.set_ylabel('Velocity (m/s)')
    ax_b.set_ylabel('Behaviour filtered (1=moving, 0=other)')


def plot_force_velocity(df):
    plt.figure('Force-velocity')
    plt.title('Force-velocity relationship')
    plt.scatter(df['Velocity_filtered'], df['Force_filtered'], alpha=0.1)
    plt.xlabel('Velocity (m/s)')
    plt.ylabel('Force (N/kg)')


def plot_scan_2D_records(t2, v2, rec_label_axis='Velocity (m/s)'):
    fig = plt.figure('Scan records 2D')
    plt.scatter(t2, v2)
    plt.xlabel('Time (s)')
    plt.ylabel(rec_label_axis)
    plt.title("Records' curve")


def plot_scan_3D_records(t3, cond3, rec3, cond_label_axis='Grade (%)',
                                          rec_label_axis='Velocity (m/s)',
                                          fig_name='Scan records velocity 3D'):
    fig = plt.figure(fig_name)
    ax = plt.axes(projection='3d')
    ax.scatter3D(t3, cond3, rec3)
    ax.set_xlabel('Time (s)')
    ax.set_ylabel(cond_label_axis)
    ax.set_zlabel(rec_label_axis)
    ax.set_title("Records' surface")
