import json
import numpy as np

def export_df_final(df, path_export):
    with open(path_export, 'w', encoding='utf-8') as json_file:

        json_data = {
            'version': '0.1',
            'data': {
                'Time metadata': {'start': str(df['DateTime'][0]), 
                                  'end': str(df['DateTime'][df.shape[0]-1]),
                                  'sampling_period(s)': round((df['DateTime'][1]-df['DateTime'][0]).delta/1e9, 6)},
                'Speed': list(np.around(df['Velocity_filtered'], decimals=5)),
                'Slope': list(np.around(df['Grade'], decimals=5)),
                'Altitude': list(np.around(df['Z_from_filter'], decimals=5)),
                'Force': list(np.around(df['Force_filtered'], decimals=5)),
            }
        }

        json.dump(json_data, json_file, indent=2)